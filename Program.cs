﻿using System;
using System.Diagnostics;
using System.Globalization;
using System.Threading;
using System.Windows.Forms;
using Cashier.Class;

namespace Cashier
{
    static class Program
    {
        //Load
        static void Main()
        {
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture("en-GB");
            string strProcessName = Process.GetCurrentProcess().ProcessName;

            Process[] Oprocess = Process.GetProcessesByName(strProcessName);
 
            if (Oprocess.Length > 1) 
            {
                MsgBoxClass.MassageBoxShowButtonOk_Error("โปรแกรมเปิดใช้งานอยู่แล้ว" + Environment.NewLine + "ไม่สามารถเปิดใช้งานโปรแกรมซ้ำได้", SystemClass.HeaderMassage);
                return;
            }
            else
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Main ());
            }
        }
    }
}
