﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using System.Configuration;

namespace Cashier.Class
{
    class ConnectionClass
    {
        public static string ConSelectMain = ConfigurationManager.ConnectionStrings["SPC119SRV"].ConnectionString;

        //Select Main 24
        public static DataTable SelectSQL_Main(string _sql)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter adp = new SqlDataAdapter();
                SqlConnection sqlConnection = new SqlConnection(ConSelectMain);
                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }
                cmd = new SqlCommand(_sql, sqlConnection);
                adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                sqlConnection.Close();
            }
            catch (Exception)
            {
                //throw;
            }

            return dt;
        }

        //Select SendServer
        public static DataTable SelectSQL_SendServer(string _sql, string ConnectionServer)
        {
            DataTable dt = new DataTable();
            try
            {
                SqlCommand cmd = new SqlCommand();
                SqlDataAdapter adp = new SqlDataAdapter();
                SqlConnection sqlConnection = new SqlConnection(ConnectionServer);
                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }
                cmd = new SqlCommand(_sql, sqlConnection);
                adp = new SqlDataAdapter(cmd);
                adp.Fill(dt);
                sqlConnection.Close();
            }
            catch (Exception)
            {

            }

            return dt;
        }

        //MyJeds save Sql For sql > 1
        public static string ExecuteSQL_ArrayMain(ArrayList _sql)
        {
            try
            {
                SqlConnection sqlConnection = new SqlConnection(ConSelectMain);
                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand = sqlConnection.CreateCommand();
                SqlTransaction transaction = sqlConnection.BeginTransaction("SampleTransaction");
                sqlCommand.Connection = sqlConnection;
                sqlCommand.Transaction = transaction;

                try
                {
                    for (int i = 0; i < _sql.Count; i++)
                    {
                        sqlCommand.CommandText = Convert.ToString(_sql[i]);
                        sqlCommand.ExecuteNonQuery();
                    }

                    transaction.Commit();
                    return "";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
                finally
                {
                    sqlConnection.Close();
                }
            }
            catch (Exception ex1)
            {
                return ex1.Message;
            }
        }

        //MyJeds save Sql For sql > 1
        public static string ExecuteSQL_ArraySendServer(ArrayList _sql, string ConnectionServer)
        {
            try
            {
                SqlConnection sqlConnection = new SqlConnection(ConnectionServer);
                if (sqlConnection.State == ConnectionState.Closed)
                {
                    sqlConnection.Open();
                }
                SqlCommand sqlCommand = new SqlCommand();
                sqlCommand = sqlConnection.CreateCommand();
                SqlTransaction transaction = sqlConnection.BeginTransaction("SampleTransaction");
                sqlCommand.Connection = sqlConnection;
                sqlCommand.Transaction = transaction;

                try
                {
                    for (int i = 0; i < _sql.Count; i++)
                    {
                        sqlCommand.CommandText = Convert.ToString(_sql[i]);
                        sqlCommand.ExecuteNonQuery();
                    }

                    transaction.Commit();
                    return "";

                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    return ex.Message;
                }
                finally
                {
                    sqlConnection.Close();
                }
            }
            catch (Exception ex1)
            {
                return ex1.Message;
            }
        }
        //Insert 24 + AX 
        public static string ExecuteMain_AX_24_SameTime(ArrayList str24, ArrayList strAX)
        {
            //24
            SqlConnection sqlConnection24 = new SqlConnection(ConSelectMain);
            if (sqlConnection24.State == ConnectionState.Closed)
            {
                sqlConnection24.Open();
            }
            //  SqlCommand sqlCommand24 = new SqlCommand();
            SqlCommand sqlCommand24 = sqlConnection24.CreateCommand();
            SqlTransaction transaction24 = sqlConnection24.BeginTransaction("SampleTransaction");
            sqlCommand24.Connection = sqlConnection24;
            sqlCommand24.Transaction = transaction24;
            //AX
            SqlConnection sqlConnectionAX = new SqlConnection(SystemClass.pServerAX);
            if (sqlConnectionAX.State == ConnectionState.Closed)
            {
                try
                {
                    sqlConnectionAX.Open();
                }
                catch (Exception)
                {
                    return "ระบบ AX-SPC708SRV หลักมีปัญหาไม่สามารถส่งข้อมูลได้";
                }

            }
            // SqlCommand sqlCommandAX = new SqlCommand();
            SqlCommand sqlCommandAX = sqlConnectionAX.CreateCommand();
            SqlTransaction transactionAX = sqlConnectionAX.BeginTransaction("SampleTransaction");
            sqlCommandAX.Connection = sqlConnectionAX;
            sqlCommandAX.Transaction = transactionAX;

            try
            {
                for (int i24 = 0; i24 < str24.Count; i24++)
                {
                    sqlCommand24.CommandText = Convert.ToString(str24[i24]);
                    sqlCommand24.ExecuteNonQuery();
                }

                for (int i = 0; i < strAX.Count; i++)
                {
                    sqlCommandAX.CommandText = Convert.ToString(strAX[i]);
                    sqlCommandAX.ExecuteNonQuery();
                }

                transaction24.Commit();
                transactionAX.Commit();

                sqlConnection24.Close();
                sqlConnectionAX.Close();
                return "";
            }
            catch (Exception ex)
            {
                transaction24.Rollback();
                transactionAX.Rollback();
                return "ไม่สามารถบันทึกข้อมูลเข้าระบบได้ ลองใหม่อีกครั้ง." + Environment.NewLine + ex.Message;
            }
        }


    }
}
