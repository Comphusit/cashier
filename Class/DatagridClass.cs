﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Telerik.WinControls.UI;
  
namespace Cashier.Class
{
    class DatagridClass
    {

        //MyJeeds SetDefaultRadGridView

        //Set Font DrorpDown
        public static void SetDefaultFontDropDown(RadDropDownList rad_Dropdown)
        {
            
            rad_Dropdown.DropDownListElement.ListElement.Font = SystemClass.SetFontGernaral;
            rad_Dropdown.DropDownListElement.Font = SystemClass.SetFontGernaral;
            rad_Dropdown.DropDownListElement.TextBox.ForeColor = Color.Blue;
            rad_Dropdown.DropDownListElement.TextBox.Font = SystemClass.SetFontGernaral;
            rad_Dropdown.DropDownListElement.DropDownWidth = 250;
            rad_Dropdown.DropDownListElement.DropDownHeight = 280;
        }

        //Set Font DrorpDown Shot
        public static void SetDefaultFontDropDownShot(RadDropDownList rad_Dropdown)
        {
            rad_Dropdown.DropDownListElement.ListElement.Font = SystemClass.SetFontGernaral;
            rad_Dropdown.DropDownListElement.Font = SystemClass.SetFontGernaral;
            rad_Dropdown.DropDownListElement.TextBox.ForeColor = Color.Blue;
            rad_Dropdown.DropDownListElement.TextBox.Font = SystemClass.SetFontGernaral;
            rad_Dropdown.DropDownListElement.DropDownWidth = 120;
            rad_Dropdown.DropDownListElement.DropDownHeight = 180;
        }
        //Set radDatetime
        public static void SetDefaultFontDateTimePicker(RadDateTimePicker rad_DateTime, DateTime defalut_DateValues, DateTime defalut_DateMax)
        {
            // DateTime defalut_DateMax
            rad_DateTime.Format = DateTimePickerFormat.Custom;
            rad_DateTime.Culture = new System.Globalization.CultureInfo("en-US");
            rad_DateTime.CustomFormat = "dd-MM-yyyy";
            rad_DateTime.Font = SystemClass.SetFontGernaral;
            rad_DateTime.ForeColor = Color.Blue;
            rad_DateTime.Value = defalut_DateValues;
            // rad_DateTime.Text = defalut_DateValues.ToString();
            rad_DateTime.MaxDate = defalut_DateMax;
        }
        //Set DateTimePicker Year
        public static void SetDefaultFontDateTimePicker_Year(RadDateTimePicker rad_DateTime, DateTime defalut_DateValues, DateTime defalut_DateMax)
        {
            // DateTime defalut_DateMax
            rad_DateTime.Format = DateTimePickerFormat.Custom;
            rad_DateTime.Culture = new System.Globalization.CultureInfo("en-US");
            rad_DateTime.CustomFormat = "yyyy";
            rad_DateTime.Font = SystemClass.SetFontGernaral;
            rad_DateTime.ForeColor = Color.Blue;
            rad_DateTime.Value = defalut_DateValues;
            // rad_DateTime.Text = defalut_DateValues.ToString();
            rad_DateTime.MaxDate = defalut_DateMax;
        }
        //Set DateTimePicker Month
        public static void SetDefaultFontDateTimePicker_Month(RadDateTimePicker rad_DateTime, DateTime defalut_DateValues, DateTime defalut_DateMax)
        {
            // DateTime defalut_DateMax
            rad_DateTime.Format = DateTimePickerFormat.Custom;
            rad_DateTime.Culture = new System.Globalization.CultureInfo("en-US");
            rad_DateTime.CustomFormat = "MM";
            rad_DateTime.Font = SystemClass.SetFontGernaral;
            rad_DateTime.ForeColor = Color.Blue;
            rad_DateTime.Value = defalut_DateValues;
            // rad_DateTime.Text = defalut_DateValues.ToString();
            rad_DateTime.MaxDate = defalut_DateMax;
        }
        //Set DateTimePicker Time
        public static void SetDefaultFontDateTimePicker_Time(RadDateTimePicker rad_DateTime)
        {
            // DateTime defalut_DateMax
            rad_DateTime.Format = DateTimePickerFormat.Custom;
            rad_DateTime.Culture = new System.Globalization.CultureInfo("en-US");
            rad_DateTime.CustomFormat = "HH:mm:ss";
            rad_DateTime.Font = SystemClass.SetFontGernaral;
            rad_DateTime.ForeColor = Color.Blue;
            rad_DateTime.Format = DateTimePickerFormat.Time;
            rad_DateTime.ShowUpDown = true;
            //rad_DateTime.Value = defalut_DateValues;
            // rad_DateTime.Text = defalut_DateValues.ToString();
            //rad_DateTime.MaxDate = defalut_DateMax;
        }
        //Set Font Head GroupBox
        public static void SetDefaultFontGroupBox(RadGroupBox rad_Groupbox)
        {
            rad_Groupbox.GroupBoxElement.Header.Font = SystemClass.SetFontGernaral_Bold;
            rad_Groupbox.GroupBoxElement.Font = SystemClass.SetFontGernaral;
        }
        // Set fefault DGV
        public static void SetDefaultRadGridView(RadGridView grid)
        {
            //grid.Dock = DockStyle.Fill;

            grid.TableElement.RowHeight = 35;
            grid.TableElement.RowHeaderColumnWidth = 40;

            grid.TableElement.Font = new Font("Tahoma", 12F, FontStyle.Bold, GraphicsUnit.Point, ((byte)(000)));
         
            grid.AutoGenerateColumns = false; //สร้างคอลัมเอง
            grid.ReadOnly = true;
            grid.ShowGroupPanel = false;//ปิดแสดงส่วนของการ group ด้านบนสุด

            grid.EnableAlternatingRowColor = true;//แถวสลับสี

            grid.MasterTemplate.EnableFiltering = true;//การกรองที่หัวกริด
            grid.MasterTemplate.ShowFilterCellOperatorText = false;//แสดงข้อความการค้นหาว่าตามอะไรที่หัสฟิลเลย 

            grid.MasterTemplate.EnableGrouping = true;//แสดงการ Group data (click ขวา)
            grid.MasterTemplate.ShowGroupedColumns = true;//คอลัมที่ Group ให้แสดงด้วย
            grid.MasterTemplate.AutoExpandGroups = true;//แสดงข้อมูลที่ group ทั้งหมด

            grid.MasterTemplate.SelectionMode = GridViewSelectionMode.CellSelect;
            grid.MasterTemplate.ShowHeaderCellButtons = false;//แสดงข้อมูลบนหัวเซลล์ให้ Filter

            grid.TableElement.VScrollBar.ThumbElement.ThumbFill.BackColor = Color.DarkGray;
            grid.TableElement.VScrollBar.ThumbElement.ThumbFill.GradientStyle = Telerik.WinControls.GradientStyles.Solid;

            grid.TableElement.HScrollBar.ThumbElement.ThumbFill.BackColor = Color.DarkGray;
            grid.TableElement.HScrollBar.ThumbElement.ThumbFill.GradientStyle = Telerik.WinControls.GradientStyles.Solid;
            //  grid.MasterTemplate.Sh
            //grid.DataSource = new object[] { }; // set Default คล้ายๆ = ""
            //this.radGridView_Show.Columns["JOB_BRANCHOUTPHUKET"].AllowFiltering = false; // ไม่อนุญาติให้ Fitter ในช่องนี้
            //grid.Refresh();

         
        }
        // Set font DGV
        public static void TreeView_NodeFormatting(object sender, TreeNodeFormattingEventArgs e)
        {
            e.NodeElement.ContentElement.Font = SystemClass.SetFontGernaral;
        }
        // Set font DGV
        public static void SetMenuFont(Telerik.WinControls.RadItemOwnerCollection radItemOwnerCollection)
        {
            foreach (RadMenuItemBase item in radItemOwnerCollection)
            {
                item.Font = SystemClass.SetFontGernaral;
                if (item.HasChildren)
                {
                    SetMenuFont(item.Items);
                }
            }
        }
        // Set font DGV
        public static void SetFormFont(Control.ControlCollection controls)
        {
            foreach (Control c in controls)
            {
                c.Font = SystemClass.SetFontGernaral;
                if (c.HasChildren)
                {
                    SetFormFont(c.Controls);
                }
            }
        }
        //MyJeeds Set DGV : รหัสสาขา
        public static GridViewTextBoxColumn AddTextBoxColumn_BranchID(string _pStr)
        {
            GridViewTextBoxColumn col = new GridViewTextBoxColumn("BRANCH_ID")
            {
                Name = "BRANCH_ID",
                FieldName = "BRANCH_ID",
                HeaderText = _pStr,
                HeaderTextAlignment = ContentAlignment.MiddleLeft,
                TextAlignment = ContentAlignment.MiddleLeft,
                Width = 60
            };

            return col;
        }
        //MyJeeds Set DGV : ชื่อสาขา
        public static GridViewTextBoxColumn AddTextBoxColumn_BranchName(string _pStr)
        {
            GridViewTextBoxColumn col = new GridViewTextBoxColumn("BRANCH_NAME")
            {
                Name = "BRANCH_NAME",
                FieldName = "BRANCH_NAME",
                HeaderText = _pStr,
                HeaderTextAlignment = ContentAlignment.MiddleLeft,
                TextAlignment = ContentAlignment.MiddleLeft,
                Width = 150
            };
            return col;
        }
        //MyJeeds Set DGV : เกี่ยวกับรหัส
        public static GridViewTextBoxColumn AddTextBoxColumn_TYPEID(string fieldName, string labelName)
        {
            GridViewTextBoxColumn col = new GridViewTextBoxColumn(fieldName)
            {
                Name = fieldName,
                FieldName = fieldName,
                HeaderText = labelName,
                HeaderTextAlignment = ContentAlignment.MiddleLeft,
                TextAlignment = ContentAlignment.MiddleLeft,
                Width = 60
            };
            return col;
        }
        //MyJeeds Set DGV : เกี่ยวกับขื่อ
        public static GridViewTextBoxColumn AddTextBoxColumn_TYPENAME(string fieldName, string labelName)
        {
            GridViewTextBoxColumn col = new GridViewTextBoxColumn(fieldName)
            {
                Name = fieldName,
                FieldName = fieldName,
                HeaderText = labelName,
                HeaderTextAlignment = ContentAlignment.MiddleLeft,
                TextAlignment = ContentAlignment.MiddleLeft,
                Width = 150
            };
            return col;
        }
        //MyJeeds Set DGV : เกี่ยวกับขื่อ
        public static GridViewTextBoxColumn AddTextBoxColumn_AddManual(string fieldName, string labelName, int _width)
        {
            GridViewTextBoxColumn col = new GridViewTextBoxColumn(fieldName)
            {
                Name = fieldName,
                FieldName = fieldName,
                HeaderText = labelName,
                HeaderTextAlignment = ContentAlignment.MiddleLeft,
                TextAlignment = ContentAlignment.MiddleLeft,
                Width = _width,
                WrapText = true
            };
            return col;
        }
        //MyJeeds Set DGV : เกี่ยวกับขื่อ 
        public static GridViewTextBoxColumn AddTextBoxColumn_AddManualSetRight(string fieldName, string labelName, int _width)
        {
            GridViewTextBoxColumn col = new GridViewTextBoxColumn(fieldName)
            {
                Name = fieldName,
                FieldName = fieldName,
                HeaderText = labelName,
                HeaderTextAlignment = ContentAlignment.MiddleCenter,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = _width,
                WrapText = true
            };
            return col;
        }
        //MyJeeds Set DGV : เกี่ยวกับขื่อ 
        public static GridViewTextBoxColumn AddTextBoxColumn_AddManualSetCenter(string fieldName, string labelName, int _width)
        {
            GridViewTextBoxColumn col = new GridViewTextBoxColumn(fieldName)
            {
                Name = fieldName,
                FieldName = fieldName,
                HeaderText = labelName,
                HeaderTextAlignment = ContentAlignment.MiddleCenter,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = _width,
                WrapText = true
            };
            return col;
        }
        //MyJeeds Set DGV : Visible
        public static GridViewTextBoxColumn AddTextBoxColumn_Visible(string fieldName, string labelName)
        {
            GridViewTextBoxColumn col = new GridViewTextBoxColumn(fieldName)
            {
                Name = fieldName,
                FieldName = fieldName,
                HeaderText = labelName,
                HeaderTextAlignment = ContentAlignment.MiddleLeft,
                TextAlignment = ContentAlignment.MiddleLeft,
                Width = 20,
                IsVisible = false
            };
            return col;
        }
        //MyJeeds Set DGV : CheckBox
        public static GridViewCheckBoxColumn AddCheckBoxColumn_TYPEID(string fieldName, string labelName)
        {
            GridViewCheckBoxColumn col = new GridViewCheckBoxColumn(fieldName)
            {
                Name = fieldName,
                FieldName = fieldName,
                HeaderText = labelName,
                HeaderTextAlignment = ContentAlignment.MiddleCenter,
                TextAlignment = ContentAlignment.MiddleLeft,

                Width = 80
            };
            return col;
        }
        //MyJeeds Set DGV : CheckBox
        public static GridViewCheckBoxColumn AddCheckBoxColumn_AddManual(string fieldName, string labelName, int _width)
        {
            GridViewCheckBoxColumn col = new GridViewCheckBoxColumn(fieldName)
            {
                Name = fieldName,
                FieldName = fieldName,
                HeaderText = labelName,
                HeaderTextAlignment = ContentAlignment.MiddleCenter,
                TextAlignment = ContentAlignment.MiddleLeft,

                Width = _width
            };
            return col;
        }
        //MyJeeds Set DGV : CheckBox
        public static GridViewCheckBoxColumn AddCheckBoxColumn_AddVisible(string fieldName, string labelName, int _width)
        {
            GridViewCheckBoxColumn col = new GridViewCheckBoxColumn(fieldName)
            {
                Name = fieldName,
                FieldName = fieldName,
                HeaderText = labelName,
                HeaderTextAlignment = ContentAlignment.MiddleCenter,
                TextAlignment = ContentAlignment.MiddleLeft,
                IsVisible = false,
                Width = _width
            };
            return col;
        }
        // MyJeeds Set DGV : MaskTextbox
        public static GridViewMaskBoxColumn AddMaskTextBoxColum_AddManual(string fieldName, string labelName, int _width)
        {
            GridViewMaskBoxColumn col = new GridViewMaskBoxColumn(fieldName)
            {
                Name = fieldName,
                FieldName = fieldName,
                HeaderText = labelName,
                HeaderTextAlignment = ContentAlignment.MiddleLeft,
                TextAlignment = ContentAlignment.MiddleLeft,
                Width = _width
            };
            return col;
        }
        //MyJeeds Set DGV : textbox For CheckBox
        public static GridViewCheckBoxColumn AddTextBoxColumn_CheckBox(string fieldName, string labelName, int _width)
        {
            GridViewCheckBoxColumn col = new GridViewCheckBoxColumn(fieldName)
            {
                Name = fieldName,
                FieldName = fieldName,
                HeaderText = labelName,
                HeaderTextAlignment = ContentAlignment.MiddleCenter,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = _width,
                DataType = typeof(string)
            };
            return col;
        }
        //MyJeeds Set DGV : Image
        public static GridViewImageColumn AddTextBoxColumn_Image(string fieldName, string labelName, int _width, ImageLayout _pLayout = ImageLayout.Zoom)
        {

            GridViewImageColumn col = new GridViewImageColumn(fieldName)
            {
                Name = fieldName,
                FieldName = fieldName,
                HeaderText = labelName,
                HeaderTextAlignment = ContentAlignment.MiddleCenter,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = _width,
                WrapText = true,
                ImageLayout = _pLayout,
                ImageAlignment = ContentAlignment.MiddleCenter
            };
            return col;
        }

        //MyJeeds Set DGV : เกี่ยวกับขื่อ 
        public static GridViewDecimalColumn AddNumberColumn_AddManualSetCenter(string fieldName, string labelName, int _width)
        {
            GridViewDecimalColumn col = new GridViewDecimalColumn(fieldName)
            {
                Name = fieldName,
                FieldName = fieldName,
                HeaderText = labelName,
                HeaderTextAlignment = ContentAlignment.MiddleCenter,
                TextAlignment = ContentAlignment.MiddleCenter,
                Width = _width,
                WrapText = true,
                FormatString = "{0:N2}",
                NullValue = 0,
                DataSourceNullValue = 0,
                DataType = typeof(System.Double),
                ShowUpDownButtons = true,
                AllowFiltering = false
                //ReadOnly = false,IsVisible = true, //Minimum = -30,                Maximum = -3,
            };
            return col;
        }
        //MyJeeds Set DGV : เกี่ยวกับขื่อ 
        public static GridViewDecimalColumn AddNumberColumn_AddManualSetRight(string fieldName, string labelName, int _width)
        {
            GridViewDecimalColumn col = new GridViewDecimalColumn(fieldName)
            {
                Name = fieldName,
                FieldName = fieldName,
                HeaderText = labelName,
                HeaderTextAlignment = ContentAlignment.MiddleRight,
                TextAlignment = ContentAlignment.MiddleRight,
                Width = _width,
                WrapText = true,
                FormatString = "{0:N2}",
                NullValue = 0,
                DataSourceNullValue = 0,
                DataType = typeof(System.Double),
                ShowUpDownButtons = true,
                AllowFiltering = false
                //ReadOnly = false,IsVisible = true, //Minimum = -30,                Maximum = -3,
            };
            return col;
        }
        //MyJeeds Set DGV : เกี่ยวกับขื่อ 
        public static GridViewDecimalColumn AddNumberColumn_AddVisible(string fieldName, string labelName)
        {
            GridViewDecimalColumn col = new GridViewDecimalColumn(fieldName)
            {
                Name = fieldName,
                FieldName = fieldName,
                HeaderText = labelName,
                HeaderTextAlignment = ContentAlignment.MiddleCenter,
                TextAlignment = ContentAlignment.MiddleCenter,
                WrapText = true,
                FormatString = "{0:N2}",
                NullValue = 0,
                DataSourceNullValue = 0,
                DataType = typeof(System.Double),
                ShowUpDownButtons = true,
                IsVisible = false,
                AllowFiltering = false
                //ReadOnly = false,IsVisible = true, //Minimum = -30,                Maximum = -3,
            };
            return col;
        }
        //MyJeeds Set DGV HyperLink
        public static GridViewHyperlinkColumn AddHyperlinkColumn_AddManual(string fieldName, string labelName, int _width)
        {
            GridViewHyperlinkColumn col = new GridViewHyperlinkColumn(fieldName)
            {
                Name = fieldName,
                FieldName = fieldName,
                HeaderText = labelName,
                HeaderTextAlignment = ContentAlignment.MiddleLeft,
                TextAlignment = ContentAlignment.MiddleLeft,
                Width = _width,
                WrapText = true,
                ReadOnly = true
            };
            return col;
        }
      
        //Set Font For Export Excel เท่านั้น โดยที่ไม่เกี่ยวกับ DGV ที่แสดงผล
        public static void SpreadExporter_CellFormatting(object sender, Telerik.WinControls.Export.CellFormattingEventArgs e)
        {
            e.CellStyleInfo.FontFamily = new FontFamily("Tahoma");
            e.CellStyleInfo.FontSize = 10;

            //
            if (e.GridRowInfoType == typeof(GridViewTableHeaderRowInfo))
            {
                if (e.GridCellInfo.RowInfo.HierarchyLevel == 0)
                {
                    e.CellStyleInfo.BackColor = Class.ConfigClass.SetColor_PurplePastel();
                    e.CellStyleInfo.IsBold = true;
                    
                }
                //else if (e.GridCellInfo.RowInfo.HierarchyLevel == 1)
                //{
                //    e.CellStyleInfo.BackColor = Color.LightSkyBlue;
                //}
            }

            //if (e.GridRowInfoType == typeof(GridViewHierarchyRowInfo))
            //{
            //    if (e.GridCellInfo.RowInfo.HierarchyLevel == 0)
            //    {
            //        e.CellStyleInfo.IsItalic = true;
            //        e.CellStyleInfo.FontSize = 12;
            //        e.CellStyleInfo.BackColor = Color.GreenYellow;
            //    }
            //    else if (e.GridCellInfo.RowInfo.HierarchyLevel == 1)
            //    {
            //        e.CellStyleInfo.ForeColor = Color.DarkGreen;
            //        e.CellStyleInfo.BackColor = Color.LightGreen;
            //    }
            //}
        }
        //นับจำนวนตัวอักษรของภาษาไทย โดยที่ไม่รวมสระ
        public static int ThaiLength(string stringthai)
        {
            int len = 0;
            int l = stringthai.Length;
            for (int i = 0; i < l; ++i)
            {
                if (char.GetUnicodeCategory(stringthai[i]) != System.Globalization.UnicodeCategory.NonSpacingMark)
                    ++len;
            }
            return len;
        }
        //SetTextBoxReadonly&&SetText
        public static void SetTextControls(Control.ControlCollection controlCollection, int Collection)
        {
            if (controlCollection == null)
            {
                return;
            }
            switch (Collection)
            {
                case 0:
                    foreach (RadLabel c in controlCollection.OfType<RadLabel>())
                    {
                        //c.ReadOnly = true;
                        c.Font = SystemClass.SetFontGernaral;
                        c.ForeColor = Class.ConfigClass.SetColor_Blue();
                    }
                    break;
                case 1:
                    foreach (RadTextBoxBase c in controlCollection.OfType<RadTextBoxBase>())
                    {
                        //c.ReadOnly = true;
                        c.Font = SystemClass.SetFontGernaral;
                        c.ForeColor = Class.ConfigClass.SetColor_Blue();
                    }
                    break;
                case 2:
                    foreach (RadCheckBox c in controlCollection.OfType<RadCheckBox>())
                    {
                        //c.ReadOnly = true;
                        c.ButtonElement.TextElement.Font = SystemClass.SetFontGernaral;
                        c.ForeColor = Class.ConfigClass.SetColor_Blue();
                    }
                    break;
                case 3:
                    foreach (RadRadioButton c in controlCollection.OfType<RadRadioButton>())
                    {
                        //c.ReadOnly = true;
                        c.Font = SystemClass.SetFontGernaral;
                        c.ForeColor = Class.ConfigClass.SetColor_Blue();
                    }
                    break;
                case 4:
                    foreach (RadDropDownList c in controlCollection.OfType<RadDropDownList>())
                    {
                        //c.ReadOnly = true;
                        c.ListElement.Font = SystemClass.SetFontGernaral;
                        c.DropDownListElement.Font = SystemClass.SetFontGernaral;
                        c.DropDownListElement.ForeColor = Class.ConfigClass.SetColor_Blue();
                    }
                    break;
                default:
                    break;
            }

        }
        //
        public static void SetPrintDocument(RadPrintDocument document, string headerTxt, int numberPrint)
        {
            document.HeaderHeight = 30;
            document.HeaderFont = new Font("Tahoma", 12, FontStyle.Bold);
            //document.Logo = System.Drawing.Image.FromFile(@"C:\MyLogo.png");
            document.MiddleHeader = headerTxt;
            //document.MiddleHeader = "Middle header";
            //document.MiddleHeader = "Printed on [Date Printed] [Time Printed].";
            //document.RightHeader = "Right header";
            document.ReverseHeaderOnEvenPages = true;
            // " Login [User Name]
            document.FooterHeight = 30;
            document.FooterFont = new Font("Tahoma", 10, FontStyle.Regular);
            document.MiddleFooter = "พิมพ์ครั้งที่ " + numberPrint.ToString() + "  หน้า [Page #]/[Total Pages]    [Date Printed] [Time Printed] ";//"Left footer";

            document.Watermark.Text = "SHOP24HRS";
            document.Watermark.TextHOffset = 0;
            document.Watermark.TextVOffset = 1120;
            document.Watermark.TextAngle = 302;
            document.Watermark.TextOpacity = 30;

            document.DefaultPageSettings.Margins = new System.Drawing.Printing.Margins(40, 40, 30, 30);
            document.DefaultPageSettings.PaperSize = new System.Drawing.Printing.PaperSize("A4", 826, 1169);

            document.ReverseFooterOnEvenPages = true;
        }
        
        //    //MyJeeds 
        //    RadGridView A = new RadGridView();
        //    //A.Name = "AAAAA";

        //    List<RadGridView> grigCollection = new List<RadGridView> { };

        //        for (int i = 0; i< 3; i++)
        //        {
        //            A = new RadGridView();
        //    A.Name = "B" + i.ToString();
        //            DatagridClass.SetDefaultRadGridView(A);
        //            A.MasterTemplate.Columns.Add((DatagridClass.AddTextBoxColumn_AddManual("INVENTLOCATIONID", "สาขา " + i.ToString(), 80)));
        //            grigCollection.Add(A);
        //        }

        //string v = grigCollection[0].Columns[0].HeaderText;
        //v = grigCollection[1].Columns[0].HeaderText;
        //        v = grigCollection[2].Columns[0].HeaderText;

        //List<RadGridView> A = new List<RadGridView> { };
        //A.Add(radGridView_Main);
        //        A.Add(radGridView_Sub);

        //        GridViewSpreadExport spreadExporter;
        //SpreadExportRenderer exportRenderer;
        //        for (int i = 0; i< 2; i++)
        //        {
        //            spreadExporter = new GridViewSpreadExport(A[i]);
        //exportRenderer = new SpreadExportRenderer();

        //spreadExporter.HiddenColumnOption = Telerik.WinControls.UI.Export.HiddenOption.DoNotExport; //คอลัมที่ hide ไว้ ไม่ต้อง export

        //            spreadExporter.HiddenRowOption = Telerik.WinControls.UI.Export.HiddenOption.DoNotExport;//แถวที่ hide ไว้ไม่ต้อง export
        //            spreadExporter.ExportVisualSettings = true;
        //            spreadExporter.RunExport(saveDia.FileName, exportRenderer, A[i].Name);
        //        }

    }
}
