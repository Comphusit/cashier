﻿
using System;
using System.Data;
using System.Drawing;
using System.Globalization;


namespace Cashier.Class
{
    class ConfigClass
    {

        //My Jeeds ค้นหาเลขที่บิลล่าสุด 0 = ประเภทของบิล 1 = เลขที่บิลขึ้นหน้า
        public static string GetMaxINVOICEID(string _pType, string _pPOS, string _pFrist, string _pCase)
        {
            string strBillID = string.Format(@" [config_SHOP_GENARATE_BILLAUTO] '" + _pType + "','" + _pPOS + "','" + _pFrist + "','" + _pCase + "'");
            DataTable dt = ConnectionClass.SelectSQL_Main(strBillID);
            return dt.Rows[0]["ID"].ToString();
        }
        ////My Jeeds ค้นหาในตาราง SHOP_CONFIGBRANCH_GenaralDetail ตาม typeID
        //public static DataTable FindData_SHOP_CONFIGBRANCH_GenaralDetail_ByTypeID(string _pTypeID)
        //{
        //    string strBillID = string.Format(@"SELECT	*	FROM	SHOP_CONFIGBRANCH_GenaralDetail WITH (NOLOCK)
        //            WHERE	TYPE_CONFIG = '" + _pTypeID + @"'
        //            ORDER BY SHOW_NAME");
        //    DataTable dt = ConnectionClass.SelectSQL_Main(strBillID);
        //    return dt;
        //}

        //Setcolor ม่วง
        public static Color SetColor_PurplePastel()
        {
            Color myRgbColor = Color.FromArgb(198, 146, 225);
            return myRgbColor;
        }
        //Setcolor ม่วง
        public static Color SetColor_SkyPastel()
        {
            Color myRgbColor = Color.FromArgb(154, 225, 255);
            return myRgbColor;
        }
        //Setcolor เขียว Pastel
        public static Color SetColor_GreenPastel()
        {
            //Color myRgbColor = new Color();
            Color myRgbColor = Color.FromArgb(173, 244, 215);
            return myRgbColor;
        }
        //Setcolor เขียว
        public static Color SetColor_Green()
        {
            //  Color myRgbColor = new Color();
            Color myRgbColor = Color.FromArgb(0, 128, 0);
            return myRgbColor;
        }
        //Setcolor ชมพู
        public static Color SetColor_PinkPastel()

        {
            Color myRgbColor = Color.FromArgb(255, 203, 226);
            return myRgbColor;
        }
        //Setcolor แดง
        public static Color SetColor_Red()

        {
            //  Color myRgbColor = new Color();
            Color myRgbColor = Color.FromArgb(255, 0, 0);
            return myRgbColor;
        }
        //Setcolor เหลือง
        public static Color SetColor_YellowPastel()

        {
            //Color myRgbColor = new Color();
            Color myRgbColor = Color.FromArgb(228, 219, 124);
            return myRgbColor;
        }
        //Setcolor เทา
        public static Color SetColor_GrayPastel()

        {
            // Color myRgbColor = new Color();
            Color myRgbColor = Color.FromArgb(190, 187, 188);
            return myRgbColor;
        }
        //Setcolor ม่วงเข้ม
        public static Color SetColor_DarkPurplePastel()

        {
            //Color myRgbColor = new Color();
            Color myRgbColor = Color.FromArgb(137, 76, 169);
            return myRgbColor;
        }
        //Setcolor น้ำเงิน
        public static Color SetColor_Blue()
        {
            //Color myRgbColor = new Color();
            Color myRgbColor = Color.Blue;
            return myRgbColor;
        }
        //Setcolor BlueViolet
        public static Color SetColor_Wheat()
        {
            //Color myRgbColor = new Color();
            Color myRgbColor = Color.Wheat;
            return myRgbColor;
        }
        //
        public static Color SetColor_Black()
        {
            Color myRgbColor = Color.Black;
            return myRgbColor;
        }
        //SetBackColor_F1
        public static Color SetBackColor_F1()
        {
            Color myRgbColor = Color.LightBlue;
            return myRgbColor;
        }
        //SetBackColor_F2
        public static Color SetBackColor_F2()
        {
            Color myRgbColor = Color.LightPink;
            return myRgbColor; 
        }
        //SetBackColor_F2
        public static Color SetBackColor_F3()
        {
            Color myRgbColor = Color.BlanchedAlmond;
            return myRgbColor; 
        }
        //SetBackColor_F4
        public static Color SetBackColor_F4()
        {
            Color myRgbColor = Color.FromArgb(192, 255, 192);
            return myRgbColor;
        }

        //SetBackColor_F6
        public static Color SetBackColor_F6()
        {
            Color myRgbColor = Color.FromArgb(247, 208, 122);
            return myRgbColor;
        }
        //SetBackColor_F7
        public static Color SetBackColor_F7()
        {
            Color myRgbColor = Color.Gainsboro;
            return myRgbColor;
        }
        //Input Number only
        public static bool IsNumber(string text)
        {
            bool res = true;
            try
            {
                if (!string.IsNullOrEmpty(text) && ((text.Length != 1) || (text != "-")))
                {
                    Decimal d = decimal.Parse(text, CultureInfo.CurrentCulture);
                }
            }
            catch
            {
                res = false;
            }
            return res;
        }

    }
}


