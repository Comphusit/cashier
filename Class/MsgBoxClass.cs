﻿
using System.Windows.Forms;

namespace Cashier.Class
{
    class MsgBoxClass
    {
        //private string MessageString;

        //private void MassageBoxString(String TypeString)
        //{

        //    switch (TypeString)
        //    {
        //        case "01":
        //            MessageString = "บันทึกเรียบร้อยแล้ว";
        //            break;
        //        case "02":
        //            MessageString = "แก้ไขเรียบร้อยแล้ว";
        //            break;
        //        case "03":
        //            MessageString = "ยกเลิกเรียบร้อยแล้ว";
        //            break;
        //        case "04":
        //            MessageString = "ข้อผิดพลาด";
        //            break;
        //        case "05":
        //            MessageString = "ค้นหาลูกค้าก่อนบันทึกเอกสารใหม่";
        //            break;
        //        default:
        //            MessageString = "";
        //            break;
        //    }
        //}

        //Msg Waring
        public static void MassageBoxShowButtonOk_Warning(string Message, string HeadMsg)
        {
            MessageBox.Show(Message,
                            HeadMsg, MessageBoxButtons.OK, MessageBoxIcon.Warning);
        }
        //Msg Error
        public static void MassageBoxShowButtonOk_Error(string Message, string HeadMsg)
        {
            MessageBox.Show(Message,
                             HeadMsg, MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
        //Msg InFormation
        public static void MassageBoxShowButtonOk_Information(string Message, string HeadMsg)
        {
            MessageBox.Show(Message,
                             HeadMsg, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        //Msg YesNo
        public static DialogResult MassageBoxShowYesNo_DialogResult(string Message, string HeadMsg)
        {
            DialogResult dialogResult = MessageBox.Show(Message, HeadMsg, MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            return dialogResult;
        }
    }
}
