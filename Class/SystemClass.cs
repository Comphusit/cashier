﻿

using System.Drawing;


namespace Cashier.Class
{
    class SystemClass
    {
        //public static string SystemPurchaseFresh = " 'D110','D164','D032','D106','D067','D144' ";

        //// เกี่ยวกับ user
        public static string SystemUserID = "";
        //public static string SystemUserID_M = "";
        public static string SystemUserName = "";
        //public static string SystemUserNameShort = "";
        //public static string SystemUserPositionID = "";
        //public static string SystemUserPositionName = "";  
        public static string SystemPOSNUMBER = "";
        public static string SystemPOSGROUP = "";
        public static string SystemLOCATIONID = "";
        public static string SystemZONEID = "";//
        public static string SystemPRICEGROUPID = "";
        public static string SystemPRICEFIELDNAME = "";
        public static string SystemRecID = "";
        public static string SystemPermissionFreeItem = "";

        //// เกี่ยวกัยสาขา
        //public static string SystemBranchID = "";// รหัสสาขาที่เข้าใช้งาน
        //public static string SystemBranchName = "";// ชื่อสาขาที่ใช้งาน
        //public static string SystemBranchPrice = ""; // ระดับราคาของสาขาที่เข้าใช้งาน

        //public static string SystemBranchServer = ""; // ชื่อ server สาขาที่ใช้
        //public static string SystemBranchOutPhuket = ""; // สาขาในจังหวัด 1 สาขาต่างจังหวัด 0
        //public static string SystemBranchInventID = "";// คลังของสาขาที่ใช้งาน
        ////public static string SystemServerIP = ""; // IP เครื่อง

        // Database Shop2013
        public static string SystemShop2013 = "Shop2013.dbo."; // 

        // เกี่ยวกับเครื่อง+IP
        public static string SystemPcName = ""; // ชื่อ SPC
        public static string SystemPcIp = ""; // IP เครื่อง 

        //Header MassageBox
        public static string HeaderMassage = "CashDesktop";
        public static string HeaderSmartShop = "ลูกค้าเครดิต";
        public static string HeaderOrder = "ออร์เดอร์ลูกค้า";
        public static string HeaderBuyCust = "รับซื้อสินค้าลูกค้า";
        public static string HeaderMNPI = "รับอาหารกล่อง";
        public static string HeaderFreeItem = "แจกของแถมลูกค้า";

        //public static string HeaderPoint = "แลกแต้ม.";
        //public static string HeaderCust = "สมัครสมาชิก";


        //public static string DateNow =DateTime.Now.Date.ToShortDateString() ;  


        //public static string Versions;
        ////เกี่ยวกับการ Update โปรแกรม
        //public static string SystemVersionShop24Hrs_SUPC = ""; // เวอร์ชั่นการใช้งาน SUPC
        //public static string SystemVersionShop24Hrs_SHOP = ""; // เวอร์ชั่นการใช้งาน SHOP
        //public static string SystemPathUpdateShop24Hrs_SUPC = ""; // เวอร์ชั่นการใช้งาน SUPC
        //public static string SystemPathUpdateShop24Hrs_SHOP = ""; // เวอร์ชั่นการใช้งาน SHOP
        public static string SystemVersion = "1.0.0.2";//Version
        //public static string SystemVersionShop24HrsCurrent_SUPC = "65"; // เวอร์ชั่นการใช้งาน SUPC Current
        //public static string SystemVersionShop24HrsCurrent_SHOP = "0"; // เวอร์ชั่นการใช้งาน SHOP Current

        //// เกี่ยวกับการตั้งค่าพิเศษ 
        //public static string SystemComMinimart = "";// สำหรับ D179 ComMN
        //public static string SystemComService = ""; // สำหรับ D179 COmService
        public static string SystemComProgrammer = ""; // ในกรณีที่เป็นโปรแกรมเมอร์
        ////การตั้งค่าหลักที่ไม่เปลี่ยนแปลง
        //public static string SystemHeadprogram = "Shop24Hrs."; // ชื่อโปแกรมหลัก คือ Shop24Hrs.
        //public static string TmpAX = "AX60CU13.dbo."; // Database AX ใน SPC119SRV

        //Set Font DGV
        public static Font SetFontGernaral = new Font(new FontFamily("Tahoma"), 10.0f);
        public static Font SetFontGernaral_Bold = new Font(new FontFamily("Tahoma"), 10.0f, FontStyle.Bold);
        public static Font SetFont12 = new Font(new FontFamily("Tahoma"), 12.0f);
        public static Font printFont = new Font("Tahoma", 10, System.Drawing.FontStyle.Regular);
        public static Font printFont15 = new Font(new FontFamily("Tahoma"), 15);

        ////แผนกจัดซื้ออาหารสด MNOG
        public static string SystemPurchaseFresh = "";
        ////บาร์โค้ดลดราคาอาหารกล่อง
        //public static string SystemBarcodeType17 = "";
        //public static string SystemBarcodeType19 = "";
        //public static string SystemBarcodeType20 = "";


        public static string pServerAX = "Data Source=192.168.100.58;Initial Catalog=AX50SP1_SPC;User Id=shop24;Password=Ca999999";
        public static string pServerPOS = "Data Source=192.168.100.57;Initial Catalog=MNPOS_SPC;User Id=shop24;Password=Ca999999";

        //ตัวแปรของระบบ SmartShop
        public static string SmartShop_CstID = "";//รหัสลูกค้า
        public static string SmartShop_CstName = "";//ชื่อลูกค้า
        public static string SmartShop_CardID = "";//รหัสบัตร ปชช
        public static string SmartShop_CardName = "";//ชื่อผู้รับสินค้า
        public static double SmartShop_CreditMax = 0;//วงเงินคงเหลือที่ลูกค้าสามารถรับสินค้าได้ในแต่ละวัน
        public static double SmartShop_CreditLimit = 0;//วงเงินคงเหลือของลูกค้า
        //public static double SmartShop_CstMax = 5000;//วงเงินที่ลูกค้าสามารถรับสินค้าได้ในแต่ละวัน สาขาใหญ่+ร้านยา 15000 สำหรับทุกไซต์คือ 5000
        public static double SmartShop_CstIDMax = 5000;//วงเงินที่ลูกค้าสามารถรับสินค้าได้ในแต่ละวัน 
        public static string SmartShop_SPC_PrintTaxInvoice = "";// ลูกค้ามีภาษี หรือ ไม่มี
        public static string SmartShop_Term = "";//จำนวนวันเครดิต
        public static string SmartShop_EmplARResponsible = "";//พนักงานที่รับผิดชอบลูกค้า
        public static string SmartShop_SyChannel = "";//ช่องทางการขาย
        public static string SmartShop_SyPOSPRICE = "";//ระดับราคาขาย

    }
}
