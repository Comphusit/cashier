﻿using System.Diagnostics;
using System.Linq;
using System.Data;
using System.Windows.Forms;

namespace Cashier.Class
{
    class ProcessAndLoginClass
    {
        //CheckLogin SPC_POS
        public static void CheckPOS()
        {
            Process[] process = Process.GetProcessesByName("SPC.POS");
            if (process.Count() == 0)
            {
                Process[] process1 = Process.GetProcessesByName("Retail POS");
                if (process1.Count() ==0)
                {
                    MsgBoxClass.MassageBoxShowButtonOk_Error("โปรแกรมไม่สามารถใช้งานได้เพราะโปรแกรมจะต้องใช้งานคู่กับโปรแกรมขายเท่านั้น.", SystemClass.HeaderMassage);
                    Application.Exit();
                }
            }
        }
        //Get User จาก SPC
        public static DataTable DtGetMachine(string MachineSPC)
        {
            //DataTable Dt;
            //string Sql;

            //Sql = String.Format(@"
            //    SELECT	POSTABLE.POSNUMBER ,POSTABLE.POSGROUP ,POSLOGINOPEN.LOCATIONID , 
            //      POSLOGINOPEN.ZONEID ,PRICEGROUPID, PRICEFIELDNAME,POSLOGINOPEN.EMPLID,POSLOGINOPEN.NAME AS SPC_NAME
            //    FROM	SHOP2013TMP.dbo.POSLOGINOPEN WITH (NOLOCK)
            //       INNER JOIN SHOP2013TMP.dbo.POSTABLE WITH (NOLOCK) ON POSLOGINOPEN.POSNUMBER =POSTABLE.POSNUMBER  
            //       INNER JOIN SHOP2013TMP.dbo.POSGROUP WITH (NOLOCK) ON  POSGROUP.POSGROUP =POSTABLE.POSGROUP  
            //    WHERE	POSTABLE.MACHINENAME ='{0}'
            //      AND CONVERT(VARCHAR,TRANSDATE,23) =CONVERT(VARCHAR,GETDATE(),23)
            //      AND POSLOGINOPEN.TRANSSTATUS not in ('0')  ", MachineSPC);

            return ConnectionClass.SelectSQL_Main(" CashDesktop_CheckLogin '" + MachineSPC + @"' ");
        }

        // ค้นหา พนักงาน รายบุคคล ยึด AX
        public static DataTable Emp_ByID(string EmpID)
        {
            return ConnectionClass.SelectSQL_Main(" EmplClass_GetEmployee '" + EmpID + @"' ");
        }

    }
}
