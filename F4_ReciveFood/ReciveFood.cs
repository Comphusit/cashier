﻿using System;
using System.Data;
using Cashier.Class;
using System.Collections;

namespace Cashier.F4_ReciveFood
{
    class ReciveFood
    {
        // ค้นหาข้อมูลการรับ
        public static DataTable GetBarcodeDetail_ByBarcode(string barcode, string posgroup)
        {
            return ConnectionClass.SelectSQL_Main(" CashDesktop_ReciveFood '0','" + barcode + @"','" + posgroup + @"' ");
        }
        //ค้นหาบิล เพื่อส่งค่าเข้า AX
        public static DataTable FindBillDetail_Docno(string docno)
        {
            return ConnectionClass.SelectSQL_Main(" CashDesktop_ReciveFood '1','" + docno + @"','' ");
        }
        //Send To AX
        public static ArrayList SqlSendAX(DataTable dt)
        {
            ArrayList sqlInAX = new ArrayList
                        {
                            String.Format(@"
                                INSERT INTO [SPC_POSTOTABLE20] ( 
                                FREIGHTSLIPTYPE,CASHIERID,ALLPRINTSTICKER,DATAAREAID,RECVERSION,RECID,TRANSFERSTATUS,
                                INVENTLOCATIONIDTO, INVENTLOCATIONIDFROM, TRANSFERID, SHIPDATE, VOUCHERID,EMPLPICK,EMPLCHECKER)
                                VALUES ('98','" + dt.Rows[0]["WHOINS"].ToString()  + @"','0','SPC','1','1','2',
                                '" + dt.Rows[0]["BRANCH_ID"].ToString() + @"','" + dt.Rows[0]["INVENT"].ToString() + @"',
                                '" + dt.Rows[0]["DOCNO"].ToString() + @"',CONVERT(VARCHAR,GETDATE(),23),'" + dt.Rows[0]["DOCNO"].ToString() + @"',
                                '','' ) ")
                        };

            int iRow = 1;
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (Convert.ToDouble(dt.Rows[i]["QTYORDER"].ToString()) > 0)
                {
                    sqlInAX.Add(string.Format(@"
                            INSERT INTO [SPC_POSTOLINE20] (RECID,REFBOXTRANS,DATAAREAID,VOUCHERID,SHIPDATE,LINENUM,QTY,
                            ITEMID,NAME,SALESUNIT,ITEMBARCODE,INVENTDIMID,INVENTTRANSID,RECVERSION)  
                            VALUES ('1','0','SPC','" + dt.Rows[0]["DOCNO"].ToString() + @"',CONVERT(VARCHAR,GETDATE(),23),
                            '" + (iRow) + @"',
                            '" + Convert.ToDouble(dt.Rows[i]["QTYORDER"].ToString()) + @"',
                            '" + dt.Rows[i]["ITEMID"].ToString() + @"',
                            '" + dt.Rows[i]["SPC_NAME"].ToString().Replace("{", "").Replace("}", "").Replace("'", "") + @"',
                            '" + dt.Rows[i]["UNITID"].ToString() + @"',
                            '" + dt.Rows[i]["ITEMBARCODE"].ToString() + @"',
                            '" + dt.Rows[i]["INVENTDIMID"].ToString() + @"',
                            '" + dt.Rows[0]["DOCNO"].ToString() + "_" + (iRow) + @"','1')
                        "));
                    iRow++;
                }
            }

            return sqlInAX;
        }
        //FindForPrint
        public static DataTable FindBillForPrint_Docno(string docno)
        {
            return ConnectionClass.SelectSQL_Main(" CashDesktop_ReciveFood '2','" + docno + @"','' ");
        }
        //Sql Update Bill Apv 24
        public static ArrayList SqlUpdateBill_Apv(string docno)
        {
            ArrayList sql24 = new ArrayList
                {
                    @"
                    UPDATE  SHOP_MNPI_HD
                    SET     STADOC = '1',STAPRCDOC = '1' 
                    WHERE   DOCNO = '" + docno + @"'
                "
                };
            return sql24;
        }
        //Update Status Bill Void
        public static string UpdateStatus_Void(string docno, string linenum)
        {
            ArrayList sqlUp = new ArrayList
            {String.Format(@"
                UPDATE  SHOP_MNPI_DT 
                SET     STA = '0' ,QTYORDER  = '0'
                WHERE   DOCNO = '" + docno + @"' AND LINENUM = '" + linenum + @"' ")
            };
            return ConnectionClass.ExecuteSQL_ArrayMain(sqlUp);
        }
        //SaveData To DB
        public static string SaveData(string docno, double qty, int iRowDT, DataTable dtBarcode)
        {
            ArrayList sql = new ArrayList();
            if (iRowDT == 1)
            {
                sql.Add(String.Format(@"
                                INSERT INTO [SHOP_MNPI_HD] ( 
                                    [DOCNO],[WHOINS],[WHOINSNAME],
                                    [BRANCH_ID],[BRANCH_NAME],
                                    [MACHINENAME],[STADOC],[STAPRCDOC],[TYPE_CONFIG],[REMARK]) 
                                VALUES ('" + docno + @"','" + SystemClass.SystemUserID + @"','" + SystemClass.SystemUserName + @"',
                                    '" + SystemClass.SystemPOSGROUP + @"', '" + SystemClass.SystemZONEID + @"',
                                    '" + SystemClass.SystemPcName + @"','0','0','19','CASHIER') "));
            }

            sql.Add(String.Format(@"
                            INSERT INTO [SHOP_MNPI_DT] ( 
                                [DOCNO],[LINENUM],[ITEMBARCODE],[SPC_NAME],[STA] ,[QTYORDER],[UNITID],[ITEMID],[INVENTDIMID],[INVENT]) 
                            VALUES ('" + docno + @"','" + iRowDT + @"',
                                '" + dtBarcode.Rows[0]["ITEMBARCODE"].ToString() + @"',
                                '" + dtBarcode.Rows[0]["SPC_ITEMNAME"].ToString().Replace("{", "").Replace("}", "").Replace("'", "") + @"',
                                '1','" + qty + @"',
                                '" + dtBarcode.Rows[0]["UNITID"].ToString() + @"',
                                '" + dtBarcode.Rows[0]["ITEMID"].ToString() + @"',
                                '" + dtBarcode.Rows[0]["INVENTDIMID"].ToString() + @"',
                                'RETAILAREA') "));


            return ConnectionClass.ExecuteSQL_ArrayMain(sql);
        }
    }
}
