﻿using System;
using System.Windows.Forms;
using Cashier.Class;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;

namespace Cashier.F4_ReciveFood
{
    public partial class ReciveFood_Main : Telerik.WinControls.UI.RadForm
    {
        private readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();
        readonly PrintController printController = new StandardPrintController();
        //private string pCopyDesc;
        //private int pCopy;
        //    string pCaseYa = "0";//กรณีช่องทางการขาย 35 และ 37

        int iCountItem = 0;
        int iRowDT = 0;
        readonly DataTable dtGrid = new DataTable();
        public ReciveFood_Main()
        {
            InitializeComponent();
        }
        //Clear
        void ClearData()
        {
            if (dtGrid.Rows.Count > 0) dtGrid.Rows.Clear();

            iRowDT = 0;
            iCountItem = 0;
            //pCopyDesc = "";
            radLabel_txt.Visible = false; radLabel_Grand.Visible = false;
            radLabel_CustName.Text = "";
            radLabel_Docno.Text = "";
            radLabel_ReciveID.Text = "";
            radLabel_ReciveName.Text = "";
            radTextBox_Barcode.Text = "";
            radLabel_X.Visible = false;
            radLabel_Qty.Visible = false; radLabel_Qty.Text = "1.00";
            radLabel_Grand.Text = "0.00";
            radTextBox_Barcode.Text = "";
            radTextBox_Barcode.Focus();
        }
        //load
        private void ReciveFood_Main_Load(object sender, EventArgs e)
        {
            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;
            barcode.BarHeight = 45;
            barcode.LeftMargin = 30;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;

            ClearData();

            radLabel_Detail.Text = "F3 > void สินค้า | F2 > ยกเลิกบิล | PdDn > Multiply | Home : รวม";

            radLabel_ReciveID.Text = SystemClass.SystemPOSGROUP;
            radLabel_ReciveName.Text = SystemClass.SystemZONEID;

            DatagridClass.SetDefaultRadGridView(radGridView_Show);

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("LINENUM", "ลำดับ"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 350));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("STA", "รับ"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("QTY", "จำนวน", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMID", "รหัสสินค้า"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("INVENTDIMID", "มิติสินค้า"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("INVENT", "คลังสินค้า"));


            radGridView_Show.MasterTemplate.EnableFiltering = false;
            radGridView_Show.TableElement.RowHeight = 60;

            dtGrid.Columns.Add("LINENUM");
            dtGrid.Columns.Add("ITEMBARCODE");
            dtGrid.Columns.Add("SPC_ITEMNAME");
            dtGrid.Columns.Add("STA");
            dtGrid.Columns.Add("QTY");
            dtGrid.Columns.Add("UNITID");
            dtGrid.Columns.Add("ITEMID");
            dtGrid.Columns.Add("INVENTDIMID");
            dtGrid.Columns.Add("INVENT");

            radTextBox_Barcode.Focus();
        }
        //Format Cell
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            e.CellElement.Font = SystemClass.SetFont12;
            try
            {
                if (e.CellElement is GridRowHeaderCellElement && e.Row is GridViewDataRowInfo)
                {
                    e.CellElement.Text = (e.CellElement.RowIndex + 1).ToString();
                    e.CellElement.TextImageRelation = TextImageRelation.ImageBeforeText;
                }
                else
                {
                    e.CellElement.ResetValue(LightVisualElement.TextImageRelationProperty, ValueResetFlags.Local);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        //F2 remove
        private void RadGridView_Show_KeyDown(object sender, KeyEventArgs e)
        {
            if (dtGrid.Rows.Count == 0) return;

            switch (e.KeyCode)
            {
                case Keys.F3: F3(radGridView_Show.CurrentRow.Index); break;
                case Keys.Escape: radTextBox_Barcode.Focus(); break;
                default:
                    break;
            }
        }
        //Enter Barcode
        private void RadTextBox_Barcode_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (radTextBox_Barcode.Text == "")
                    {
                        MsgBoxClass.MassageBoxShowButtonOk_Warning("ระบุบาร์โค้ดสินค้าก่อน Enter.", SystemClass.HeaderMNPI);
                        radTextBox_Barcode.Focus();
                        return;
                    }
                    EnterTextbox(radTextBox_Barcode.Text);
                    break;

                case Keys.PageDown:
                    if (radTextBox_Barcode.Text == "")
                    {
                        return;
                    }
                    int qty;
                    try
                    {
                        qty = Convert.ToInt32((radTextBox_Barcode.Text));
                    }
                    catch (Exception)
                    {
                        qty = 0;
                    }
                    //check qty
                    if (qty <= 0)
                    {
                        MsgBoxClass.MassageBoxShowButtonOk_Warning("จำนวนที่ระบุต้องมากกว่า 0 เท่านั้น.", SystemClass.HeaderMNPI);
                        radTextBox_Barcode.SelectAll();
                        radTextBox_Barcode.Focus();
                        return;
                    }
                    if (qty > 10000)
                    {
                        MsgBoxClass.MassageBoxShowButtonOk_Warning("จำนวนสินค้าที่ระบุมากผิดปกติ เช็คใหม่อีกครั้ง", SystemClass.HeaderMNPI);
                        radTextBox_Barcode.SelectAll();
                        radTextBox_Barcode.Focus();
                        return;
                    }
                    radLabel_X.Visible = true;
                    radLabel_Qty.Text = qty.ToString("N2");
                    radLabel_Qty.Visible = true;
                    radTextBox_Barcode.SelectAll();
                    radTextBox_Barcode.Focus();
                    break;

                case Keys.F2:
                    F2();
                    break;

                case Keys.F3:
                    if (dtGrid.Rows.Count == 0) return;
                    F3(dtGrid.Rows.Count - 1);
                    break;

                case Keys.Home:
                    Home();
                    break;

                case Keys.Escape:
                    if (radGridView_Show.Rows.Count == 0) Application.Exit();
                    break;
                default:
                    return;
            }
        }
        //Enter Barcode
        void EnterTextbox(string barcode)
        {
            //ค้นหารายละเอียดสินค้า
            DataTable dtBarcode = ReciveFood.GetBarcodeDetail_ByBarcode(barcode, SystemClass.SystemPOSGROUP);
            if (dtBarcode.Rows.Count == 0)
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning("ไม่พบข้อมูลสินค้าของบาร์โค้ดที่ระบุ" + Environment.NewLine + "ลองใหม่อีกครั้ง.", SystemClass.HeaderMNPI);
                radTextBox_Barcode.SelectAll(); radTextBox_Barcode.Focus();
                return;
            }
            //Save
            if (dtGrid.Rows.Count == 0)
            {
                if (radLabel_Docno.Text == "")
                {
                    SaveHD(dtBarcode);
                }
                else
                {
                    SaveDT(dtBarcode);
                }
            }
            else
            {
                SaveDT(dtBarcode);
            }

        }
        // Sava HD
        void SaveHD(DataTable dtBarcode)
        {
            string billMaxNO = Class.ConfigClass.GetMaxINVOICEID("MNPI", "-", "MNPI", "1");
            string result = ReciveFood.SaveData(billMaxNO, Convert.ToDouble(radLabel_Qty.Text), 1, dtBarcode);
            if (result == "")
            {
                dtGrid.Rows.Add("1", dtBarcode.Rows[0]["ITEMBARCODE"].ToString(),
                      dtBarcode.Rows[0]["SPC_ITEMNAME"].ToString(),
                      "1",
                      Convert.ToDouble(radLabel_Qty.Text).ToString("N2"),
                      dtBarcode.Rows[0]["UNITID"].ToString(),
                      dtBarcode.Rows[0]["ITEMID"].ToString(),
                      dtBarcode.Rows[0]["INVENTDIMID"].ToString(),
                      "RETAILAREA");

                radGridView_Show.DataSource = dtGrid;
                dtGrid.AcceptChanges();

                iCountItem = 1;
                iRowDT = 1;
                radLabel_X.Visible = false;
                radLabel_Qty.Text = "1";
                radLabel_Qty.Visible = false;
                radLabel_Docno.Text = billMaxNO;
                radTextBox_Barcode.Text = "";
                radTextBox_Barcode.Focus();
                radGridView_Show.Rows[dtGrid.Rows.Count - 1].IsCurrent = true;
            }
            else
            {
                MsgBoxClass.MassageBoxShowButtonOk_Error("ไม่สามารถบันทึกข้อมูลได้ ลองใหม่อีกครั้ง" + Environment.NewLine + result, SystemClass.HeaderMNPI);
                radTextBox_Barcode.SelectAll();
                radTextBox_Barcode.Focus();
                return;
            }
        }
        //Save DT
        void SaveDT(DataTable dtBarcode)
        {
            iRowDT++;

            string result = ReciveFood.SaveData(radLabel_Docno.Text, Convert.ToDouble(radLabel_Qty.Text), iRowDT, dtBarcode);// ConnectionClass.ExecuteSQL_ArrayMain(sql);
            if (result == "")
            {
                iCountItem++;
                dtGrid.Rows.Add(iRowDT, dtBarcode.Rows[0]["ITEMBARCODE"].ToString(),
                       dtBarcode.Rows[0]["SPC_ITEMNAME"].ToString(),
                       "1",
                       Convert.ToDouble(radLabel_Qty.Text).ToString("N2"),
                       dtBarcode.Rows[0]["UNITID"].ToString(),
                       dtBarcode.Rows[0]["ITEMID"].ToString(),
                       dtBarcode.Rows[0]["INVENTDIMID"].ToString(),
                       "RETAILAREA");

                radGridView_Show.DataSource = dtGrid;
                dtGrid.AcceptChanges();
                radLabel_X.Visible = false;
                radLabel_Qty.Text = "1";
                radLabel_Qty.Visible = false;

                radTextBox_Barcode.Text = "";
                radTextBox_Barcode.Focus();
                radGridView_Show.Rows[dtGrid.Rows.Count - 1].IsCurrent = true;
            }
            else
            {
                iRowDT -= 1;
                MsgBoxClass.MassageBoxShowButtonOk_Error("ไม่สามารถบันทึกข้อมูลได้ ลองใหม่อีกครั้ง" + Environment.NewLine + result, SystemClass.HeaderMNPI);
                radTextBox_Barcode.SelectAll();
                radTextBox_Barcode.Focus();
                return;
            }
        }
        //F2
        void F2()
        {
            if (dtGrid.Rows.Count == 0) ClearData();

            if (MsgBoxClass.MassageBoxShowYesNo_DialogResult("ยืนยันการยกเลิกบิลเลขที่ " + radLabel_Docno.Text + " ?.", SystemClass.HeaderMNPI) == DialogResult.No) return;

            Application.Exit();
        }
        //F3
        void F3(int iRows)
        {
            if (dtGrid.Rows.Count == 0) return;

            if (radGridView_Show.Rows[iRows].Cells["LINENUM"].Value.ToString() == "") { return; }
            if (radGridView_Show.Rows[iRows].Cells["STA"].Value.ToString() == "0") return;

            String desc = dtGrid.Rows[iRows]["ITEMBARCODE"].ToString() + " " + dtGrid.Rows[iRows]["SPC_ITEMNAME"].ToString() +
                " จำนวน  " + dtGrid.Rows[iRows]["QTY"].ToString() + "  " + dtGrid.Rows[iRows]["UNITID"].ToString();

            if (MessageBox.Show("ยืนยันการยกเลิกรายการ " + Environment.NewLine + desc + " ?.", SystemClass.HeaderMNPI,
                       MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;
         
            String result = ReciveFood.UpdateStatus_Void(radLabel_Docno.Text, dtGrid.Rows[iRows]["LINENUM"].ToString());// ConnectionClass.ExecuteSQL_ArrayMain(sqlUp);
            if (result == "")
            {
                iCountItem--;
                radGridView_Show.Rows[iRows].Cells["STA"].Value = "0";
                radGridView_Show.Rows[iRows].Cells["SPC_ITEMNAME"].Value = "Void-" + radGridView_Show.Rows[iRows].Cells["SPC_ITEMNAME"].Value;
                radGridView_Show.Rows[iRows].Cells["QTY"].Value = "-" + Convert.ToDouble(radGridView_Show.Rows[iRows].Cells["QTY"].Value).ToString("N2");
                dtGrid.AcceptChanges();
                radTextBox_Barcode.SelectAll();
                radTextBox_Barcode.Focus();
                return;
            }
            else
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning("ไม่สามารถยกเลิกรายการสินค้า" + Environment.NewLine + desc + " ได้ " + Environment.NewLine +
                    result, SystemClass.HeaderMNPI);
                radTextBox_Barcode.SelectAll();
                radTextBox_Barcode.Focus();
                return;
            }
        }
        //Home
        void Home()
        {
            if (dtGrid.Rows.Count == 0) return;

            GeneralForm.FormHome _home = new GeneralForm.FormHome("F4", "0", radLabel_Docno.Text, radLabel_Grand.Text, iCountItem.ToString());
            if (_home.ShowDialog(this) == DialogResult.Yes)
            {
                DataTable dtSendAX = ReciveFood.FindBillDetail_Docno(radLabel_Docno.Text);
                if (dtSendAX.Rows.Count == 0)
                {
                    MsgBoxClass.MassageBoxShowButtonOk_Error("ไม่พบเลขที่บิล " + radLabel_Docno.Text + " ลองใหม่อีกครั้ง.", SystemClass.HeaderMNPI);
                    return;
                }
                //Update Data + SendAX    
                string result = ConnectionClass.ExecuteMain_AX_24_SameTime(ReciveFood.SqlUpdateBill_Apv(radLabel_Docno.Text), ReciveFood.SqlSendAX(dtSendAX));
                if (result == "")
                {
                    PrintDocBill();
                    Application.Exit();
                }
                else
                {
                    MsgBoxClass.MassageBoxShowButtonOk_Error("ไม่สามารถส่งข้อมูลได้ ลองใหม่อีกครั้ง" + Environment.NewLine + result, SystemClass.HeaderMNPI);
                    radTextBox_Barcode.SelectAll();
                    radTextBox_Barcode.Focus();
                    return;
                }
            }
            else
            {
                radTextBox_Barcode.SelectAll();
                radTextBox_Barcode.Focus();
                return;
            }
        }

        //พิมพ์เอกสาร
        private void PrintDocument_printBill_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            barcode.Data = radLabel_Docno.Text;
            Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());

            int Y = 0;
            e.Graphics.DrawString("รับอาหารกล่อง", SystemClass.SetFont12, Brushes.Black, 10, Y);
            Y += 25;
            e.Graphics.DrawString(SystemClass.SystemPOSGROUP + " " + SystemClass.SystemZONEID, SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 20;
            e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 20;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawImage(barcodeInBitmap, 2, Y);
            Y += 67;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            for (int ii = 0; ii < radGridView_Show.Rows.Count; ii++)
            {
                if (radGridView_Show.Rows[ii].Cells["STA"].Value.ToString() == "1")
                {
                    Y += 20;
                    e.Graphics.DrawString((ii + 1).ToString() + ".(" + (Convert.ToDouble(radGridView_Show.Rows[ii].Cells["QTY"].Value).ToString("#,#0.00")).ToString() + " X " +
                                      radGridView_Show.Rows[ii].Cells["UNITID"].Value.ToString() + ")   " +
                                      radGridView_Show.Rows[ii].Cells["ITEMBARCODE"].Value.ToString(),
                         SystemClass.printFont, Brushes.Black, 1, Y);
                    Y += 15;
                    e.Graphics.DrawString(radGridView_Show.Rows[ii].Cells["SPC_ITEMNAME"].Value.ToString(),
                        SystemClass.printFont, Brushes.Black, 0, Y);
                }
            }
            Y += 15;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString("จำนวนทั้งหมด " + iCountItem.ToString("N2") + @"  รายการ", SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 20;
            e.Graphics.DrawString("แคชเชียร์ : " + SystemClass.SystemUserID, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString(SystemClass.SystemUserName, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            e.Graphics.PageUnit = GraphicsUnit.Inch;

        }
        //print
        void PrintDocBill()
        {
            System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
            PrinterSettings settings = new PrinterSettings();
            settings.DefaultPageSettings.PaperSize = ps;
            string defaultPrinterName = settings.PrinterName;

            PrintDocument_printBill.PrintController = printController;
            PrintDocument_printBill.PrinterSettings.PrinterName = defaultPrinterName;

            PrintDocument_printBill.Print();
        }
    }
}
