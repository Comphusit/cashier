﻿using System;
using System.Windows.Forms;
using Cashier.Class;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using System.Data;
using System.Drawing;

namespace Cashier.GeneralForm
{
    public partial class FormHome : Telerik.WinControls.UI.RadForm
    {
        public string pXXX;
        readonly string _pCaseOpen;
        readonly string _pDocno;
        readonly string _pGrand;
        readonly string _pRows;
        readonly string _pFormPrint;//0 ทำบิลปกติ 1 พิมพ์บิลซ้ำ
        public FormHome(string pCaseOpen, string pFormPrint, string pDocno, string pGrand, string pRows)
        {
            InitializeComponent();
            _pCaseOpen = pCaseOpen;
            _pDocno = pDocno;
            _pGrand = pGrand;
            _pRows = pRows;
            _pFormPrint = pFormPrint;
        }
        //Clear
        void ClearData()
        {
            //pXXX = "";
            radLabel_Docno.Text = "";
            radLabel_Desc.Text = "";
            radLabel_Grand.Text = "";

            radTextBox_Recive.Text = "";
            radTextBox_Recive.Focus();
        }
        //load
        private void FormHome_Load(object sender, EventArgs e)
        {

            DatagridClass.SetDefaultRadGridView(radGridView_Show);

            ClearData();

            switch (_pCaseOpen)
            {
                case "F1":
                    this.BackColor = ConfigClass.SetBackColor_F1();
                    radLabel_Detail.BackColor = ConfigClass.SetBackColor_F1();

                    radLabel_Detail.Text = "กด ESC >> ออกจากหน้าจอ | กด Enter >> พิมพ์บิล | กด F12 >> พิมพ์บิล";

                    radLabel_Docno.Text = _pDocno;

                    if (_pFormPrint == "0")
                    {
                        radLabel_Desc.Text = "ยอดเงิน [โดยประมาณ]"; radLabel_Desc.ForeColor = ConfigClass.SetColor_Black();
                        radLabel_Grand.Text = _pGrand;
                        radTextBox_Recive.Text = _pGrand;
                    }
                    else
                    {
                        radTextBox_Recive.Text = "";
                        radLabel_Desc.Text = "";
                        radLabel_Grand.Text = "";
                    }

                    radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("QTY_Desc", "คำอธิบาย", 300));
                    radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("QTY", "รายการ", 150));
                    radGridView_Show.MasterTemplate.ShowColumnHeaders = false;
                    radGridView_Show.MasterTemplate.EnableFiltering = false;
                    radGridView_Show.ClearSelection();
                    radGridView_Show.CurrentRow = null;
                    radGridView_Show.Rows.Add("จำนวนรายการทั้งหมด", _pRows);

                    radTextBox_Recive.SelectAll();
                    radTextBox_Recive.Focus();
                    break;

                case "F2":
                    this.BackColor = ConfigClass.SetBackColor_F2();
                    radLabel_Detail.BackColor = ConfigClass.SetBackColor_F2();

                    radLabel_Detail.Text = "กด ESC >> ออกจากหน้าจอ | กด Enter >> พิมพ์บิล | กด F12 >> พิมพ์บิล";

                    radLabel_Docno.Text = _pDocno;
                    radLabel_Docno.Visible = true;
                    radTextBox_Recive.Text = "";
                    radLabel_Desc.Text = "";
                    radLabel_Grand.Text = "";

                    radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("QTY_Desc", "คำอธิบาย", 300));
                    radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("QTY", "รายการ", 150));
                    radGridView_Show.MasterTemplate.ShowColumnHeaders = false;
                    radGridView_Show.MasterTemplate.EnableFiltering = false;
                    radGridView_Show.ClearSelection();
                    radGridView_Show.CurrentRow = null;
                    radGridView_Show.Rows.Add("จำนวนรายการทั้งหมด", _pRows);

                    radTextBox_Recive.SelectAll();
                    radTextBox_Recive.Focus();
                    break;

                case "F3":
                    this.BackColor = ConfigClass.SetBackColor_F3();
                    radLabel_Detail.BackColor = ConfigClass.SetBackColor_F3();

                    radLabel_Detail.Text = "ระบุรอบส่งเงิน > Enter | กด F12 >> พิมพ์บิล | กด ESC >> ออกจากหน้าจอ";

                    radLabel_Docno.Text = _pDocno;

                    radLabel_Desc.Text = "ยอดเงินรวม"; radLabel_Desc.ForeColor = ConfigClass.SetColor_Black();
                    radLabel_Grand.Text = _pGrand;
                    radLabel_Grand.ForeColor = Color.Red;
                    radTextBox_Recive.Text = "";

                    radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ID", "เลขที่ส่งเงิน", 300));
                    radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("Grand", "ยอดเงิน", 150));
                    radGridView_Show.MasterTemplate.ShowColumnHeaders = true;
                    radGridView_Show.MasterTemplate.EnableFiltering = false;
                    radGridView_Show.ClearSelection();
                    radGridView_Show.CurrentRow = null;

                    if (_pFormPrint == "1")
                    {
                        radLabel_Grand.ForeColor = Color.Blue;
                        radGridView_Show.Rows.Add(pXXX, Convert.ToDouble(radLabel_Grand.Text).ToString("N2"));
                        radLabel_Detail.Text = "กด F12 >> พิมพ์บิล | กด ESC >> ออกจากหน้าจอ";
                    }

                    radTextBox_Recive.SelectAll();
                    radTextBox_Recive.Focus();
                    break;

                case "F4":
                    this.BackColor = ConfigClass.SetBackColor_F4();
                    radLabel_Detail.BackColor = ConfigClass.SetBackColor_F4();

                    radLabel_Detail.Text = "กด ESC >> ออกจากหน้าจอ | กด Enter >> พิมพ์บิล | กด F12 >> พิมพ์บิล";

                    radLabel_Docno.Text = _pDocno;

                    radTextBox_Recive.Text = "";
                    radLabel_Desc.Text = "";
                    radLabel_Grand.Text = "";

                    radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("QTY_Desc", "คำอธิบาย", 320));
                    radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("QTY", "รายการ", 170));
                    radGridView_Show.MasterTemplate.ShowColumnHeaders = false;
                    radGridView_Show.MasterTemplate.EnableFiltering = false;
                    radGridView_Show.ClearSelection();
                    radGridView_Show.CurrentRow = null;
                    radGridView_Show.Rows.Add("จำนวนรายการทั้งหมด", _pRows);

                    radTextBox_Recive.SelectAll();
                    radTextBox_Recive.Focus();
                    break;

                case "F6":
                    this.BackColor = ConfigClass.SetBackColor_F6();
                    radLabel_Detail.BackColor = ConfigClass.SetBackColor_F6();

                    radLabel_Detail.Text = "กด ESC >> ออกจากหน้าจอ | กด Enter >> พิมพ์บิล | กด F12 >> พิมพ์บิล";

                    radLabel_Docno.Text = _pDocno;

                    radLabel_Desc.Text = "ยอดเงินรวม"; radLabel_Desc.ForeColor = ConfigClass.SetColor_Black();
                    radLabel_Grand.Text = _pGrand;
                    radLabel_Grand.ForeColor = Color.Blue;
                    radTextBox_Recive.Text = "";

                    radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ID", "เลขที่ส่งเงิน", 300));
                    radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("Grand", "ยอดเงิน", 150));
                    radGridView_Show.MasterTemplate.ShowColumnHeaders = true;
                    radGridView_Show.MasterTemplate.EnableFiltering = false;
                    radGridView_Show.ClearSelection();
                    radGridView_Show.CurrentRow = null;

                    radGridView_Show.Rows.Add("จำนวนรายการทั้งหมด", _pRows);
                     
                    radTextBox_Recive.SelectAll();
                    radTextBox_Recive.Focus();
                    break;
                case "F7":
                    this.BackColor = ConfigClass.SetBackColor_F7();
                    radLabel_Detail.BackColor = ConfigClass.SetBackColor_F7();

                    radLabel_Detail.Text = "กด ESC >> ออกจากหน้าจอ | กด Enter >> พิมพ์บิล | กด F12 >> พิมพ์บิล";

                    radLabel_Docno.Text = _pDocno;

                    //radLabel_Desc.Text = "ยอดเงินรวม"; radLabel_Desc.ForeColor = ConfigClass.SetColor_Black();
                   // radLabel_Grand.Text = _pGrand;
                    //radLabel_Grand.ForeColor = Color.Blue;
                    radTextBox_Recive.Text = "";

                    radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("QTY_Desc", "คำอธิบาย", 300));
                    radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("QTY", "รายการ", 150));
                    radGridView_Show.MasterTemplate.ShowColumnHeaders = true;
                    radGridView_Show.MasterTemplate.EnableFiltering = false;
                    radGridView_Show.ClearSelection();
                    radGridView_Show.CurrentRow = null;

                    radGridView_Show.Rows.Add("จำนวนรายการทั้งหมด", _pRows);

                    radTextBox_Recive.SelectAll();
                    radTextBox_Recive.Focus();
                    break;
                default:
                    break;
            }

        }
        //Format Cell
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            e.CellElement.Font = SystemClass.SetFont12;
            try
            {
                if (e.CellElement is GridRowHeaderCellElement && e.Row is GridViewDataRowInfo)
                {
                    e.CellElement.Text = (e.CellElement.RowIndex + 1).ToString();
                    e.CellElement.TextImageRelation = TextImageRelation.ImageBeforeText;
                }
                else
                {
                    e.CellElement.ResetValue(LightVisualElement.TextImageRelationProperty, ValueResetFlags.Local);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void RadTextBox_Recive_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    this.DialogResult = DialogResult.No;
                    this.Close();
                    break;
                case Keys.F12:
                    if (_pCaseOpen == "F3")
                    {
                        if (radGridView_Show.Rows.Count == 0)
                        {
                            MsgBoxClass.MassageBoxShowButtonOk_Warning("ต้องระบุรอบส่งเงินเพื่อจ่ายเงินให้ลูกค้าทุกครั้ง" + Environment.NewLine +
                                "ไม่สามารถพิมพ์บิลได้ ถ้าไม่ระบุ.", SystemClass.HeaderBuyCust);
                            return;
                        }

                        this.DialogResult = DialogResult.Yes;
                        this.Close();
                    }
                    else
                    {
                        this.DialogResult = DialogResult.Yes;
                        this.Close();
                    }

                    break;
                case Keys.Enter:
                    if (_pCaseOpen == "F3")
                    {
                        if (_pFormPrint == "1")
                        {
                            return;
                        }
                        if (radTextBox_Recive.Text == "")
                        {
                            MsgBoxClass.MassageBoxShowButtonOk_Error("ระบุรอบส่งเงินของแคชเชียร์ให้เรียบร้อยก่อนเท่านั้น", SystemClass.HeaderBuyCust);
                            radTextBox_Recive.SelectAll();
                            radTextBox_Recive.Focus();
                            return;
                        }
                        DataTable dtXXX = F3_BuyItem.BuyItem.GetXXX_POSDRAWERCYCLETABLE(radTextBox_Recive.Text.Trim());
                        if (dtXXX.Rows.Count == 0)
                        {
                            MsgBoxClass.MassageBoxShowButtonOk_Warning("ไม่พบรอบส่งเงินที่ระบุ ลองใหม่อีกครั้ง.", SystemClass.HeaderBuyCust);
                            radTextBox_Recive.SelectAll(); radTextBox_Recive.Focus();
                            return;
                        }

                        if (F3_BuyItem.BuyItem.CheckUseMoneyRound(dtXXX.Rows[0]["DRAWERID"].ToString()) > 0)
                        {
                            MsgBoxClass.MassageBoxShowButtonOk_Warning("รอบส่งเงินที่ระบุ ได้ใช้ซื้อสินค้าไปเรียบร้อยแล้ว" + Environment.NewLine +
                                "ไม่สามารถใช้ซ้ำได้อีก", SystemClass.HeaderBuyCust);
                            radTextBox_Recive.SelectAll(); radTextBox_Recive.Focus();
                            return;
                        }

                        if (dtXXX.Rows[0]["POSGROUP"].ToString() != SystemClass.SystemPOSGROUP)
                        {
                            MsgBoxClass.MassageBoxShowButtonOk_Warning("รอบส่งเงินที่ระบุ ไม่ใช่สาขาที่เปิดใช้งาน" + Environment.NewLine +
                                 "เช็คข้อมูลใหม่อีกครั้ง", SystemClass.HeaderBuyCust);
                            radTextBox_Recive.SelectAll(); radTextBox_Recive.Focus();
                            return;

                        }

                        if (dtXXX.Rows[0]["STA_DATE"].ToString() == "0")
                        {
                            MsgBoxClass.MassageBoxShowButtonOk_Warning("รอบส่งเงินที่ระบุ ไม่ใช่วันที่ปัจจุบัน" + Environment.NewLine +
                                "ไม่สามารถใช้งานได้", SystemClass.HeaderBuyCust);
                            radTextBox_Recive.SelectAll(); radTextBox_Recive.Focus();
                            return;
                        }
                        if (dtXXX.Rows[0]["EMPLID"].ToString() != SystemClass.SystemUserID)
                        {
                            MsgBoxClass.MassageBoxShowButtonOk_Warning("รอบส่งเงินที่ระบุ ไม่ใช่แคชเชียร์ที่เปิดใช้งาน" + Environment.NewLine +
                               "ไม่สามารถใช้งานได้", SystemClass.HeaderBuyCust);
                            radTextBox_Recive.SelectAll(); radTextBox_Recive.Focus();
                            return;
                        }
                        if (Convert.ToDouble(dtXXX.Rows[0]["CASHCOUNT"].ToString()) != Convert.ToDouble(_pGrand))
                        {
                            MsgBoxClass.MassageBoxShowButtonOk_Warning("รอบส่งเงินที่ระบุ มียอดเงินที่ไม่เท่ากับราคาซื้อสินค้า" + Environment.NewLine +
                               "เช็คข้อมูลใหม่อีกครั้ง", SystemClass.HeaderBuyCust);
                            radTextBox_Recive.SelectAll(); radTextBox_Recive.Focus();
                            return;
                        }
                        radLabel_Grand.ForeColor = Color.Blue;
                        pXXX = dtXXX.Rows[0]["DRAWERID"].ToString();
                        radGridView_Show.Rows.Add(dtXXX.Rows[0]["DRAWERID"].ToString(), Convert.ToDouble(dtXXX.Rows[0]["CASHCOUNT"].ToString()).ToString("N2"));
                        radTextBox_Recive.Text = ""; radTextBox_Recive.Focus();
                    }
                    else
                    {
                        this.DialogResult = DialogResult.Yes;
                        this.Close();
                    }
                    break;
                default:
                    break;
            }
        }
    }
}


