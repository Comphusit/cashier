﻿using System;
using System.Windows.Forms;
using Cashier.Class;

namespace Cashier.GeneralForm
{
    public partial class InputData : Telerik.WinControls.UI.RadForm
    {
        public string pInputData;
        readonly string sTANumberOtText; //0 ต้องการเปนตัวเลข 1 ต้องการเป็นข้อความ
        readonly string _pCaseOpen;

        public InputData(string pCaseOpen, string sSTA_0Number_1Text, string sShowData, string sShowInput, string sShowUnit)
        {
            InitializeComponent();
            radLabel_Show.Text = sShowData;
            radLabel_Input.Text = sShowInput;
            radLabel_Unit.Text = sShowUnit;
            sTANumberOtText = sSTA_0Number_1Text;
            _pCaseOpen = pCaseOpen;
        }
        //load
        private void InputData_Load(object sender, EventArgs e)
        {
            switch (_pCaseOpen)
            {
                case "F1":
                    this.BackColor = ConfigClass.SetBackColor_F1();
                    break;
           
                default:
                    break;
            }

            radButton_Save.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;

            radTextBox_Input.Text = pInputData;
            if (sTANumberOtText == "1")
            {
                radTextBox_Input.Multiline = true;
                radTextBox_Input.Size = new System.Drawing.Size(359, 63);
            }
            else
            {
                radTextBox_Input.Multiline = false;
                radTextBox_Input.Size = new System.Drawing.Size(286, 34);
            }
            radTextBox_Input.SelectAll();
            radTextBox_Input.Focus();
        }

        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        private void RadButton_Save_Click(object sender, EventArgs e)
        {
            EnterData();
        }

        void EnterData()
        {
            switch (sTANumberOtText)
            {
                case "0":
                    try
                    {
                        double i = Convert.ToDouble(radTextBox_Input.Text);
                        if (i == 0)
                        {
                            if (MsgBoxClass.MassageBoxShowYesNo_DialogResult("ยืนยันการระบุข้อมูลที่เป็น 0 ?.", SystemClass.HeaderSmartShop) == DialogResult.No) return;
                        }
                    }
                    catch (Exception)
                    {
                        MsgBoxClass.MassageBoxShowButtonOk_Warning("ต้องระบุข้อมูลเป็นตัวเลขเท่านั้น ลองใหม่อีกครั้ง.", SystemClass.HeaderSmartShop);
                        return;
                    }
                    break;

                case "1":
                    if (radTextBox_Input.Text == "")
                    {
                        MsgBoxClass.MassageBoxShowButtonOk_Warning("ต้องระบุข้อมูลก่อนการตกลงให้เรียบร้อย ลองใหม่อีกครั้ง.", SystemClass.HeaderSmartShop);
                        return;
                    }
                    break;
                default:
                    return;
            }

            pInputData = radTextBox_Input.Text;
            this.DialogResult = DialogResult.Yes;
            this.Close();
        }
        //Enter
        private void RadTextBox_Input_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    EnterData();
                    break;
                default:
                    break;
            }
        }

        private void RadTextBox_Input_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (sTANumberOtText == "0")
            {
                if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
                {
                    e.Handled = true;
                }
            }


        }
    }
}
