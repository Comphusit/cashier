﻿using System;
using System.Drawing;
using System.Drawing.Printing;
using System.Windows.Forms;
using Cashier.Class;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using System.Data;
namespace Cashier.GeneralForm
{
    public partial class FormPrint : Telerik.WinControls.UI.RadForm
    {
        private readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();
        readonly PrintController printController = new StandardPrintController();

        string pCaseOpen;
        string pCopyDesc;

        string whoid = "", whoname = "";
        string mnbc_custTel = "", mnbc_xxx = "", mnpd_recivePlace = "", mnog_recivedate = "", mnog_rmk = "";

        string printHead = "", printDesc = "";// F3 Only
        public FormPrint()
        {
            InitializeComponent();
        }
        //Format Cell
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            e.CellElement.Font = SystemClass.SetFont12;
            try
            {
                if (e.CellElement is GridRowHeaderCellElement && e.Row is GridViewDataRowInfo)
                {
                    e.CellElement.Text = (e.CellElement.RowIndex + 1).ToString();
                    e.CellElement.TextImageRelation = TextImageRelation.ImageBeforeText;
                }
                else
                {
                    e.CellElement.ResetValue(LightVisualElement.TextImageRelationProperty, ValueResetFlags.Local);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        //Clear
        void ClearData()
        {
            pCaseOpen = "";
            whoid = ""; whoname = "";
            mnbc_custTel = ""; mnbc_xxx = ""; mnpd_recivePlace = ""; mnog_recivedate = ""; mnog_rmk = "";
            radLabel_Head.Text = "";
            radLabel_CustID.Text = "";
            radLabel_CustName.Text = "";
            radLabel_Docno.Text = "";
            radLabel_ReciveID.Text = ""; radLabel_ReciveName.Text = "";

            radTextBox_Bill.Text = "";

            radLabel_txt.Visible = false; radLabel_Grand.Visible = false;

            radLabel_Grand.Text = "0.00";
            radTextBox_Bill.Text = "";
            radTextBox_Bill.Focus();
        }
        //load
        private void FormPrint_Load(object sender, EventArgs e)
        {
            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;
            barcode.BarHeight = 45;
            barcode.LeftMargin = 30;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;

            radLabel_Detail.Text = "ระบุเลขที่บิล Enter > ค้นหาข้อมูล | Home : พิมพ์ | ESC > ออกจากโปรแกรม";

            DatagridClass.SetDefaultRadGridView(radGridView_Show);


            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 140));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 400));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("QTY", "ปริมาณ", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SALEPRICE", "ราคา/หน่วย", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("LINEAMOUNT", "ยอดรวม", 120));

            radGridView_Show.MasterTemplate.EnableFiltering = false;
            radGridView_Show.TableElement.RowHeight = 60;

            ClearData();
        }
        //Check Input
        private void RadTextBox_Bill_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (radTextBox_Bill.Text == "") return;

                    if (radTextBox_Bill.Text.Length != 16)
                    {
                        MsgBoxClass.MassageBoxShowButtonOk_Warning("เลขที่บิลที่ระบุไม่ถูกต้อง" + Environment.NewLine +
                            "ต้องระบุเลขที่บิลให้ครบ 16 หลักเท่านั้น" + Environment.NewLine + "ลองใหม่อีกครั้ง.", SystemClass.HeaderMassage);
                        return;
                    }
                    //CheckGroup
                    switch (radTextBox_Bill.Text.ToUpper().Substring(0, 4))
                    {
                        case "MNHS":
                            CheckMNHS(radTextBox_Bill.Text.ToUpper());
                            break;

                        case "MNOG":
                            CheckMNOG(radTextBox_Bill.Text.ToUpper());
                            break;
                        case "MNPD":
                            CheckMNPD(radTextBox_Bill.Text.ToUpper());
                            break;

                        case "MNBC":
                            CheckMNBC(radTextBox_Bill.Text.ToUpper());
                            break;

                        case "MNPI":
                            CheckMNPI(radTextBox_Bill.Text.ToUpper());
                            break;

                        case "MNRR":
                            CheckMNRR(radTextBox_Bill.Text.ToUpper());
                            break;

                        default:
                            MsgBoxClass.MassageBoxShowButtonOk_Error("เลขที่บิลที่ระบุไม่ถูกต้อง" + Environment.NewLine +
                                "เลขที่บิลจะต้องขึ้นต้นด้วยหมวด MNHS/MNPD/MNOG/MNPI/MNBC/MNRR เท่านั้น" + Environment.NewLine +
                                "ลองใหม่อีกครั้ง.",
                                SystemClass.HeaderMassage);
                            radTextBox_Bill.SelectAll();
                            radTextBox_Bill.Focus();
                            break;
                    }
                    break;
                case Keys.Home:
                    Home();
                    break;
                case Keys.Escape:
                    Application.Exit();
                    break;
                default:
                    break;
            }
        }
        //Check MNHS
        void CheckMNHS(string pDocno)
        {

            DataTable dtMNHS = F1_SmartShop.SmartShop.FindBillForPrint_Docno(pDocno);
            if (dtMNHS.Rows.Count == 0)
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning("ไม่พบข้อมูลที่ระบุในหมวดบิล ลูกค้าเครดิต" + Environment.NewLine +
                    "เช็คข้อมูลแล้วลองใหม่อีกครั้ง", SystemClass.HeaderMassage);
                return;
            }

            if (dtMNHS.Rows[0]["POSGROUP"].ToString() != SystemClass.SystemPOSGROUP)
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning("เลขที่บิลที่ระบุไม่ใช่บิลของสาขา" + Environment.NewLine +
                    "ไม่อนุญาติให้พิมพ์ต่างสาขา", SystemClass.HeaderMassage);
                dtMNHS.Rows.Clear();
                radGridView_Show.DataSource = dtMNHS;
                dtMNHS.AcceptChanges();
                ClearData();
                return;
            }

            if (dtMNHS.Rows[0]["STA_PRINT"].ToString() == "0")
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning("เลขที่บิลที่ระบุไม่ใช่วันที่ปัจจุบัน" + Environment.NewLine +
                   "ไม่อนุญาติให้พิมพ์บิลต่างวันที่", SystemClass.HeaderMassage);
                dtMNHS.Rows.Clear();
                radGridView_Show.DataSource = dtMNHS;
                dtMNHS.AcceptChanges();
                ClearData();
                return;
            }

            radLabel_Head.Text = "ลูกค้าเครดิต";
            radLabel_Docno.Text = dtMNHS.Rows[0]["INVOICEID"].ToString();
            radLabel_CustID.Text = dtMNHS.Rows[0]["INVOICEACCOUNT"].ToString();
            radLabel_CustName.Text = dtMNHS.Rows[0]["NAME"].ToString();
            radLabel_ReciveID.Text = dtMNHS.Rows[0]["PERSONNELID"].ToString();
            radLabel_ReciveName.Text = dtMNHS.Rows[0]["PERSONNELNAME"].ToString();
            whoid = dtMNHS.Rows[0]["SALESID"].ToString();
            whoname = dtMNHS.Rows[0]["SPC_NAME"].ToString();

            radGridView_Show.DataSource = dtMNHS;
            dtMNHS.AcceptChanges();

            radTextBox_Bill.SelectAll();
            radTextBox_Bill.Focus();
            pCaseOpen = "F1";

        }
        //Check MNPI
        void CheckMNPI(string pDocno)
        {
            DataTable dtMNPI = F4_ReciveFood.ReciveFood.FindBillForPrint_Docno(pDocno);
            if (dtMNPI.Rows.Count == 0)
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning("ไม่พบข้อมูลที่ระบุในหมวดบิล รับอาหารกล่อง" + Environment.NewLine +
                    "เช็คข้อมูลแล้วลองใหม่อีกครั้ง", SystemClass.HeaderMassage);
                radGridView_Show.DataSource = dtMNPI;
                dtMNPI.AcceptChanges();
                ClearData();
                return;
            }

            if (dtMNPI.Rows[0]["BRANCH_ID"].ToString() != SystemClass.SystemPOSGROUP)
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning("เลขที่บิลที่ระบุไม่ใช่บิลของสาขา" + Environment.NewLine +
                    "ไม่อนุญาติให้พิมพ์ต่างสาขา", SystemClass.HeaderMassage);
                dtMNPI.Rows.Clear();
                radGridView_Show.DataSource = dtMNPI;
                dtMNPI.AcceptChanges();
                ClearData();
                return;
            }

            if (dtMNPI.Rows[0]["STA_PRINT"].ToString() == "0")
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning("เลขที่บิลที่ระบุไม่ใช่วันที่ปัจจุบัน" + Environment.NewLine +
                   "ไม่อนุญาติให้พิมพ์บิลต่างวันที่", SystemClass.HeaderMassage);
                dtMNPI.Rows.Clear();
                radGridView_Show.DataSource = dtMNPI;
                dtMNPI.AcceptChanges();
                ClearData();
                return;
            }

            radLabel_Head.Text = "อาหารกล่อง";
            radLabel_Docno.Text = dtMNPI.Rows[0]["INVOICEID"].ToString();
            radLabel_CustID.Text = "";
            radLabel_CustName.Text = "";
            radLabel_ReciveID.Text = dtMNPI.Rows[0]["BRANCH_ID"].ToString();
            radLabel_ReciveName.Text = dtMNPI.Rows[0]["BRANCH_NAME"].ToString();
            whoid = dtMNPI.Rows[0]["WHOINS"].ToString();
            whoname = dtMNPI.Rows[0]["SPC_NAME"].ToString();

            radGridView_Show.Columns["SALEPRICE"].IsVisible = false;
            radGridView_Show.Columns["LINEAMOUNT"].IsVisible = false;

            radGridView_Show.DataSource = dtMNPI;
            dtMNPI.AcceptChanges();

            radTextBox_Bill.SelectAll();
            radTextBox_Bill.Focus();
            pCaseOpen = "F4";

        }
        //Check MNBC
        void CheckMNBC(string pDocno)
        {
            DataTable dtMNBC = F3_BuyItem.BuyItem.FindBillForPrint_Docno(pDocno);
            if (dtMNBC.Rows.Count == 0)
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning("ไม่พบข้อมูลที่ระบุในหมวดบิลที่ระบุ" + Environment.NewLine +
                    "เช็คข้อมูลแล้วลองใหม่อีกครั้ง", SystemClass.HeaderMassage);
                radGridView_Show.DataSource = dtMNBC;
                dtMNBC.AcceptChanges();
                ClearData();
                return;
            }

            if (dtMNBC.Rows[0]["BRANCH_ID"].ToString() != SystemClass.SystemPOSGROUP)
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning("เลขที่บิลที่ระบุไม่ใช่บิลของสาขา" + Environment.NewLine +
                    "ไม่อนุญาติให้พิมพ์ต่างสาขา", SystemClass.HeaderMassage);
                dtMNBC.Rows.Clear();
                radGridView_Show.DataSource = dtMNBC;
                dtMNBC.AcceptChanges();
                ClearData();
                return;
            }

            if (dtMNBC.Rows[0]["STA_PRINT"].ToString() == "0")
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning("เลขที่บิลที่ระบุไม่ใช่วันที่ปัจจุบัน" + Environment.NewLine +
                   "ไม่อนุญาติให้พิมพ์บิลต่างวันที่", SystemClass.HeaderMassage);
                dtMNBC.Rows.Clear();
                radGridView_Show.DataSource = dtMNBC;
                dtMNBC.AcceptChanges();
                ClearData();
                return;
            }


            radLabel_Docno.Text = dtMNBC.Rows[0]["INVOICEID"].ToString();
            radLabel_CustID.Text = dtMNBC.Rows[0]["MNBCCstID"].ToString();
            radLabel_CustName.Text = dtMNBC.Rows[0]["MNBCCstName"].ToString();
            radLabel_ReciveID.Text = dtMNBC.Rows[0]["BRANCH_ID"].ToString();
            radLabel_ReciveName.Text = dtMNBC.Rows[0]["BRANCH_NAME"].ToString();
            whoid = dtMNBC.Rows[0]["WHOINS"].ToString();
            whoname = dtMNBC.Rows[0]["SPC_NAME"].ToString();
            mnbc_custTel = dtMNBC.Rows[0]["MNBCCstTel"].ToString();
            mnbc_xxx = dtMNBC.Rows[0]["MNBCMoneyRound"].ToString();
            radGridView_Show.Columns["SALEPRICE"].IsVisible = true;
            radGridView_Show.Columns["LINEAMOUNT"].IsVisible = true;
            printHead = dtMNBC.Rows[0]["PRINT_HEAD"].ToString();
            printDesc = dtMNBC.Rows[0]["PRINT_DESC"].ToString();

            radGridView_Show.DataSource = dtMNBC;
            dtMNBC.AcceptChanges();

            radLabel_Head.Text = printDesc;
            radLabel_txt.Text = "ยอดเงินรวม"; radLabel_txt.Visible = true;
            radLabel_Grand.Text = Convert.ToDouble(dtMNBC.Rows[0]["MNBCPriceNet"].ToString()).ToString("N2");
            radLabel_Grand.Visible = true;

            radTextBox_Bill.SelectAll();
            radTextBox_Bill.Focus();
            pCaseOpen = dtMNBC.Rows[0]["PRINT_F"].ToString();

        }
        //Check MNOG
        void CheckMNOG(string pDocno)
        {
            DataTable dtMNOG = F2_OrderCust.OrderCust.FindBillForPrint_DocnoMNOG(pDocno);
            if (dtMNOG.Rows.Count == 0)
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning("ไม่พบข้อมูลที่ระบุในหมวดบิล ออเดอร์ลูกค้า" + Environment.NewLine +
                    "เช็คข้อมูลแล้วลองใหม่อีกครั้ง", SystemClass.HeaderMassage);
                radGridView_Show.DataSource = dtMNOG;
                dtMNOG.AcceptChanges();
                ClearData();
                return;
            }

            if (dtMNOG.Rows[0]["BRANCH_ID"].ToString() != SystemClass.SystemPOSGROUP)
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning("เลขที่บิลที่ระบุไม่ใช่บิลของสาขา" + Environment.NewLine +
                    "ไม่อนุญาติให้พิมพ์ต่างสาขา", SystemClass.HeaderMassage);
                dtMNOG.Rows.Clear();
                radGridView_Show.DataSource = dtMNOG;
                dtMNOG.AcceptChanges();
                ClearData();
                return;
            }

            if (dtMNOG.Rows[0]["STA_PRINT"].ToString() == "0")
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning("เลขที่บิลที่ระบุไม่ใช่วันที่ปัจจุบัน" + Environment.NewLine +
                   "ไม่อนุญาติให้พิมพ์บิลต่างวันที่", SystemClass.HeaderMassage);
                dtMNOG.Rows.Clear();
                radGridView_Show.DataSource = dtMNOG;
                dtMNOG.AcceptChanges();
                ClearData();
                return;
            }

            radLabel_Head.Text = "ออเดอร์ลูกค้า";
            radLabel_Docno.Text = dtMNOG.Rows[0]["INVOICEID"].ToString();
            radLabel_CustID.Text = dtMNOG.Rows[0]["CstID"].ToString();
            radLabel_CustName.Text = dtMNOG.Rows[0]["CstName"].ToString();
            radLabel_ReciveID.Text = dtMNOG.Rows[0]["BRANCH_ID"].ToString();
            radLabel_ReciveName.Text = dtMNOG.Rows[0]["BRANCH_NAME"].ToString();
            whoid = dtMNOG.Rows[0]["WHOINS"].ToString();
            whoname = dtMNOG.Rows[0]["SPC_NAME"].ToString();
            mnbc_custTel = dtMNOG.Rows[0]["CstTel"].ToString();
            mnog_recivedate = dtMNOG.Rows[0]["RECIVE_DATE"].ToString();
            mnog_rmk = dtMNOG.Rows[0]["Rmk"].ToString();
            radGridView_Show.Columns["SALEPRICE"].IsVisible = false;
            radGridView_Show.Columns["LINEAMOUNT"].IsVisible = false;
            mnpd_recivePlace = "";
            radGridView_Show.DataSource = dtMNOG;
            dtMNOG.AcceptChanges();
            radTextBox_Bill.SelectAll();
            radTextBox_Bill.Focus();
            pCaseOpen = "F2";
        }
        //Check MNPD
        void CheckMNPD(string pDocno)
        {
            DataTable dtMNPD = F2_OrderCust.OrderCust.FindBillForPrint_DocnoMNPD(pDocno);
            if (dtMNPD.Rows.Count == 0)
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning("ไม่พบข้อมูลที่ระบุในหมวดบิล ออเดอร์ลูกค้า" + Environment.NewLine +
                    "เช็คข้อมูลแล้วลองใหม่อีกครั้ง", SystemClass.HeaderMassage);
                radGridView_Show.DataSource = dtMNPD;
                dtMNPD.AcceptChanges();
                ClearData();
                return;
            }

            if (dtMNPD.Rows[0]["BRANCH_ID"].ToString() != SystemClass.SystemPOSGROUP)
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning("เลขที่บิลที่ระบุไม่ใช่บิลของสาขา" + Environment.NewLine +
                    "ไม่อนุญาติให้พิมพ์ต่างสาขา", SystemClass.HeaderMassage);
                dtMNPD.Rows.Clear();
                radGridView_Show.DataSource = dtMNPD;
                dtMNPD.AcceptChanges();
                ClearData();
                return;
            }

            if (dtMNPD.Rows[0]["STA_PRINT"].ToString() == "0")
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning("เลขที่บิลที่ระบุไม่ใช่วันที่ปัจจุบัน" + Environment.NewLine +
                   "ไม่อนุญาติให้พิมพ์บิลต่างวันที่", SystemClass.HeaderMassage);
                dtMNPD.Rows.Clear();
                radGridView_Show.DataSource = dtMNPD;
                dtMNPD.AcceptChanges();
                ClearData();
                return;
            }

            radLabel_Head.Text = "ออเดอร์ลูกค้า";
            radLabel_Docno.Text = dtMNPD.Rows[0]["INVOICEID"].ToString();
            radLabel_CustID.Text = dtMNPD.Rows[0]["CstID"].ToString();
            radLabel_CustName.Text = dtMNPD.Rows[0]["Customer"].ToString();
            radLabel_ReciveID.Text = dtMNPD.Rows[0]["BRANCH_ID"].ToString();
            radLabel_ReciveName.Text = dtMNPD.Rows[0]["BRANCH_NAME"].ToString();
            whoid = dtMNPD.Rows[0]["WHOINS"].ToString();
            whoname = dtMNPD.Rows[0]["SPC_NAME"].ToString();
            mnbc_custTel = dtMNPD.Rows[0]["Tel"].ToString();
            mnog_recivedate = "";
            mnog_rmk = dtMNPD.Rows[0]["Rmk"].ToString();
            radGridView_Show.Columns["SALEPRICE"].IsVisible = false;
            radGridView_Show.Columns["LINEAMOUNT"].IsVisible = false;
            mnpd_recivePlace = dtMNPD.Rows[0]["RecivePlace"].ToString(); ;
            radGridView_Show.DataSource = dtMNPD;
            dtMNPD.AcceptChanges();
            radTextBox_Bill.SelectAll();
            radTextBox_Bill.Focus();
            pCaseOpen = "F2";
        }
        //Check MNRR
        void CheckMNRR(string pDocno)
        {
            DataTable dtMNRR = F7_FreeItem.FreeItem.FindBillForPrint_Docno(pDocno);
            if (dtMNRR.Rows.Count == 0)
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning("ไม่พบข้อมูลที่ระบุในหมวดบิล แจกของแถมลูกค้า" + Environment.NewLine +
                    "เช็คข้อมูลแล้วลองใหม่อีกครั้ง", SystemClass.HeaderMassage);
                radGridView_Show.DataSource = dtMNRR;
                dtMNRR.AcceptChanges();
                ClearData();
                return;
            }

            if (dtMNRR.Rows[0]["BRANCH_ID"].ToString() != SystemClass.SystemPOSGROUP)
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning("เลขที่บิลที่ระบุไม่ใช่บิลของสาขา" + Environment.NewLine +
                    "ไม่อนุญาติให้พิมพ์ต่างสาขา", SystemClass.HeaderMassage);
                dtMNRR.Rows.Clear();
                radGridView_Show.DataSource = dtMNRR;
                dtMNRR.AcceptChanges();
                ClearData();
                return;
            }

            if (dtMNRR.Rows[0]["STA_PRINT"].ToString() == "0")
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning("เลขที่บิลที่ระบุไม่ใช่วันที่ปัจจุบัน" + Environment.NewLine +
                   "ไม่อนุญาติให้พิมพ์บิลต่างวันที่", SystemClass.HeaderMassage);
                dtMNRR.Rows.Clear();
                radGridView_Show.DataSource = dtMNRR;
                dtMNRR.AcceptChanges();
                ClearData();
                return;
            }

            radLabel_Head.Text = "แจกของแถมลูกค้า";
            radLabel_Docno.Text = dtMNRR.Rows[0]["INVOICEID"].ToString();
            radLabel_CustID.Text = "";
            radLabel_CustName.Text = "";
            radLabel_ReciveID.Text = dtMNRR.Rows[0]["BRANCH_ID"].ToString();
            radLabel_ReciveName.Text = dtMNRR.Rows[0]["BRANCH_NAME"].ToString();
            whoid = dtMNRR.Rows[0]["WHOINS"].ToString();
            whoname = dtMNRR.Rows[0]["SPC_NAME"].ToString();

            radGridView_Show.Columns["SALEPRICE"].IsVisible = false;
            radGridView_Show.Columns["LINEAMOUNT"].IsVisible = false;

            radGridView_Show.DataSource = dtMNRR;
            dtMNRR.AcceptChanges();

            radTextBox_Bill.SelectAll();
            radTextBox_Bill.Focus();
            pCaseOpen = "F7";

        }
        //Home
        void Home()
        {
            if (radGridView_Show.Rows.Count == 0) return;

            GeneralForm.FormHome _home = new GeneralForm.FormHome(pCaseOpen, "1", radLabel_Docno.Text, radLabel_Grand.Text, radGridView_Show.Rows.Count.ToString());
            if (pCaseOpen == "F3")
            {
                _home.pXXX = mnbc_xxx;
            }

            if (_home.ShowDialog(this) == DialogResult.Yes)
            {
                PrintDocBill();
            }
            else
            {
                radTextBox_Bill.SelectAll();
                radTextBox_Bill.Focus();
                return;
            }
        }
        //print
        void PrintDocBill()
        {
            System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
            PrinterSettings settings = new PrinterSettings();
            settings.DefaultPageSettings.PaperSize = ps;
            string defaultPrinterName = settings.PrinterName;

            PrintDocument_printBill.PrintController = printController;
            PrintDocument_printBill.PrinterSettings.PrinterName = defaultPrinterName;

            switch (pCaseOpen)
            {
                case "F1":
                    pCopyDesc = ">>>>>> สำหรับลูกค้า";
                    PrintDocument_printBill.Print();
                    pCopyDesc = ">>>>>> สำหรับมินิมาร์ท";
                    PrintDocument_printBill.Print();
                    pCopyDesc = ">>>>>> สำหรับบัญชี";
                    PrintDocument_printBill.Print();
                    Application.Exit();
                    break;

                case "F2":
                    PrintDocument_printBill.Print();
                    Application.Exit();
                    break;


                case "F3":
                    PrintDocument_printBill.Print();
                    PrintDocument_printBill.Print();
                    PrintDocument_printBill.Print();
                    Application.Exit();
                    break;
                case "F6":
                    PrintDocument_printBill.Print();
                    PrintDocument_printBill.Print();
                    PrintDocument_printBill.Print();
                    Application.Exit();
                    break;

                case "F4":
                    PrintDocument_printBill.Print();
                    Application.Exit();
                    break;

                case "F7":
                    PrintDocument_printBill.Print();
                    Application.Exit();
                    break;
                default:
                    break;
            }
        }
        //Print
        private void PrintDocument_printBill_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            switch (pCaseOpen)
            {
                case "F1":
                    barcode.Data = radLabel_Docno.Text;
                    Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());

                    int Y = 0;
                    e.Graphics.DrawString("ลูกค้าเครดิตมินิมาร์ท [พิมพ์ซ้ำ]", SystemClass.SetFont12, Brushes.Black, 10, Y);
                    Y += 25;
                    e.Graphics.DrawString(SystemClass.SystemPOSGROUP + " " + SystemClass.SystemZONEID, SystemClass.printFont, Brushes.Black, 10, Y);
                    Y += 20;
                    e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), SystemClass.printFont, Brushes.Black, 10, Y);
                    Y += 20;
                    e.Graphics.DrawString("รหัสลูกค้า " + radLabel_CustID.Text, SystemClass.printFont, Brushes.Black, 10, Y);
                    Y += 20;
                    e.Graphics.DrawString(radLabel_CustName.Text, SystemClass.printFont, Brushes.Black, 10, Y);
                    Y += 20;
                    e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 20;
                    e.Graphics.DrawImage(barcodeInBitmap, 0, Y);
                    Y += 70;
                    e.Graphics.DrawString("ผู้รับสินค้า " + radLabel_ReciveID.Text, SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 20;
                    e.Graphics.DrawString(radLabel_ReciveName.Text, SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 15;
                    e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

                    for (int ii = 0; ii < radGridView_Show.Rows.Count; ii++)
                    {
                        Y += 20;
                        e.Graphics.DrawString((ii + 1).ToString() + ".(" + (Convert.ToDouble(radGridView_Show.Rows[ii].Cells["QTY"].Value).ToString("#,#0.00")).ToString() + " X " +
                                          radGridView_Show.Rows[ii].Cells["UNITID"].Value.ToString() + ")   " +
                                          radGridView_Show.Rows[ii].Cells["ITEMBARCODE"].Value.ToString(),
                             SystemClass.printFont, Brushes.Black, 1, Y);

                        if ((SystemClass.SystemPOSGROUP == "MN098") || (SystemClass.SystemPOSGROUP == "RT"))
                        {
                            Y += 15;
                            e.Graphics.DrawString(radGridView_Show.Rows[ii].Cells["UNITID"].Value.ToString() + " ละ " +
                              (Convert.ToDouble(radGridView_Show.Rows[ii].Cells["SALEPRICE"].Value).ToString("#,#0.00")).ToString() + " บาท รวม " +
                            (Convert.ToDouble(radGridView_Show.Rows[ii].Cells["LINEAMOUNT"].Value).ToString("#,#0.00")).ToString() + " บาท",
                                SystemClass.printFont, Brushes.Black, 0, Y);
                        }

                        Y += 15;
                        e.Graphics.DrawString(radGridView_Show.Rows[ii].Cells["SPC_ITEMNAME"].Value.ToString(),
                            SystemClass.printFont, Brushes.Black, 0, Y);
                    }

                    Y += 15;
                    e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 15;
                    e.Graphics.DrawString("จำนวนทั้งหมด " + radGridView_Show.Rows.Count.ToString("N2") + @"  รายการ", SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 20;
                    e.Graphics.DrawString("แคชเชียร์ : " + whoid, SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 20;
                    e.Graphics.DrawString(whoname, SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 15;
                    e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 20;
                    e.Graphics.DrawString(pCopyDesc, SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 25;
                    e.Graphics.DrawString("ผู้รับ[ลูกค้า]__________________________________", SystemClass.printFont, Brushes.Black, 0, Y);
                    Y += 25;
                    e.Graphics.DrawString("ผู้ส่ง[สาขา]__________________________________", SystemClass.printFont, Brushes.Black, 0, Y);

                    e.Graphics.PageUnit = GraphicsUnit.Inch;
                    break;

                case "F2":
                    int Y2 = 0;
                    e.Graphics.DrawString("พิมพ์บิลซ้ำ", SystemClass.SetFont12, Brushes.Black, 10, Y2);
                    Y2 += 25;
                    e.Graphics.DrawString("ออเดอร์ลูกค้า " + mnpd_recivePlace, SystemClass.SetFont12, Brushes.Black, 10, Y2);

                    string type = "ของแห้ง - ";
                    if (radLabel_Docno.Text.Contains("OG"))
                    { type = "ของสด - "; }
                    Y2 += 25;
                    e.Graphics.DrawString(type + radLabel_Docno.Text, SystemClass.SetFont12, Brushes.Black, 10, Y2);

                    Y2 += 25;
                    e.Graphics.DrawString(SystemClass.SystemPOSGROUP + " " + SystemClass.SystemZONEID, SystemClass.printFont, Brushes.Black, 10, Y2);
                    Y2 += 20;
                    e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), SystemClass.printFont, Brushes.Black, 10, Y2);
                    Y2 += 20;
                    e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y2);

                    for (int ii = 0; ii < radGridView_Show.Rows.Count; ii++)
                    {
                        Y2 += 20;
                        e.Graphics.DrawString((ii + 1).ToString() + ".(" + (Convert.ToDouble(radGridView_Show.Rows[ii].Cells["QTY"].Value).ToString("#,#0.00")).ToString() + " X " +
                                          radGridView_Show.Rows[ii].Cells["UNITID"].Value.ToString() + ")   " +
                                          radGridView_Show.Rows[ii].Cells["ITEMBARCODE"].Value.ToString(),
                             SystemClass.printFont, Brushes.Black, 1, Y2);
                        Y2 += 15;
                        e.Graphics.DrawString(radGridView_Show.Rows[ii].Cells["SPC_ITEMNAME"].Value.ToString(),
                            SystemClass.printFont, Brushes.Black, 0, Y2);

                    }
                    Y2 += 15;
                    e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y2);
                    Y2 += 15;
                    e.Graphics.DrawString("จำนวนทั้งหมด " + radGridView_Show.Rows.Count.ToString("N2") + @"  รายการ", SystemClass.printFont, Brushes.Black, 0, Y2);
                    Y2 += 20;
                    e.Graphics.DrawString("แคชเชียร์ : " + SystemClass.SystemUserID, SystemClass.printFont, Brushes.Black, 0, Y2);
                    Y2 += 20;
                    e.Graphics.DrawString(SystemClass.SystemUserName, SystemClass.printFont, Brushes.Black, 0, Y2);
                    Y2 += 15;
                    e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y2);
                    Y2 += 15;
                    e.Graphics.DrawString("ลูกค้า : " + radLabel_CustID.Text + " โทร : " + mnbc_custTel, SystemClass.printFont, Brushes.Black, 0, Y2);
                    Y2 += 20;
                    e.Graphics.DrawString(radLabel_CustName.Text, SystemClass.printFont, Brushes.Black, 0, Y2);

                    if (radLabel_Docno.Text.Contains("OG"))
                    {
                        Y2 += 20;
                        e.Graphics.DrawString("วันที่รับต้องการของ : " + mnog_recivedate, SystemClass.printFont, Brushes.Black, 0, Y2);
                    }
                    Y2 += 20;
                    e.Graphics.DrawString("หมายเหตุ : " + mnog_rmk, SystemClass.printFont, Brushes.Black, 0, Y2);

                    e.Graphics.PageUnit = GraphicsUnit.Inch;
                    break;

                case "F6":
                    barcode.Data = radLabel_Docno.Text;
                    Bitmap barcodeInBitmap6 = new Bitmap(barcode.drawBarcode());

                    int Y6 = 0;
                    e.Graphics.DrawString($@"{printDesc} [พิมพ์ซ้ำ]", SystemClass.SetFont12, Brushes.Black, 10, Y6);
                    Y6 += 25;
                    e.Graphics.DrawString(SystemClass.SystemPOSGROUP + " " + SystemClass.SystemZONEID, SystemClass.printFont, Brushes.Black, 10, Y6);
                    Y6 += 20;
                    e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), SystemClass.printFont, Brushes.Black, 10, Y6);
                    Y6 += 20;
                    e.Graphics.DrawString($@"{printHead} : " + radLabel_CustID.Text + " โทร : " + mnbc_custTel, SystemClass.printFont, Brushes.Black, 0, Y6);
                    Y6 += 20;
                    e.Graphics.DrawString(radLabel_CustName.Text, SystemClass.printFont, Brushes.Black, 0, Y6);
                    Y6 += 20;
                    e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y6);
                    Y6 += 20;
                    e.Graphics.DrawImage(barcodeInBitmap6, 0, Y6);
                    Y6 += 67;
                    e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y6);

                    for (int ii = 0; ii < radGridView_Show.Rows.Count; ii++)
                    {
                        Y6 += 20;
                        e.Graphics.DrawString((ii + 1).ToString() +
                                         ".(" + (Convert.ToDouble(radGridView_Show.Rows[ii].Cells["QTY"].Value).ToString("#,#0.00")).ToString() + " X " +
                                          radGridView_Show.Rows[ii].Cells["UNITID"].Value.ToString() + ")   " +
                                          radGridView_Show.Rows[ii].Cells["ITEMBARCODE"].Value.ToString(),
                             SystemClass.printFont, Brushes.Black, 1, Y6);
                        Y6 += 15;
                        e.Graphics.DrawString(radGridView_Show.Rows[ii].Cells["UNITID"].Value.ToString() + " ละ " +
                            Convert.ToDouble(radGridView_Show.Rows[ii].Cells["SALEPRICE"].Value.ToString()).ToString("#,#0.00") + " บาท รวม " +
                            Convert.ToDouble(radGridView_Show.Rows[ii].Cells["LINEAMOUNT"].Value.ToString()).ToString("#,#0.00") + " บาท",
                            SystemClass.printFont, Brushes.Black, 0, Y6);
                        Y6 += 15;
                        e.Graphics.DrawString(radGridView_Show.Rows[ii].Cells["SPC_ITEMNAME"].Value.ToString(),
                            SystemClass.printFont, Brushes.Black, 0, Y6);
                    }
                    Y6 += 15;
                    e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y6);
                    Y6 += 15;
                    e.Graphics.DrawString("จำนวนทั้งหมด " + radGridView_Show.Rows.Count.ToString("N2") + @"  รายการ", SystemClass.printFont, Brushes.Black, 0, Y6);
                    Y6 += 20;
                    e.Graphics.DrawString("รวมยอดเงินทั้งหมด " + radLabel_Grand.Text, SystemClass.printFont, Brushes.Black, 0, Y6);
                    Y6 += 20;
                    e.Graphics.DrawString("แคชเชียร์ : " + whoid, SystemClass.printFont, Brushes.Black, 0, Y6);
                    Y6 += 20;
                    e.Graphics.DrawString(whoname, SystemClass.printFont, Brushes.Black, 0, Y6);
                    e.Graphics.PageUnit = GraphicsUnit.Inch;
                    break;

                case "F3":
                    barcode.Data = radLabel_Docno.Text;
                    Bitmap barcodeInBitmap3 = new Bitmap(barcode.drawBarcode());

                    int Y3 = 0;
                    e.Graphics.DrawString($@"{printDesc} [พิมพ์ซ้ำ]", SystemClass.SetFont12, Brushes.Black, 10, Y3);
                    Y3 += 25;
                    e.Graphics.DrawString(SystemClass.SystemPOSGROUP + " " + SystemClass.SystemZONEID, SystemClass.printFont, Brushes.Black, 10, Y3);
                    Y3 += 20;
                    e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), SystemClass.printFont, Brushes.Black, 10, Y3);
                    Y3 += 20;
                    e.Graphics.DrawString($@"{printHead} : " + radLabel_CustID.Text + " โทร : " + mnbc_custTel, SystemClass.printFont, Brushes.Black, 0, Y3);
                    Y3 += 20;
                    e.Graphics.DrawString(radLabel_CustName.Text, SystemClass.printFont, Brushes.Black, 0, Y3);
                    if (printHead == "ลูกค้า")
                    {
                        Y3 += 22;
                        e.Graphics.DrawString("รอบส่งเงิน " + mnbc_xxx, SystemClass.printFont, Brushes.Black, 10, Y3);
                    }
                    Y3 += 20;
                    e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y3);
                    Y3 += 20;
                    e.Graphics.DrawImage(barcodeInBitmap3, 0, Y3);
                    Y3 += 67;
                    e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y3);

                    for (int ii = 0; ii < radGridView_Show.Rows.Count; ii++)
                    {
                        Y3 += 20;
                        e.Graphics.DrawString((ii + 1).ToString() +
                                         ".(" + (Convert.ToDouble(radGridView_Show.Rows[ii].Cells["QTY"].Value).ToString("#,#0.00")).ToString() + " X " +
                                             radGridView_Show.Rows[ii].Cells["UNITID"].Value.ToString() + ")   " +
                                          radGridView_Show.Rows[ii].Cells["ITEMBARCODE"].Value.ToString(),
                             SystemClass.printFont, Brushes.Black, 1, Y3);
                        Y3 += 15;
                        e.Graphics.DrawString(radGridView_Show.Rows[ii].Cells["UNITID"].Value.ToString() + " ละ " +
                            Convert.ToDouble(radGridView_Show.Rows[ii].Cells["SALEPRICE"].Value.ToString()).ToString("#,#0.00") + " บาท รวม " +
                            Convert.ToDouble(radGridView_Show.Rows[ii].Cells["LINEAMOUNT"].Value.ToString()).ToString("#,#0.00") + " บาท",
                            SystemClass.printFont, Brushes.Black, 0, Y3);
                        Y3 += 15;
                        e.Graphics.DrawString(radGridView_Show.Rows[ii].Cells["SPC_ITEMNAME"].Value.ToString(),
                            SystemClass.printFont, Brushes.Black, 0, Y3);
                    }
                    Y3 += 15;
                    e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y3);
                    Y3 += 15;
                    e.Graphics.DrawString("จำนวนทั้งหมด " + radGridView_Show.Rows.Count.ToString("N2") + @"  รายการ", SystemClass.printFont, Brushes.Black, 0, Y3);
                    Y3 += 20;
                    e.Graphics.DrawString("รวมยอดเงินทั้งหมด " + radLabel_Grand.Text, SystemClass.printFont, Brushes.Black, 0, Y3);
                    Y3 += 20;
                    e.Graphics.DrawString("แคชเชียร์ : " + whoid, SystemClass.printFont, Brushes.Black, 0, Y3);
                    Y3 += 20;
                    e.Graphics.DrawString(whoname, SystemClass.printFont, Brushes.Black, 0, Y3);
                    e.Graphics.PageUnit = GraphicsUnit.Inch;
                    break;



                case "F4":
                    barcode.Data = radLabel_Docno.Text;
                    Bitmap barcodeInBitmap4 = new Bitmap(barcode.drawBarcode());

                    int Y4 = 0;
                    e.Graphics.DrawString("รับอาหารกล่อง [พิมพ์ซ้ำ]", SystemClass.SetFont12, Brushes.Black, 10, Y4);
                    Y4 += 25;
                    e.Graphics.DrawString(SystemClass.SystemPOSGROUP + " " + SystemClass.SystemZONEID, SystemClass.printFont, Brushes.Black, 10, Y4);
                    Y4 += 20;
                    e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), SystemClass.printFont, Brushes.Black, 10, Y4);
                    Y4 += 20;
                    e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y4);
                    Y4 += 20;
                    e.Graphics.DrawImage(barcodeInBitmap4, 2, Y4);
                    Y4 += 67;
                    e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y4);

                    for (int ii = 0; ii < radGridView_Show.Rows.Count; ii++)
                    {

                        Y4 += 20;
                        e.Graphics.DrawString((ii + 1).ToString() + ".(" + (Convert.ToDouble(radGridView_Show.Rows[ii].Cells["QTY"].Value).ToString("#,#0.00")).ToString() + " X " +
                                          radGridView_Show.Rows[ii].Cells["UNITID"].Value.ToString() + ")   " +
                                          radGridView_Show.Rows[ii].Cells["ITEMBARCODE"].Value.ToString(),
                             SystemClass.printFont, Brushes.Black, 1, Y4);
                        Y4 += 15;
                        e.Graphics.DrawString(radGridView_Show.Rows[ii].Cells["SPC_ITEMNAME"].Value.ToString(),
                            SystemClass.printFont, Brushes.Black, 0, Y4);

                    }
                    Y4 += 15;
                    e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y4);
                    Y4 += 15;
                    e.Graphics.DrawString("จำนวนทั้งหมด " + radGridView_Show.Rows.Count.ToString("N2") + @"  รายการ", SystemClass.printFont, Brushes.Black, 10, Y4);
                    Y4 += 20;
                    e.Graphics.DrawString("แคชเชียร์ : " + whoid, SystemClass.printFont, Brushes.Black, 0, Y4);
                    Y4 += 20;
                    e.Graphics.DrawString(whoname, SystemClass.printFont, Brushes.Black, 0, Y4);
                    Y4 += 15;
                    e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y4);

                    e.Graphics.PageUnit = GraphicsUnit.Inch;
                    break;
                case "F7":
                    barcode.Data = radLabel_Docno.Text;
                    Bitmap barcodeInBitmap7 = new Bitmap(barcode.drawBarcode());

                    int Y7 = 0;
                    e.Graphics.DrawString("แจกของแถมลูกค้า [พิมพ์ซ้ำ]", SystemClass.SetFont12, Brushes.Black, 10, Y7);
                    Y7 += 25;
                    e.Graphics.DrawString(SystemClass.SystemPOSGROUP + " " + SystemClass.SystemZONEID, SystemClass.printFont, Brushes.Black, 10, Y7);
                    Y7 += 20;
                    e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), SystemClass.printFont, Brushes.Black, 10, Y7);
                    Y7 += 20;
                    e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y7);
                    Y7 += 20;
                    e.Graphics.DrawImage(barcodeInBitmap7, 2, Y7);
                    Y7 += 67;
                    e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y7);

                    for (int ii = 0; ii < radGridView_Show.Rows.Count; ii++)
                    {
                        Y7 += 20;
                        e.Graphics.DrawString((ii + 1).ToString() + ".(" + (Convert.ToDouble(radGridView_Show.Rows[ii].Cells["QTY"].Value).ToString("#,#0.00")).ToString() + " X " +
                                          radGridView_Show.Rows[ii].Cells["UNITID"].Value.ToString() + ")   " +
                                          radGridView_Show.Rows[ii].Cells["ITEMBARCODE"].Value.ToString(),
                             SystemClass.printFont, Brushes.Black, 1, Y7);
                        Y7 += 15;
                        e.Graphics.DrawString(radGridView_Show.Rows[ii].Cells["SPC_ITEMNAME"].Value.ToString(),
                            SystemClass.printFont, Brushes.Black, 0, Y7);

                    }
                    Y7 += 15;
                    e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y7);
                    Y7 += 15;
                    e.Graphics.DrawString("จำนวนทั้งหมด " + radGridView_Show.Rows.Count.ToString("N2") + @"  รายการ", SystemClass.printFont, Brushes.Black, 0, Y7);
                    Y7 += 20;
                    e.Graphics.DrawString("แคชเชียร์ : " + SystemClass.SystemUserID, SystemClass.printFont, Brushes.Black, 0, Y7);
                    Y7 += 20;
                    e.Graphics.DrawString(SystemClass.SystemUserName, SystemClass.printFont, Brushes.Black, 0, Y7);
                    Y7 += 15;
                    e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y7);

                    e.Graphics.PageUnit = GraphicsUnit.Inch;
                    break;

                default:
                    break;
            }
        }
    }
}
