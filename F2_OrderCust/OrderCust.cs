﻿using System;
using System.Collections;
using System.Data;
using Cashier.Class;

namespace Cashier.F2_OrderCust
{
    class OrderCust
    {
        //OK FindBarcode Detail
        public static DataTable GetBarcodeDetail_Barcode(string Barcode)
        {
            return ConnectionClass.SelectSQL_Main(" ItembarcodeClass_GetItembarcode_ALLDeatil '" + Barcode + @"' ");
        }
        //สำหรับการ ConFig แผนกจัดซื้ออาหารสด
        public static void GetSystemPurchaseFresh()
        {
            DataTable dt = ConnectionClass.SelectSQL_Main("CashDesktop_OrderCust '0','' ");
            SystemClass.SystemPurchaseFresh = dt.Rows[0]["SERVERIP"].ToString();
        }
        //ค้นหาลูกค้า
        public static DataTable GetDetailCust(string custID)
        {
            return ConnectionClass.SelectSQL_Main(" CashDesktop_OrderCust '1','" + custID + @"' ");
        }
        //string Update MNPD
        public static ArrayList SqlUpdateMNPD_Apv(string docno)
        {
            //ArrayList sqlUp24 = new ArrayList
            //        {
            //           string.Format(@" 
            //                UPDATE  SHOP2013.dbo.Shop_OrderPDHD  
            //                SET     StaPrcDoc = '1' ,
            //                        DateUp = CONVERT(VARCHAR,GETDATE(),23),TimeUp = CONVERT(VARCHAR,GETDATE(),24),
            //                        WhoUp = '" + SystemClass.SystemUserID + @"',WhoUpName = '" + SystemClass.SystemUserName + @"' 
            //                WHERE   DocNo = '" + docno + @"'  ")
            //        };
            //return sqlUp24;

            ArrayList sqlUp24 = new ArrayList
                    {
                       string.Format($@"
                            UPDATE  SHOP_MNPD_HD  
                            SET     STAPRCDOC = '1' ,
                                    DATEUPD = CONVERT(VARCHAR,GETDATE(),25),
                                    WHOUPD = '{SystemClass.SystemUserID}',WHOUPDNAME = '{SystemClass.SystemUserName}' 
                            WHERE   DOCNO = '{docno}'
                        ")
                    };
            return sqlUp24;
        }
        //ค้นหา PD ที่พร้อมส่งเข้า AX Copy มาจาก 24
        public static DataTable GetDataDetailMNPD_ForSendAX(string pDocno)
        {
            return ConnectionClass.SelectSQL_Main(" CashDesktop_OrderCust '2','" + pDocno + @"' ");
        }
        //Send Data AX SPC_POSTOTABLE10 -- เปิดบิลจาก Minimart Copy มาจาก 24
        public static string SendAX_SPC_POSTOTABLE10(ArrayList sqlUp24, DataTable dtDataForSendAX, string FREIGHTSLIPTYPE_0MNTF_98MNPO)
        {
            //ArrayList sqlAX = new ArrayList();
            ////ในกรณีที่ต้องหาคลังก่อน 1 บิลมีมากกว่า 1 คลัง
            //int SeqNo = 0, II = 1;
            //string Docno = "-";

            //try
            //{
            //    for (int i = 0; i < dtDataForSendAX.Rows.Count; i++)
            //    {
            //        if (Docno != dtDataForSendAX.Rows[i]["WHSoure"].ToString())
            //        {
            //            Docno = dtDataForSendAX.Rows[i]["WHSoure"].ToString();
            //            SeqNo += 1;
            //            II = 1;

            //            //HD
            //            sqlAX.Add(string.Format(@"
            //        insert into SPC_POSTOTABLE10 
            //            (FREIGHTSLIPTYPE,CASHIERID,ALLPRINTSTICKER,DATAAREAID,RECVERSION,RECID,TRANSFERSTATUS,
            //            INVENTLOCATIONIDTO, INVENTLOCATIONIDFROM, TRANSFERID, SHIPDATE, VOUCHERID,DLVTERM,REMARKS) 
            //        values('" + FREIGHTSLIPTYPE_0MNTF_98MNPO + "','" + dtDataForSendAX.Rows[i]["WHOINS"].ToString() + @"','0','SPC','1','1','0',
            //            '" + dtDataForSendAX.Rows[i]["WHDestination"].ToString() + @"', 
            //            '" + dtDataForSendAX.Rows[i]["WHSoure"].ToString() + @"',
            //            '" + dtDataForSendAX.Rows[i]["DOCNO"].ToString() + @"-" + SeqNo.ToString() + @"',
            //            '" + dtDataForSendAX.Rows[i]["DateIns"].ToString() + @"',
            //            '" + dtDataForSendAX.Rows[i]["DOCNO"].ToString() + @"-" + SeqNo.ToString() + @"', '" + dtDataForSendAX.Rows[i]["Lock"].ToString() + @"',
            //            '" + dtDataForSendAX.Rows[i]["RMK"].ToString() + @"' ) 
            //        "));

            //            //DT  
            //            sqlAX.Add(string.Format(@"
            //        insert into SPC_POSTOLINE10 
            //                    (RECID,REFBOXTRANS,DATAAREAID,
            //                    VOUCHERID,SHIPDATE,LINENUM,QTY,SALESPRICE,ITEMID,NAME,SALESUNIT,ITEMBARCODE, 
            //                    INVENTDIMID,INVENTTRANSID,RECVERSION) 
            //        values('1','0','SPC','" + dtDataForSendAX.Rows[i]["DOCNO"].ToString() + @"-" + SeqNo.ToString() + @"',
            //                    '" + dtDataForSendAX.Rows[i]["DateIns"].ToString() + @"',
            //                    '" + II + @"',
            //                    '" + Convert.ToDouble(dtDataForSendAX.Rows[i]["QtyOrder"].ToString()) + @"',
            //                    '" + Convert.ToDouble(dtDataForSendAX.Rows[i]["PRICE"].ToString()) + @"',
            //                    '" + dtDataForSendAX.Rows[i]["ITEMID"].ToString() + @"',
            //                    '" + dtDataForSendAX.Rows[i]["SPC_ITEMNAME"].ToString().Replace("'", "").Replace("{", "").Replace("}", "") + @"',
            //                    '" + dtDataForSendAX.Rows[i]["UnitID"].ToString() + @"',
            //                    '" + dtDataForSendAX.Rows[i]["ITEMBARCODE"].ToString() + @"',
            //                    '" + dtDataForSendAX.Rows[i]["INVENTDIM"].ToString() + @"',
            //                    '" + dtDataForSendAX.Rows[i]["DOCNO"].ToString() + @"-" + dtDataForSendAX.Rows[i]["LineNum"].ToString() + @"',
            //                    '1')
            //        "));
            //            II += 1;
            //        }
            //        else
            //        {
            //            sqlAX.Add(string.Format(@"
            //        insert into SPC_POSTOLINE10 
            //                    (RECID,REFBOXTRANS,DATAAREAID,
            //                    VOUCHERID,SHIPDATE,LINENUM,QTY,SALESPRICE,ITEMID,NAME,SALESUNIT,ITEMBARCODE, 
            //                    INVENTDIMID,INVENTTRANSID,RECVERSION) 
            //        values('1','0','SPC','" + dtDataForSendAX.Rows[i]["DOCNO"].ToString() + @"-" + SeqNo.ToString() + @"',
            //                    '" + dtDataForSendAX.Rows[i]["DateIns"].ToString() + @"',
            //                    '" + II + @"',
            //                    '" + Convert.ToDouble(dtDataForSendAX.Rows[i]["QtyOrder"].ToString()) + @"',
            //                    '" + Convert.ToDouble(dtDataForSendAX.Rows[i]["PRICE"].ToString()) + @"',
            //                    '" + dtDataForSendAX.Rows[i]["ITEMID"].ToString() + @"',
            //                    '" + dtDataForSendAX.Rows[i]["SPC_ITEMNAME"].ToString().Replace("'", "").Replace("{", "").Replace("}", "") + @"',
            //                    '" + dtDataForSendAX.Rows[i]["UnitID"].ToString() + @"',
            //                    '" + dtDataForSendAX.Rows[i]["ITEMBARCODE"].ToString() + @"',
            //                    '" + dtDataForSendAX.Rows[i]["INVENTDIM"].ToString() + @"',
            //                    '" + dtDataForSendAX.Rows[i]["DOCNO"].ToString() + @"-" + dtDataForSendAX.Rows[i]["LineNum"].ToString() + @"',
            //                    '1')
            //        "));
            //            II += 1;
            //        }
            //    }
            //    return ConnectionClass.ExecuteMain_AX_24_SameTime(sqlUp24, sqlAX);
            //}
            //catch (Exception ex)
            //{
            //    return ex.Message;
            //}

            ArrayList sqlAX = new ArrayList();
            //ในกรณีที่ต้องหาคลังก่อน 1 บิลมีมากกว่า 1 คลัง
            int SeqNo = 0, II = 1;
            string Docno = "-";

            try
            {
                for (int i = 0; i < dtDataForSendAX.Rows.Count; i++)
                {
                    if (Docno != dtDataForSendAX.Rows[i]["WHSoure"].ToString())
                    {
                        Docno = dtDataForSendAX.Rows[i]["WHSoure"].ToString();
                        SeqNo += 1;
                        II = 1;

                        //HD
                        sqlAX.Add(string.Format(@"
                    insert into SPC_POSTOTABLE10 
                        (FREIGHTSLIPTYPE,CASHIERID,ALLPRINTSTICKER,DATAAREAID,RECVERSION,RECID,TRANSFERSTATUS,
                        INVENTLOCATIONIDTO, INVENTLOCATIONIDFROM, TRANSFERID, SHIPDATE, VOUCHERID,DLVTERM,REMARKS) 
                    values('" + FREIGHTSLIPTYPE_0MNTF_98MNPO + "','" + dtDataForSendAX.Rows[i]["WHOINS"].ToString() + @"','0','SPC','1','1','0',
                        '" + dtDataForSendAX.Rows[i]["WHDestination"].ToString() + @"', 
                        '" + dtDataForSendAX.Rows[i]["WHSoure"].ToString() + @"',
                        '" + dtDataForSendAX.Rows[i]["DOCNO"].ToString() + @"-" + SeqNo.ToString() + @"',
                        '" + dtDataForSendAX.Rows[i]["DateIns"].ToString() + @"',
                        '" + dtDataForSendAX.Rows[i]["DOCNO"].ToString() + @"-" + SeqNo.ToString() + @"', '" + dtDataForSendAX.Rows[i]["Lock"].ToString() + @"',
                        '" + dtDataForSendAX.Rows[i]["RMK"].ToString() + @"' ) 
                    "));

                        //DT  
                        sqlAX.Add(string.Format(@"
                    insert into SPC_POSTOLINE10 
                                (RECID,REFBOXTRANS,DATAAREAID,
                                VOUCHERID,SHIPDATE,LINENUM,QTY,SALESPRICE,ITEMID,NAME,SALESUNIT,ITEMBARCODE, 
                                INVENTDIMID,INVENTTRANSID,RECVERSION) 
                    values('1','0','SPC','" + dtDataForSendAX.Rows[i]["DOCNO"].ToString() + @"-" + SeqNo.ToString() + @"',
                                '" + dtDataForSendAX.Rows[i]["DateIns"].ToString() + @"',
                                '" + II + @"',
                                '" + Convert.ToDouble(dtDataForSendAX.Rows[i]["QtyOrder"].ToString()) + @"',
                                '" + Convert.ToDouble(dtDataForSendAX.Rows[i]["PRICE"].ToString()) + @"',
                                '" + dtDataForSendAX.Rows[i]["ITEMID"].ToString() + @"',
                                '" + dtDataForSendAX.Rows[i]["SPC_ITEMNAME"].ToString().Replace("'", "").Replace("{", "").Replace("}", "") + @"',
                                '" + dtDataForSendAX.Rows[i]["UnitID"].ToString() + @"',
                                '" + dtDataForSendAX.Rows[i]["ITEMBARCODE"].ToString() + @"',
                                '" + dtDataForSendAX.Rows[i]["INVENTDIM"].ToString() + @"',
                                '" + dtDataForSendAX.Rows[i]["DOCNO"].ToString() + @"-" + dtDataForSendAX.Rows[i]["LineNum"].ToString() + @"',
                                '1')
                    "));
                        II += 1;
                    }
                    else
                    {
                        sqlAX.Add(string.Format(@"
                    insert into SPC_POSTOLINE10 
                                (RECID,REFBOXTRANS,DATAAREAID,
                                VOUCHERID,SHIPDATE,LINENUM,QTY,SALESPRICE,ITEMID,NAME,SALESUNIT,ITEMBARCODE, 
                                INVENTDIMID,INVENTTRANSID,RECVERSION) 
                    values('1','0','SPC','" + dtDataForSendAX.Rows[i]["DOCNO"].ToString() + @"-" + SeqNo.ToString() + @"',
                                '" + dtDataForSendAX.Rows[i]["DateIns"].ToString() + @"',
                                '" + II + @"',
                                '" + Convert.ToDouble(dtDataForSendAX.Rows[i]["QtyOrder"].ToString()) + @"',
                                '" + Convert.ToDouble(dtDataForSendAX.Rows[i]["PRICE"].ToString()) + @"',
                                '" + dtDataForSendAX.Rows[i]["ITEMID"].ToString() + @"',
                                '" + dtDataForSendAX.Rows[i]["SPC_ITEMNAME"].ToString().Replace("'", "").Replace("{", "").Replace("}", "") + @"',
                                '" + dtDataForSendAX.Rows[i]["UnitID"].ToString() + @"',
                                '" + dtDataForSendAX.Rows[i]["ITEMBARCODE"].ToString() + @"',
                                '" + dtDataForSendAX.Rows[i]["INVENTDIM"].ToString() + @"',
                                '" + dtDataForSendAX.Rows[i]["DOCNO"].ToString() + @"-" + dtDataForSendAX.Rows[i]["LineNum"].ToString() + @"',
                                '1')
                    "));
                        II += 1;
                    }
                }
                return ConnectionClass.ExecuteMain_AX_24_SameTime(sqlUp24, sqlAX);
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }
        //พิมบิลซ้ำ
        public static DataTable FindBillForPrint_DocnoMNOG(string pDocno)
        {
            return ConnectionClass.SelectSQL_Main("  CashDesktop_OrderCust '3','" + pDocno + @"' ");
        }
        //พิมบิลซ้ำ
        public static DataTable FindBillForPrint_DocnoMNPD(string pDocno)
        {
            return ConnectionClass.SelectSQL_Main("  CashDesktop_OrderCust '4','" + pDocno + @"' ");
        }
    }
}
