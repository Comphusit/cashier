﻿using System;
using System.Windows.Forms;
using Cashier.Class;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Collections;

namespace Cashier.F2_OrderCust
{
    public partial class OrderCust_Main : Telerik.WinControls.UI.RadForm
    {
        private readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();
        readonly PrintController printController = new StandardPrintController();

        readonly DataTable dtGrid = new DataTable();

        string custID, custName, custTel, custRmk, custReciveDate, custRecivePlace, custRecivePlaceName;
        string billMaxMNOG = "", billMaxMNPD = "";
        public OrderCust_Main()
        {
            InitializeComponent();
        }
        //Clear
        void ClearData()
        {
            if (dtGrid.Rows.Count > 0) dtGrid.Rows.Clear();
            custID = ""; custName = ""; custTel = ""; custRmk = ""; custReciveDate = ""; custRecivePlace = "";
            billMaxMNOG = ""; billMaxMNPD = "";
            radLabel_txt.Visible = false; radLabel_Grand.Visible = false;
            radLabel_CustName.Text = "";
            radLabel_Docno.Text = "";
            radLabel_ReciveID.Text = "";
            radLabel_ReciveName.Text = "";
            radTextBox_Barcode.Text = "";
            radLabel_X.Visible = false;
            radLabel_Qty.Visible = false; radLabel_Qty.Text = "1.00";
            radLabel_Grand.Text = "0.00";
            radTextBox_Barcode.Text = "";
            radTextBox_Barcode.Focus();
        }
        //load
        private void OrderCust_Main_Load(object sender, EventArgs e)
        {
            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;
            barcode.BarHeight = 45;
            barcode.LeftMargin = 30;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;

            ClearData();

            radLabel_Detail.Text = "Ctrl+F3 > ระบุลูกค้า | F3 > void สินค้า | F2 > ยกเลิกบิล | PdDn > Multiply | Home : รวม";

            radLabel_ReciveID.Text = SystemClass.SystemPOSGROUP;
            radLabel_ReciveName.Text = SystemClass.SystemZONEID;

            DatagridClass.SetDefaultRadGridView(radGridView_Show);

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 350));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("QtyOrder", "จำนวน", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMID", "รหัสสินค้า"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("INVENTDIMID", "มิติสินค้า"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("DIMENSION", "จัดซื้อ"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("DESCRIPTION", "DESCRIPTION"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddVisible("QTY", "อัตราส่วน"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("SPC_SalesPriceType", "SPC_SalesPriceType"));

            radGridView_Show.MasterTemplate.EnableFiltering = false;
            radGridView_Show.TableElement.RowHeight = 60;

            dtGrid.Columns.Add("ITEMBARCODE");
            dtGrid.Columns.Add("SPC_ITEMNAME");
            dtGrid.Columns.Add("QtyOrder");
            dtGrid.Columns.Add("UNITID");
            dtGrid.Columns.Add("ITEMID");
            dtGrid.Columns.Add("INVENTDIMID");
            dtGrid.Columns.Add("DIMENSION");
            dtGrid.Columns.Add("DESCRIPTION");
            dtGrid.Columns.Add("QTY");
            dtGrid.Columns.Add("SPC_SalesPriceType");

            radTextBox_Barcode.Focus();
        }
        //Format Cell
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            e.CellElement.Font = SystemClass.SetFont12;
            try
            {
                if (e.CellElement is GridRowHeaderCellElement && e.Row is GridViewDataRowInfo)
                {
                    e.CellElement.Text = (e.CellElement.RowIndex + 1).ToString();
                    e.CellElement.TextImageRelation = TextImageRelation.ImageBeforeText;
                }
                else
                {
                    e.CellElement.ResetValue(LightVisualElement.TextImageRelationProperty, ValueResetFlags.Local);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        //F2 remove
        private void RadGridView_Show_KeyDown(object sender, KeyEventArgs e)
        {
            if (dtGrid.Rows.Count == 0) return;

            switch (e.KeyCode)
            {
                case Keys.F3: F3(radGridView_Show.CurrentRow.Index); break;
                case Keys.Escape: radTextBox_Barcode.Focus(); break;
                default:
                    break;
            }
        }
        //Enter Barcode
        private void RadTextBox_Barcode_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Control) && (e.KeyCode == Keys.F3))
            {
                CustInput();
                return;
            }

            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (radTextBox_Barcode.Text == "")
                    {
                        MsgBoxClass.MassageBoxShowButtonOk_Warning("ระบุบาร์โค้ดสินค้าก่อน Enter.", SystemClass.HeaderOrder);
                        radTextBox_Barcode.Focus();
                        return;
                    }
                    EnterTextbox(radTextBox_Barcode.Text);
                    break;

                case Keys.PageDown:
                    if (radTextBox_Barcode.Text == "")
                    {
                        return;
                    }
                    int qty;
                    try
                    {
                        qty = Convert.ToInt32((radTextBox_Barcode.Text));
                    }
                    catch (Exception)
                    {
                        qty = 0;
                    }
                    //check qty
                    if (qty <= 0)
                    {
                        MsgBoxClass.MassageBoxShowButtonOk_Warning("จำนวนที่ระบุต้องมากกว่า 0 เท่านั้น.", SystemClass.HeaderOrder);
                        radTextBox_Barcode.SelectAll();
                        radTextBox_Barcode.Focus();
                        return;
                    }
                    if (qty > 10000)
                    {
                        MsgBoxClass.MassageBoxShowButtonOk_Warning("จำนวนสินค้าที่ระบุมากผิดปกติ เช็คใหม่อีกครั้ง", SystemClass.HeaderBuyCust);
                        radTextBox_Barcode.SelectAll();
                        radTextBox_Barcode.Focus();
                        return;
                    }
                    radLabel_X.Visible = true;
                    radLabel_Qty.Text = qty.ToString("N2");
                    radLabel_Qty.Visible = true;
                    radTextBox_Barcode.SelectAll();
                    radTextBox_Barcode.Focus();
                    break;

                case Keys.F2:
                    F2();
                    break;

                case Keys.F3:
                    if (dtGrid.Rows.Count == 0) return;
                    F3(dtGrid.Rows.Count - 1);
                    break;

                case Keys.Home:
                    Home();
                    break;

                case Keys.Escape:
                    if (radGridView_Show.Rows.Count == 0) Application.Exit();
                    break;
                default:
                    return;
            }
        }
        //Enter Barcode
        void EnterTextbox(string barcode)
        {
            //ค้นหารายละเอียดสินค้า
            DataTable dtBarcode = OrderCust.GetBarcodeDetail_Barcode(barcode);
            if (dtBarcode.Rows.Count == 0)
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning("ไม่พบข้อมูลสินค้าของบาร์โค้ดที่ระบุ" + Environment.NewLine + "ลองใหม่อีกครั้ง.", SystemClass.HeaderOrder);
                radTextBox_Barcode.SelectAll(); radTextBox_Barcode.Focus();
                return;
            }
            //CheckPrice = 0
            if (Convert.ToDouble(dtBarcode.Rows[0][SystemClass.SystemPRICEFIELDNAME].ToString()) == 0)
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning("สินค้าที่ระบุ ไม่สามารถขายได้" + Environment.NewLine + "เนื่องจากราคาเป็น 0", SystemClass.HeaderOrder);
                radTextBox_Barcode.SelectAll(); radTextBox_Barcode.Focus();
                return;
            }
            //Check HT
            if (dtBarcode.Rows[0]["SPC_ITEMNAME"].ToString().Contains("HT"))
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning("สินค้าที่ระบุ ไม่สามารถขายได้" + Environment.NewLine + @"เนื่องจากเป็นบาร์โค้ดสำหรับโรงแกมเท่านั้น", SystemClass.HeaderOrder);
                radTextBox_Barcode.SelectAll(); radTextBox_Barcode.Focus();
                return;
            }
            //Check In Grid
            int iRows = CheckBarcodeInGrid(dtBarcode.Rows[0]["ITEMBARCODE"].ToString());
            if (iRows == 999)
            {

                dtGrid.Rows.Add(dtBarcode.Rows[0]["ITEMBARCODE"].ToString(),
                    dtBarcode.Rows[0]["SPC_ITEMNAME"].ToString(), Convert.ToDouble(radLabel_Qty.Text).ToString("N2"),
                    dtBarcode.Rows[0]["UNITID"].ToString(), dtBarcode.Rows[0]["ITEMID"].ToString(),
                    dtBarcode.Rows[0]["INVENTDIMID"].ToString(), dtBarcode.Rows[0]["DIMENSION"].ToString(), dtBarcode.Rows[0]["DESCRIPTION"].ToString(),
                    dtBarcode.Rows[0]["QTY"].ToString(), dtBarcode.Rows[0]["SPC_SalesPriceType"].ToString());

                radGridView_Show.DataSource = dtGrid;
                dtGrid.AcceptChanges();
            }
            else
            {
                double qty_up = Convert.ToDouble(radGridView_Show.Rows[iRows].Cells["QtyOrder"].Value.ToString());
                qty_up += Convert.ToDouble(radLabel_Qty.Text);
                radGridView_Show.Rows[iRows].Cells["QtyOrder"].Value = qty_up.ToString("N2");
                dtGrid.AcceptChanges();
            }


            radLabel_X.Visible = false;
            radLabel_Qty.Text = "1";
            radLabel_Qty.Visible = false;
            radTextBox_Barcode.Text = "";
            radTextBox_Barcode.Focus();
            radGridView_Show.Rows[dtGrid.Rows.Count - 1].IsCurrent = true;
        }
        //CheckBarcode IN Grid
        int CheckBarcodeInGrid(string barcode)
        {
            int iCheck = 999;
            for (int i = 0; i < radGridView_Show.Rows.Count; i++)
            {
                if (radGridView_Show.Rows[i].Cells["ITEMBARCODE"].Value.ToString() == barcode)
                {
                    iCheck = i;
                    break;
                }
            }
            return iCheck;
        }
        // Sava Data
        void SaveDataSendAX()
        {
            ArrayList sql24 = new ArrayList();
            int iRowsMNOG = 1;
            int iRowsMNPD = 1;

            for (int i = 0; i < radGridView_Show.Rows.Count; i++)
            {
                if (SystemClass.SystemPurchaseFresh.Contains(radGridView_Show.Rows[i].Cells["DIMENSION"].Value.ToString()) == true)
                {
                    if (billMaxMNOG == "")
                    {
                        billMaxMNOG = Class.ConfigClass.GetMaxINVOICEID("MNOG", "-", "MNOG", "1");
                    }

                    //sql24.Add(String.Format(@"
                    //INSERT INTO SHOP2013.dbo.Shop_MNOG_OrderDT 
                    //    (MNPODocNo,MNPOSeqNo,MNPOItemID,MNPODimid,MNPOBarcode 
                    //   ,MNPOName,MNPOQtyOrder,MNPOUnitID,MNPOFactor,MNPOWhoIn,NUMID,NUMDesc ) 
                    //VALUES ('" + billMaxMNOG + @"','" + iRowsMNOG + @"',
                    //   '" + radGridView_Show.Rows[i].Cells["ITEMID"].Value.ToString() + @"',
                    //   '" + radGridView_Show.Rows[i].Cells["INVENTDIMID"].Value.ToString() + @"',
                    //   '" + radGridView_Show.Rows[i].Cells["ITEMBARCODE"].Value.ToString() + @"',
                    //   '" + radGridView_Show.Rows[i].Cells["SPC_ITEMNAME"].Value.ToString().Replace("'", "").Replace("{", "").Replace("}", "") + @"',
                    //   '" + Convert.ToDouble(radGridView_Show.Rows[i].Cells["QtyOrder"].Value.ToString()) + @"',
                    //   '" + radGridView_Show.Rows[i].Cells["UNITID"].Value.ToString() + @"',
                    //   '" + Convert.ToDouble(radGridView_Show.Rows[i].Cells["QTY"].Value.ToString()) + @"',
                    //   '" + SystemClass.SystemUserID + @"','" + radGridView_Show.Rows[i].Cells["DIMENSION"].Value.ToString() + @"',
                    //    '" + radGridView_Show.Rows[i].Cells["DESCRIPTION"].Value.ToString() + @"'
                    //   ) "));

                    sql24.Add(String.Format(@"
                    INSERT INTO SHOP_MNOG_DT
                        (DOCNO,LINENUM,ITEMID,INVENTDIMID,ITEMBARCODE
                       ,SPC_ITEMNAME,QTYORDER,UNITID,FACTOR,WHOINS,WHOINSNAME,NUMID,NUMDesc ) 
                    VALUES ('" + billMaxMNOG + @"','" + iRowsMNOG + @"',
                       '" + radGridView_Show.Rows[i].Cells["ITEMID"].Value.ToString() + @"',
                       '" + radGridView_Show.Rows[i].Cells["INVENTDIMID"].Value.ToString() + @"',
                       '" + radGridView_Show.Rows[i].Cells["ITEMBARCODE"].Value.ToString() + @"',
                       '" + radGridView_Show.Rows[i].Cells["SPC_ITEMNAME"].Value.ToString().Replace("'", "").Replace("{", "").Replace("}", "") + @"',
                       '" + Convert.ToDouble(radGridView_Show.Rows[i].Cells["QtyOrder"].Value.ToString()) + @"',
                       '" + radGridView_Show.Rows[i].Cells["UNITID"].Value.ToString() + @"',
                       '" + Convert.ToDouble(radGridView_Show.Rows[i].Cells["QTY"].Value.ToString()) + @"',
                       '" + SystemClass.SystemUserID + @"','" + SystemClass.SystemUserName + @"','" + radGridView_Show.Rows[i].Cells["DIMENSION"].Value.ToString() + @"',
                        '" + radGridView_Show.Rows[i].Cells["DESCRIPTION"].Value.ToString() + @"'
                       ) "));

                    iRowsMNOG++;
                }
                else
                {
                    if (billMaxMNPD == "")
                    {
                        billMaxMNPD = Class.ConfigClass.GetMaxINVOICEID("MNPD", "-", "MNPD", "1");
                    }
                    //sql24.Add(String.Format(@"
                    //INSERT INTO SHOP2013.dbo.Shop_OrderPDDT 
                    //    (DocNo,SeqNo,ItemID,Dimid,Barcode
                    //   ,ItemName,Qty,UnitID,Factor,WHOIN) 
                    //VALUES ('" + billMaxMNPD + @"','" + iRowsMNPD + @"',
                    //   '" + radGridView_Show.Rows[i].Cells["ITEMID"].Value.ToString() + @"',
                    //   '" + radGridView_Show.Rows[i].Cells["INVENTDIMID"].Value.ToString() + @"',
                    //   '" + radGridView_Show.Rows[i].Cells["ITEMBARCODE"].Value.ToString() + @"',
                    //   '" + radGridView_Show.Rows[i].Cells["SPC_ITEMNAME"].Value.ToString().Replace("'", "").Replace("{", " ").Replace("}", " ") + @"',
                    //   '" + Convert.ToDouble(radGridView_Show.Rows[i].Cells["QtyOrder"].Value.ToString()) + @"',
                    //   '" + radGridView_Show.Rows[i].Cells["UNITID"].Value.ToString() + @"',
                    //   '" + Convert.ToDouble(radGridView_Show.Rows[i].Cells["QTY"].Value.ToString()) + @"',
                    //   '" + SystemClass.SystemUserID + @"'
                    //   ) "));


                    sql24.Add(String.Format(@"
                    INSERT INTO SHOP_MNPD_DT 
                        (DOCNO,LINENUM,ITEMID,INVENTDIMID,ITEMBARCODE,SPC_NAME,QTYORDER,UNITID,FACTOR,WHOINS,WHOINSNAME) 
                    VALUES ('" + billMaxMNPD + @"','" + iRowsMNPD + @"',
                       '" + radGridView_Show.Rows[i].Cells["ITEMID"].Value.ToString() + @"',
                       '" + radGridView_Show.Rows[i].Cells["INVENTDIMID"].Value.ToString() + @"',
                       '" + radGridView_Show.Rows[i].Cells["ITEMBARCODE"].Value.ToString() + @"',
                       '" + radGridView_Show.Rows[i].Cells["SPC_ITEMNAME"].Value.ToString().Replace("'", "").Replace("{", " ").Replace("}", " ") + @"',
                       '" + Convert.ToDouble(radGridView_Show.Rows[i].Cells["QtyOrder"].Value.ToString()) + @"',
                       '" + radGridView_Show.Rows[i].Cells["UNITID"].Value.ToString() + @"',
                       '" + Convert.ToDouble(radGridView_Show.Rows[i].Cells["QTY"].Value.ToString()) + @"',
                       '" + SystemClass.SystemUserID + @"','" + SystemClass.SystemUserName + @"'
                       ) "));
                    iRowsMNPD++;
                }
            }

            if (billMaxMNOG != "")
            {
                ////HD 24
                //sql24.Add(@"
                //    INSERT INTO SHOP2013.dbo.Shop_MNOG_OrderHD 
	               //         (MNPODocNo,MNPOUserCode,MNPOBranch,MNPOBranchName,
	               //         MNPORemark,MNPOWhoIn,MNPOWhoInName,MNPOTYPE_PCORPDA,MNPODptCode,OPENBY,
                //            MNPOStaPrcDoc,CstID,CstName,CstTel,RECIVE_DATE,
                //            MNPODateUp,MNPOTimeUp,MNPOWhoUp,MNPOWhoUpName,CountPrt
                //            ) 
                //    VALUES ('" + billMaxMNOG + @"','" + SystemClass.SystemUserID + @"','" + SystemClass.SystemPOSGROUP + @"','" + SystemClass.SystemZONEID + @"',
                //            '" + custRmk + @"','" + SystemClass.SystemUserID + @"','" + SystemClass.SystemUserName + @"',
                //            '" + SystemClass.SystemPcName + @"','" + SystemClass.SystemPOSGROUP + @"','OrderCustByCash',
                //            '1','" + custID + @"','" + custName + @"','" + custTel + @"','" + custReciveDate + @"',
                //            CONVERT(VARCHAR,GETDATE(),23),CONVERT(VARCHAR,GETDATE(),24),'" + SystemClass.SystemUserID + @"','" + SystemClass.SystemUserName + @"','1'
                //            )");

                //HD 24
                sql24.Add(@"
                    INSERT INTO SHOP_MNOG_HD
                         (DOCNO,BRANCH_ID,BRANCH_NAME,
                         REMARK, WHOINS, WHOINSNAME, MACHINE, DPTID, OPENBY,
                            STAPRCDOC,CUST_ID,CUST_NAME,CUST_TEL,RECIVE_DATE,
                            COUNT_PRINT
                            ) 
                    VALUES ('" + billMaxMNOG + @"','" + SystemClass.SystemPOSGROUP + @"','" + SystemClass.SystemZONEID + @"',
                            '" + custRmk + @"','" + SystemClass.SystemUserID + @"','" + SystemClass.SystemUserName + @"',
                            '" + SystemClass.SystemPcName + @"','" + SystemClass.SystemPOSGROUP + @"','OrderCustByCash',
                            '1','" + custID + @"','" + custName + @"','" + custTel + @"','" + custReciveDate + @"',
                           '1'
                            )");
            }

            if (billMaxMNPD != "")
            {
                ////HD 24
                //sql24.Add(@"
                //    INSERT INTO SHOP2013.dbo.Shop_OrderPDHD 
                //         (DocNo,UserCode,Branch,BranchName,
                //         Remark,WhoIn,WhoInName,MNPODptCode,OPENBY,
                //            CstID,Customer,TYPEPRODUCT,Tel,CountPrt
                //            ) 
                //    VALUES ('" + billMaxMNPD + @"','" + SystemClass.SystemUserID + @"','" + SystemClass.SystemPOSGROUP + @"','" + SystemClass.SystemZONEID + @"',
                //            '" + custRmk + @"','" + SystemClass.SystemUserID + @"','" + SystemClass.SystemUserName + @"',
                //            '" + SystemClass.SystemPOSGROUP + @"','OrderCustByCash',
                //            '" + custID + @"','" + custName + @"','" + custRecivePlace + @"','" + custTel + @"','1'
                //    ) ");
                //HD 24     
                sql24.Add(@"
                    INSERT INTO SHOP_MNPD_HD 
	                        (DOCNO,BRANCH_ID,BRANCH_NAME,
	                        Remark,WHOINS,WHOINSNAME,DPTID,OPENBY,
                            CUST_ID,CUST_NAME,TYPEPRODUCT,CUST_TEL,COUNT_PRINT
                            ) 
                    VALUES ('" + billMaxMNPD + @"','" + SystemClass.SystemPOSGROUP + @"','" + SystemClass.SystemZONEID + @"',
                            '" + custRmk + @"','" + SystemClass.SystemUserID + @"','" + SystemClass.SystemUserName + @"','" + SystemClass.SystemPOSGROUP + @"','OrderCustByCash',
                            '" + custID + @"','" + custName + @"','" + custRecivePlace + @"','" + custTel + @"','1'
                    ) ");
            }

            string result = ConnectionClass.ExecuteSQL_ArrayMain(sql24);
            if (result == "")
            {
                ArrayList sqlUp24 = OrderCust.SqlUpdateMNPD_Apv(billMaxMNPD);
                string resultAX;
                if ((billMaxMNPD != "") && (custRecivePlace == "2"))
                {
                    DataTable dtForSend = OrderCust.GetDataDetailMNPD_ForSendAX(billMaxMNPD);
                    if (dtForSend.Rows.Count == 0)
                    {
                        MsgBoxClass.MassageBoxShowButtonOk_Warning("ไม่พบข้อมูลออเดอร์สินค้าลูกค้าเลขที่ " + radLabel_Docno.Text +
                            Environment.NewLine + "เช็คข้อมูลใหม่อีกครั้ง.", SystemClass.HeaderOrder);
                        return;
                    }
                    resultAX = OrderCust.SendAX_SPC_POSTOTABLE10(sqlUp24, dtForSend, "98");
                }
                else
                {
                    resultAX = ConnectionClass.ExecuteSQL_ArrayMain(sqlUp24);
                }

                if (resultAX == "")
                {
                    PrintDocBill();
                    Application.Exit();
                }
                else
                {
                    MsgBoxClass.MassageBoxShowButtonOk_Error("ไม่สามารถบันทึกยืนยันได้ ลองใหม่อีกครั้ง" + Environment.NewLine + resultAX, SystemClass.HeaderOrder);
                    radTextBox_Barcode.SelectAll();
                    radTextBox_Barcode.Focus();
                    return;
                }
            }
            else
            {
                MsgBoxClass.MassageBoxShowButtonOk_Error("ไม่สามารถบันทึกข้อมูลได้ ลองใหม่อีกครั้ง" + Environment.NewLine + result, SystemClass.HeaderOrder);
                radTextBox_Barcode.SelectAll();
                radTextBox_Barcode.Focus();
                return;
            }
        }
        //F2
        void F2()
        {
            if (dtGrid.Rows.Count == 0) ClearData();

            if (MsgBoxClass.MassageBoxShowYesNo_DialogResult("ยืนยันการยกเลิกบิลที่ดำเนินการอยู่ ?.", SystemClass.HeaderOrder) == DialogResult.No) return;

            Application.Exit();
        }
        //F3
        void F3(int iRows)
        {
            if (dtGrid.Rows.Count == 0) return;


            String desc = dtGrid.Rows[iRows]["ITEMBARCODE"].ToString() + " " + dtGrid.Rows[iRows]["SPC_ITEMNAME"].ToString() +
                " จำนวน  " + dtGrid.Rows[iRows]["QtyOrder"].ToString() + "  " + dtGrid.Rows[iRows]["UNITID"].ToString();

            if (MessageBox.Show("ยืนยันการยกเลิกรายการ " + Environment.NewLine + desc + " ?.", SystemClass.HeaderOrder,
                       MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;

            radGridView_Show.Rows.RemoveAt(iRows);
            dtGrid.AcceptChanges();
            radTextBox_Barcode.SelectAll();
            radTextBox_Barcode.Focus();

            if (radGridView_Show.Rows.Count > 0)
            {
                radGridView_Show.Rows[dtGrid.Rows.Count - 1].IsCurrent = true;
            }

        }
        //Cust
        string CustInput()
        {
            OrderCust_Cust _orderCust = new OrderCust_Cust(custID, custName, custTel, custRmk,
                custReciveDate, custRecivePlaceName);
            if (_orderCust.ShowDialog(this) == DialogResult.Yes)
            {
                custID = _orderCust.custID;
                custName = _orderCust.custName;
                custTel = _orderCust.custTel;
                custRmk = _orderCust.custRmk;
                custReciveDate = _orderCust.custReciveDate;
                custRecivePlace = _orderCust.custRecivePlace;
                custRecivePlaceName = _orderCust.custRecivePlaceName;
                radLabel_CustName.Text = "ลูกค้า " + custName + " " + custTel;
                radLabel_Docno.Text = custRecivePlaceName;
                return "1";
            }
            else
            {
                return "0";
            }
        }
        //Home
        void Home()
        {
            if (dtGrid.Rows.Count == 0) return;

            if ((custName == "") || (custTel == ""))
            {
                if (CustInput() == "0")
                {
                    return;
                }
            }

            GeneralForm.FormHome _home = new GeneralForm.FormHome("F2", "0", custRecivePlaceName, "0.00", radGridView_Show.Rows.Count.ToString("N2"));
            if (_home.ShowDialog(this) == DialogResult.Yes)
            {
                SaveDataSendAX();
            }
            else
            {
                radTextBox_Barcode.SelectAll();
                radTextBox_Barcode.Focus();
                return;
            }
        }
        //พิมพ์เอกสาร
        private void PrintDocument_printBill_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            int Y = 0;
            e.Graphics.DrawString("ออเดอร์ลูกค้า [" + custRecivePlaceName + @"]", SystemClass.SetFont12, Brushes.Black, 10, Y);

            if (billMaxMNOG != "")
            {
                Y += 25;
                e.Graphics.DrawString("ของสด - " + billMaxMNOG, SystemClass.SetFont12, Brushes.Black, 10, Y);
            }

            if (billMaxMNPD != "")
            {
                Y += 25;
                e.Graphics.DrawString("ของแห้ง - " + billMaxMNPD, SystemClass.SetFont12, Brushes.Black, 10, Y);
            }

            Y += 25;
            e.Graphics.DrawString(SystemClass.SystemPOSGROUP + " " + SystemClass.SystemZONEID, SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 20;
            e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 20;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            for (int ii = 0; ii < radGridView_Show.Rows.Count; ii++)
            {
                Y += 20;
                e.Graphics.DrawString((ii + 1).ToString() + ".(" + (Convert.ToDouble(radGridView_Show.Rows[ii].Cells["QtyOrder"].Value).ToString("#,#0.00")).ToString() + " X " +
                                  radGridView_Show.Rows[ii].Cells["UNITID"].Value.ToString() + ")   " +
                                  radGridView_Show.Rows[ii].Cells["ITEMBARCODE"].Value.ToString(),
                     SystemClass.printFont, Brushes.Black, 1, Y);
                Y += 15;
                e.Graphics.DrawString(radGridView_Show.Rows[ii].Cells["SPC_ITEMNAME"].Value.ToString(),
                    SystemClass.printFont, Brushes.Black, 0, Y);

            }
            Y += 15;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString("จำนวนทั้งหมด " + radGridView_Show.Rows.Count.ToString("N2") + @"  รายการ", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("แคชเชียร์ : " + SystemClass.SystemUserID, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString(SystemClass.SystemUserName, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString("ลูกค้า : " + custID + " โทร : " + custTel, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString(custName, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("วันที่รับต้องการของ : " + custReciveDate, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("หมายเหตุ : " + custRmk, SystemClass.printFont, Brushes.Black, 0, Y);

            e.Graphics.PageUnit = GraphicsUnit.Inch;
        }

        //print
        void PrintDocBill()
        {
            System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
            PrinterSettings settings = new PrinterSettings();
            settings.DefaultPageSettings.PaperSize = ps;
            string defaultPrinterName = settings.PrinterName;

            PrintDocument_printBill.PrintController = printController;
            PrintDocument_printBill.PrinterSettings.PrinterName = defaultPrinterName;

            PrintDocument_printBill.Print();
        }
    }
}
