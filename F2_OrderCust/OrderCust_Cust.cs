﻿using System;
using System.Data;
using System.Windows.Forms;
using Cashier.Class; 

namespace Cashier.F2_OrderCust
{
    public partial class OrderCust_Cust : Telerik.WinControls.UI.RadForm
    {
        public string custID, custName, custTel, custRmk, custReciveDate, custRecivePlace, custRecivePlaceName;
        readonly string _custID, _custName, _custTel, _custRmk, _custReciveDate, _custRecivePlaceName;

        private void RadTextBox_CustRmk_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode==Keys.Enter)
            {
                radButton_Choose.Focus();
            }           
        }

        private void RadTextBox_CustTel_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                radTextBox_CustRmk.Focus();
            }           
        }

        private void RadTextBox_CustName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                radTextBox_CustTel.Focus();
            }
          
        }

        //Input Find Cust
        private void RadTextBox_CustID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_CustID.Text == "") return;
                DataTable dtCust = OrderCust.GetDetailCust(radTextBox_CustID.Text.Trim());
                if (dtCust.Rows.Count == 0)
                {
                    MsgBoxClass.MassageBoxShowButtonOk_Warning("ไม่พบรหัสูกค้าที่ระบุ ลองใหม่อีกครั้ง.", SystemClass.HeaderOrder);
                    radTextBox_CustID.SelectAll();
                    radTextBox_CustID.Focus();
                    return;
                }
                else
                {
                    radTextBox_CustID.Text = dtCust.Rows[0]["ACCOUNTNUM"].ToString();
                    radTextBox_CustName.Text = dtCust.Rows[0]["NAME"].ToString();
                    radTextBox_CustTel.Text = dtCust.Rows[0]["PHONE"].ToString();
                    if (radTextBox_CustTel.Text == "")
                    {
                        radTextBox_CustTel.Focus();
                    }
                    else
                    {
                        radTextBox_CustRmk.Focus();
                    }
                    return;
                }
            }
        }

        //NumberOnly
        private void RadTextBox_CustTel_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        //Load
        public OrderCust_Cust(string pCustID, string pCustName, string pCustTel, string pCustRmk,
            string pCustReciveDate, string pCustRecivePlaceName)
        {
            InitializeComponent();

            _custID = pCustID; _custName = pCustName; _custTel = pCustTel; _custRmk = pCustRmk;
            _custReciveDate = pCustReciveDate;
            //  _custRecivePlace = pCustRecivePlace; 
            _custRecivePlaceName = pCustRecivePlaceName;


            radButton_Choose.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;
        }
        //Clear
        void ClearData()
        {
            radTextBox_CustID.Text = _custID;
            radTextBox_CustName.Text = _custName;
            radTextBox_CustTel.Text = _custTel;
            radTextBox_CustRmk.Text = _custRmk;

            if (_custReciveDate == "")
            {
                radDateTimePicker_D1.Value = DateTime.Now.AddDays(1);
            }
            else
            {
                if (Convert.ToDateTime(_custReciveDate) <= DateTime.Now.AddDays(1))
                {
                    radDateTimePicker_D1.Value = DateTime.Now.AddDays(1);
                }
                else
                {
                    radDateTimePicker_D1.Value = Convert.ToDateTime(_custReciveDate);
                }
            }

            switch (_custRecivePlaceName)
            {
                case "":
                    radioButton_SPC.Checked = false;
                    radioButton_Bch.Checked = false;
                    break;
                case "ลูกค้ารอรับสินค้า":
                    radioButton_SPC.Checked = true;
                    radioButton_Bch.Checked = false;
                    break;
                case "ลูกค้ารับสินค้าเลย":
                    radioButton_SPC.Checked = false;
                    radioButton_Bch.Checked = true;
                    break;
                default:
                    break;
            }

            radTextBox_CustID.Focus();
        }
        //load
        private void OrderCust_Cust_Load(object sender, EventArgs e)
        {
            DatagridClass.SetDefaultFontDateTimePicker(radDateTimePicker_D1, DateTime.Now.AddDays(1), DateTime.Now.AddDays(30));

            radioButton_Bch.Text = "ลูกค้ารับสินค้าเลย" + Environment.NewLine + "(เอาสินค้าจากสาขาให้)";
            radioButton_SPC.Text = "ลูกค้ารอรับสินค้า" + Environment.NewLine + "(รอสินค้าจากสาขาใหญ่)";

            radLabel_Detail.Text = "ระบุวันที่ ลูกค้าต้องการสินค้า | ระบุข้อมูลลูกค้า > กด ตกลง";

            ClearData();


        }
        //ปิด
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        //Choose
        private void RadButton_Choose_Click(object sender, EventArgs e)
        {

            if ((radioButton_SPC.Checked == false) && (radioButton_Bch.Checked == false))
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning("ต้องระบุการรับสินค้าของลูกค้าให้เรียบร้อย เท่านั้น.", SystemClass.HeaderOrder);
                return;
            }

            if (radTextBox_CustName.Text == "")
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning("ต้องระบุการชื่อของลูกค้าให้เรียบร้อย เท่านั้น.", SystemClass.HeaderOrder);
                radTextBox_CustName.Focus();
                return;
            }

            if (radTextBox_CustTel.Text == "")
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning("ต้องระบุการเบอร์โทรของลูกค้าให้เรียบร้อย เท่านั้น.", SystemClass.HeaderOrder);
                radTextBox_CustTel.Focus();
                return;
            }

            custID = radTextBox_CustID.Text;
            custName = radTextBox_CustName.Text;
            custTel = radTextBox_CustTel.Text;
            custRmk = radTextBox_CustRmk.Text;
            custReciveDate = radDateTimePicker_D1.Value.ToString("yyyy-MM-dd");

            custRecivePlace = "1";// ลูกค้ารับสินค้าเลย 
            custRecivePlaceName = "ลูกค้ารับสินค้าเลย";
            if (radioButton_SPC.Checked == true)
            {
                custRecivePlace = "2";//ลูกค้ารอรับสินค้า
                custRecivePlaceName = "ลูกค้ารอรับสินค้า";
            }

            this.DialogResult = DialogResult.Yes;
            this.Close();
        }


    }
}


