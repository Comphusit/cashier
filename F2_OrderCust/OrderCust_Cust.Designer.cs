﻿namespace Cashier.F2_OrderCust
{
    partial class OrderCust_Cust
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrderCust_Cust));
            this.radButton_Choose = new Telerik.WinControls.UI.RadButton();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_Detail = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radLabel5 = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_CustRmk = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_CustTel = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_CustName = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_CustID = new Telerik.WinControls.UI.RadTextBox();
            this.radDateTimePicker_D1 = new Telerik.WinControls.UI.RadDateTimePicker();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel3 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel2 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel1 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_Input = new Telerik.WinControls.UI.RadLabel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.radioButton_SPC = new System.Windows.Forms.RadioButton();
            this.radioButton_Bch = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Choose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CustRmk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CustTel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CustName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CustID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_D1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Input)).BeginInit();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radButton_Choose
            // 
            this.radButton_Choose.Dock = System.Windows.Forms.DockStyle.Right;
            this.radButton_Choose.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Choose.Location = new System.Drawing.Point(144, 3);
            this.radButton_Choose.Name = "radButton_Choose";
            this.radButton_Choose.Size = new System.Drawing.Size(120, 38);
            this.radButton_Choose.TabIndex = 0;
            this.radButton_Choose.Text = "ตกลง";
            this.radButton_Choose.ThemeName = "Fluent";
            this.radButton_Choose.Click += new System.EventHandler(this.RadButton_Choose_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Choose.GetChildAt(0))).Text = "ตกลง";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Choose.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Choose.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Choose.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Choose.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Choose.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Choose.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Choose.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Dock = System.Windows.Forms.DockStyle.Left;
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancel.Location = new System.Drawing.Point(270, 3);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(120, 38);
            this.radButton_Cancel.TabIndex = 1;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.radLabel_Detail, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(540, 424);
            this.tableLayoutPanel1.TabIndex = 45;
            // 
            // radLabel_Detail
            // 
            this.radLabel_Detail.AutoSize = false;
            this.radLabel_Detail.BackColor = System.Drawing.Color.LightPink;
            this.radLabel_Detail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_Detail.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radLabel_Detail.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Detail.Location = new System.Drawing.Point(3, 402);
            this.radLabel_Detail.Name = "radLabel_Detail";
            this.radLabel_Detail.Size = new System.Drawing.Size(534, 19);
            this.radLabel_Detail.TabIndex = 54;
            this.radLabel_Detail.Text = "<html>เลือก รหัส/บริษัท &gt;&gt; กด เลือก | สีแดง &gt;&gt; ลูกค้าไม่สามารถทำบิลได" +
    "้ [กด เลือก เพื่อแสดงข้อความ]</html>";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.radButton_Cancel, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.radButton_Choose, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 352);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(534, 44);
            this.tableLayoutPanel2.TabIndex = 45;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radLabel5);
            this.panel1.Controls.Add(this.radTextBox_CustRmk);
            this.panel1.Controls.Add(this.radTextBox_CustTel);
            this.panel1.Controls.Add(this.radTextBox_CustName);
            this.panel1.Controls.Add(this.radTextBox_CustID);
            this.panel1.Controls.Add(this.radDateTimePicker_D1);
            this.panel1.Controls.Add(this.radLabel4);
            this.panel1.Controls.Add(this.radLabel3);
            this.panel1.Controls.Add(this.radLabel2);
            this.panel1.Controls.Add(this.radLabel1);
            this.panel1.Controls.Add(this.radLabel_Input);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Font = new System.Drawing.Font("Tahoma", 12F);
            this.panel1.Location = new System.Drawing.Point(3, 63);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(534, 283);
            this.panel1.TabIndex = 0;
            // 
            // radLabel5
            // 
            this.radLabel5.Font = new System.Drawing.Font("Tahoma", 8F);
            this.radLabel5.ForeColor = System.Drawing.Color.Green;
            this.radLabel5.Location = new System.Drawing.Point(360, 71);
            this.radLabel5.Name = "radLabel5";
            this.radLabel5.Size = new System.Drawing.Size(147, 16);
            this.radLabel5.TabIndex = 30;
            this.radLabel5.Text = "*รหัส-เบอร์-บัตร ปชช-ชื่อค้นหา";
            // 
            // radTextBox_CustRmk
            // 
            this.radTextBox_CustRmk.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_CustRmk.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_CustRmk.Location = new System.Drawing.Point(179, 214);
            this.radTextBox_CustRmk.Multiline = true;
            this.radTextBox_CustRmk.Name = "radTextBox_CustRmk";
            // 
            // 
            // 
            this.radTextBox_CustRmk.RootElement.StretchVertically = true;
            this.radTextBox_CustRmk.Size = new System.Drawing.Size(339, 47);
            this.radTextBox_CustRmk.TabIndex = 3;
            this.radTextBox_CustRmk.Tag = "";
            this.radTextBox_CustRmk.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_CustRmk_KeyDown);
            // 
            // radTextBox_CustTel
            // 
            this.radTextBox_CustTel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_CustTel.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_CustTel.Location = new System.Drawing.Point(179, 160);
            this.radTextBox_CustTel.MaxLength = 10;
            this.radTextBox_CustTel.Name = "radTextBox_CustTel";
            this.radTextBox_CustTel.Size = new System.Drawing.Size(339, 25);
            this.radTextBox_CustTel.TabIndex = 2;
            this.radTextBox_CustTel.Tag = "";
            this.radTextBox_CustTel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_CustTel_KeyDown);
            this.radTextBox_CustTel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_CustTel_KeyPress);
            // 
            // radTextBox_CustName
            // 
            this.radTextBox_CustName.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_CustName.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_CustName.Location = new System.Drawing.Point(179, 110);
            this.radTextBox_CustName.Name = "radTextBox_CustName";
            this.radTextBox_CustName.Size = new System.Drawing.Size(339, 25);
            this.radTextBox_CustName.TabIndex = 1;
            this.radTextBox_CustName.Tag = "";
            this.radTextBox_CustName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_CustName_KeyDown);
            // 
            // radTextBox_CustID
            // 
            this.radTextBox_CustID.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_CustID.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_CustID.Location = new System.Drawing.Point(179, 64);
            this.radTextBox_CustID.Name = "radTextBox_CustID";
            this.radTextBox_CustID.Size = new System.Drawing.Size(175, 25);
            this.radTextBox_CustID.TabIndex = 0;
            this.radTextBox_CustID.Tag = "";
            this.radTextBox_CustID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_CustID_KeyDown);
            // 
            // radDateTimePicker_D1
            // 
            this.radDateTimePicker_D1.CustomFormat = "dd/MM/yyyy";
            this.radDateTimePicker_D1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radDateTimePicker_D1.ForeColor = System.Drawing.Color.Blue;
            this.radDateTimePicker_D1.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.radDateTimePicker_D1.Location = new System.Drawing.Point(179, 20);
            this.radDateTimePicker_D1.Name = "radDateTimePicker_D1";
            this.radDateTimePicker_D1.Size = new System.Drawing.Size(175, 25);
            this.radDateTimePicker_D1.TabIndex = 4;
            this.radDateTimePicker_D1.TabStop = false;
            this.radDateTimePicker_D1.Text = "22/05/2020";
            this.radDateTimePicker_D1.Value = new System.DateTime(2020, 5, 22, 0, 0, 0, 0);
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel4.Location = new System.Drawing.Point(11, 214);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(79, 23);
            this.radLabel4.TabIndex = 29;
            this.radLabel4.Text = "หมายเหตุ";
            // 
            // radLabel3
            // 
            this.radLabel3.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel3.Location = new System.Drawing.Point(9, 160);
            this.radLabel3.Name = "radLabel3";
            this.radLabel3.Size = new System.Drawing.Size(135, 23);
            this.radLabel3.TabIndex = 28;
            this.radLabel3.Text = "เบอร์โทร [บังคับ]";
            // 
            // radLabel2
            // 
            this.radLabel2.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel2.Location = new System.Drawing.Point(9, 112);
            this.radLabel2.Name = "radLabel2";
            this.radLabel2.Size = new System.Drawing.Size(135, 23);
            this.radLabel2.TabIndex = 27;
            this.radLabel2.Text = "ชื่อลูกค้า [บังคับ]";
            // 
            // radLabel1
            // 
            this.radLabel1.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel1.Location = new System.Drawing.Point(9, 64);
            this.radLabel1.Name = "radLabel1";
            this.radLabel1.Size = new System.Drawing.Size(145, 23);
            this.radLabel1.TabIndex = 26;
            this.radLabel1.Text = "รหัสลูกค้า [Enter]";
            // 
            // radLabel_Input
            // 
            this.radLabel_Input.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Input.Location = new System.Drawing.Point(9, 20);
            this.radLabel_Input.Name = "radLabel_Input";
            this.radLabel_Input.Size = new System.Drawing.Size(141, 23);
            this.radLabel_Input.TabIndex = 25;
            this.radLabel_Input.Text = "วันที่ต้องการสินค้า";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.radioButton_SPC);
            this.panel2.Controls.Add(this.radioButton_Bch);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(534, 54);
            this.panel2.TabIndex = 57;
            // 
            // radioButton_SPC
            // 
            this.radioButton_SPC.AutoSize = true;
            this.radioButton_SPC.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_SPC.ForeColor = System.Drawing.Color.Blue;
            this.radioButton_SPC.Location = new System.Drawing.Point(309, 6);
            this.radioButton_SPC.Name = "radioButton_SPC";
            this.radioButton_SPC.Size = new System.Drawing.Size(154, 23);
            this.radioButton_SPC.TabIndex = 1;
            this.radioButton_SPC.TabStop = true;
            this.radioButton_SPC.Text = "ลูกค้ารับสินค้าเลย";
            this.radioButton_SPC.UseVisualStyleBackColor = true;
            // 
            // radioButton_Bch
            // 
            this.radioButton_Bch.AutoSize = true;
            this.radioButton_Bch.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton_Bch.ForeColor = System.Drawing.Color.Blue;
            this.radioButton_Bch.Location = new System.Drawing.Point(53, 6);
            this.radioButton_Bch.Name = "radioButton_Bch";
            this.radioButton_Bch.Size = new System.Drawing.Size(154, 23);
            this.radioButton_Bch.TabIndex = 0;
            this.radioButton_Bch.TabStop = true;
            this.radioButton_Bch.Text = "ลูกค้ารับสินค้าเลย";
            this.radioButton_Bch.UseVisualStyleBackColor = true;
            // 
            // OrderCust_Cust
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightPink;
            this.CancelButton = this.radButton_Cancel;
            this.ClientSize = new System.Drawing.Size(540, 424);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "OrderCust_Cust";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ข้อมูลลูกค้า";
            this.Load += new System.EventHandler(this.OrderCust_Cust_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Choose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CustRmk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CustTel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CustName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CustID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radDateTimePicker_D1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Input)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected Telerik.WinControls.UI.RadButton radButton_Choose;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadLabel radLabel_Detail;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadLabel radLabel_Input;
        private Telerik.WinControls.UI.RadLabel radLabel1;
        private Telerik.WinControls.UI.RadLabel radLabel2;
        private Telerik.WinControls.UI.RadLabel radLabel3;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton radioButton_Bch;
        private System.Windows.Forms.RadioButton radioButton_SPC;
        private Telerik.WinControls.UI.RadDateTimePicker radDateTimePicker_D1;
        private Telerik.WinControls.UI.RadTextBox radTextBox_CustName;
        private Telerik.WinControls.UI.RadTextBox radTextBox_CustID;
        private Telerik.WinControls.UI.RadTextBox radTextBox_CustTel;
        private Telerik.WinControls.UI.RadTextBox radTextBox_CustRmk;
        private Telerik.WinControls.UI.RadLabel radLabel5;
    }
}
