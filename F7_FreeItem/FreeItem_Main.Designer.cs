﻿namespace Cashier.F7_FreeItem
{
    partial class FreeItem_Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Telerik.WinControls.UI.TableViewDefinition tableViewDefinition1 = new Telerik.WinControls.UI.TableViewDefinition();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FreeItem_Main));
            this.radGridView_Show = new Telerik.WinControls.UI.RadGridView();
            this.radLabel_Docno = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_CustName = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_Barcode = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel_Qty = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_X = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_Detail = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_Grand = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_txt = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_Head = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_ReciveName = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_ReciveID = new Telerik.WinControls.UI.RadLabel();
            this.PrintDocument_printBill = new System.Drawing.Printing.PrintDocument();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Docno)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CustName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Barcode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Qty)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_X)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Grand)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_txt)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Head)).BeginInit();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_ReciveName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_ReciveID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radGridView_Show
            // 
            this.radGridView_Show.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.radGridView_Show.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radGridView_Show.Location = new System.Drawing.Point(3, 103);
            // 
            // 
            // 
            this.radGridView_Show.MasterTemplate.AllowAddNewRow = false;
            this.radGridView_Show.MasterTemplate.ViewDefinition = tableViewDefinition1;
            this.radGridView_Show.Name = "radGridView_Show";
            this.radGridView_Show.ReadOnly = true;
            // 
            // 
            // 
            this.radGridView_Show.RootElement.HighlightColor = System.Drawing.Color.FromArgb(((int)(((byte)(198)))), ((int)(((byte)(146)))), ((int)(((byte)(225)))));
            this.radGridView_Show.Size = new System.Drawing.Size(786, 394);
            this.radGridView_Show.TabIndex = 41;
            this.radGridView_Show.ViewCellFormatting += new Telerik.WinControls.UI.CellFormattingEventHandler(this.RadGridView_Show_ViewCellFormatting);
            this.radGridView_Show.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadGridView_Show_KeyDown);
            // 
            // radLabel_Docno
            // 
            this.radLabel_Docno.AutoSize = false;
            this.radLabel_Docno.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_Docno.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radLabel_Docno.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Docno.Location = new System.Drawing.Point(574, 3);
            this.radLabel_Docno.Name = "radLabel_Docno";
            this.radLabel_Docno.Size = new System.Drawing.Size(209, 38);
            this.radLabel_Docno.TabIndex = 45;
            this.radLabel_Docno.Text = "MNRR210327000001";
            this.radLabel_Docno.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // radLabel_CustName
            // 
            this.radLabel_CustName.AutoSize = false;
            this.radLabel_CustName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_CustName.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radLabel_CustName.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_CustName.Location = new System.Drawing.Point(253, 3);
            this.radLabel_CustName.Name = "radLabel_CustName";
            this.radLabel_CustName.Size = new System.Drawing.Size(315, 38);
            this.radLabel_CustName.TabIndex = 44;
            this.radLabel_CustName.Text = "ไม่แสดงอะไร";
            // 
            // radTextBox_Barcode
            // 
            this.radTextBox_Barcode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radTextBox_Barcode.Font = new System.Drawing.Font("Tahoma", 14F);
            this.radTextBox_Barcode.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_Barcode.Location = new System.Drawing.Point(3, 3);
            this.radTextBox_Barcode.MaxLength = 50;
            this.radTextBox_Barcode.Name = "radTextBox_Barcode";
            this.radTextBox_Barcode.Size = new System.Drawing.Size(194, 33);
            this.radTextBox_Barcode.TabIndex = 0;
            this.radTextBox_Barcode.Text = "8850124003850";
            this.radTextBox_Barcode.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.radTextBox_Barcode.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_Barcode_KeyDown);
            // 
            // radLabel_Qty
            // 
            this.radLabel_Qty.AutoSize = false;
            this.radLabel_Qty.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_Qty.Font = new System.Drawing.Font("Tahoma", 13F);
            this.radLabel_Qty.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Qty.Location = new System.Drawing.Point(243, 3);
            this.radLabel_Qty.Name = "radLabel_Qty";
            this.radLabel_Qty.Size = new System.Drawing.Size(124, 33);
            this.radLabel_Qty.TabIndex = 47;
            this.radLabel_Qty.Text = "จำนวนที่แจก";
            // 
            // radLabel_X
            // 
            this.radLabel_X.AutoSize = false;
            this.radLabel_X.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_X.Font = new System.Drawing.Font("Tahoma", 13F);
            this.radLabel_X.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_X.Location = new System.Drawing.Point(203, 3);
            this.radLabel_X.Name = "radLabel_X";
            this.radLabel_X.Size = new System.Drawing.Size(34, 33);
            this.radLabel_X.TabIndex = 48;
            this.radLabel_X.Text = "X";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.radGridView_Show, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.radLabel_Detail, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(792, 570);
            this.tableLayoutPanel1.TabIndex = 49;
            // 
            // radLabel_Detail
            // 
            this.radLabel_Detail.AutoSize = false;
            this.radLabel_Detail.BackColor = System.Drawing.Color.Gainsboro;
            this.radLabel_Detail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_Detail.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_Detail.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Detail.Location = new System.Drawing.Point(3, 548);
            this.radLabel_Detail.Name = "radLabel_Detail";
            this.radLabel_Detail.Size = new System.Drawing.Size(786, 19);
            this.radLabel_Detail.TabIndex = 53;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 6;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 200F));
            this.tableLayoutPanel2.Controls.Add(this.radTextBox_Barcode, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.radLabel_X, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.radLabel_Qty, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.radLabel_Grand, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.radLabel_txt, 4, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 503);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(786, 39);
            this.tableLayoutPanel2.TabIndex = 54;
            // 
            // radLabel_Grand
            // 
            this.radLabel_Grand.AutoSize = false;
            this.radLabel_Grand.BackColor = System.Drawing.Color.Transparent;
            this.radLabel_Grand.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_Grand.Font = new System.Drawing.Font("Tahoma", 22F, System.Drawing.FontStyle.Bold);
            this.radLabel_Grand.ForeColor = System.Drawing.Color.Blue;
            this.radLabel_Grand.Location = new System.Drawing.Point(589, 3);
            this.radLabel_Grand.Name = "radLabel_Grand";
            this.radLabel_Grand.Size = new System.Drawing.Size(194, 33);
            this.radLabel_Grand.TabIndex = 55;
            this.radLabel_Grand.Text = "150,000.00";
            this.radLabel_Grand.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // radLabel_txt
            // 
            this.radLabel_txt.AutoSize = false;
            this.radLabel_txt.BackColor = System.Drawing.Color.Transparent;
            this.radLabel_txt.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_txt.Font = new System.Drawing.Font("Tahoma", 14F, System.Drawing.FontStyle.Bold);
            this.radLabel_txt.ForeColor = System.Drawing.Color.Black;
            this.radLabel_txt.Location = new System.Drawing.Point(389, 3);
            this.radLabel_txt.Name = "radLabel_txt";
            this.radLabel_txt.Size = new System.Drawing.Size(194, 33);
            this.radLabel_txt.TabIndex = 54;
            this.radLabel_txt.Text = "ยอดเงินประมาณ";
            this.radLabel_txt.TextAlignment = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 3;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Controls.Add(this.radLabel_Head, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.radLabel_Docno, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.radLabel_CustName, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(786, 44);
            this.tableLayoutPanel3.TabIndex = 55;
            // 
            // radLabel_Head
            // 
            this.radLabel_Head.AutoSize = false;
            this.radLabel_Head.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_Head.Font = new System.Drawing.Font("Tahoma", 17.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radLabel_Head.ForeColor = System.Drawing.Color.Red;
            this.radLabel_Head.Location = new System.Drawing.Point(3, 3);
            this.radLabel_Head.Name = "radLabel_Head";
            this.radLabel_Head.Size = new System.Drawing.Size(244, 38);
            this.radLabel_Head.TabIndex = 47;
            this.radLabel_Head.Text = "แจกของแถม";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 150F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel4.Controls.Add(this.radLabel_ReciveName, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.radLabel_ReciveID, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 53);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(786, 44);
            this.tableLayoutPanel4.TabIndex = 56;
            // 
            // radLabel_ReciveName
            // 
            this.radLabel_ReciveName.AutoSize = false;
            this.radLabel_ReciveName.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_ReciveName.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radLabel_ReciveName.Location = new System.Drawing.Point(153, 3);
            this.radLabel_ReciveName.Name = "radLabel_ReciveName";
            this.radLabel_ReciveName.Size = new System.Drawing.Size(530, 38);
            this.radLabel_ReciveName.TabIndex = 45;
            this.radLabel_ReciveName.Text = "ชื่อสาขา";
            // 
            // radLabel_ReciveID
            // 
            this.radLabel_ReciveID.AutoSize = false;
            this.radLabel_ReciveID.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_ReciveID.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radLabel_ReciveID.Location = new System.Drawing.Point(3, 3);
            this.radLabel_ReciveID.Name = "radLabel_ReciveID";
            this.radLabel_ReciveID.Size = new System.Drawing.Size(144, 38);
            this.radLabel_ReciveID.TabIndex = 44;
            this.radLabel_ReciveID.Text = "รหัส";
            // 
            // PrintDocument_printBill
            // 
            this.PrintDocument_printBill.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(this.PrintDocument_printBill_PrintPage);
            // 
            // FreeItem_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Gainsboro;
            this.ClientSize = new System.Drawing.Size(792, 570);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FreeItem_Main";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "แจกของแถมลูกค้า [MNRR]";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FreeItem_Main_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show.MasterTemplate)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radGridView_Show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Docno)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CustName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_Barcode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Qty)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_X)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Grand)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_txt)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Head)).EndInit();
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_ReciveName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_ReciveID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadGridView radGridView_Show;
        private Telerik.WinControls.UI.RadLabel radLabel_Docno;
        private Telerik.WinControls.UI.RadLabel radLabel_CustName;
        private Telerik.WinControls.UI.RadTextBox radTextBox_Barcode;
        private Telerik.WinControls.UI.RadLabel radLabel_Qty;
        private Telerik.WinControls.UI.RadLabel radLabel_X;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Telerik.WinControls.UI.RadLabel radLabel_Detail;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadLabel radLabel_txt;
        private Telerik.WinControls.UI.RadLabel radLabel_Grand;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private Telerik.WinControls.UI.RadLabel radLabel_ReciveID;
        private Telerik.WinControls.UI.RadLabel radLabel_ReciveName;
        private Telerik.WinControls.UI.RadLabel radLabel_Head;
        private System.Drawing.Printing.PrintDocument PrintDocument_printBill;
    }
}
