﻿using System;
using System.Data;
using Cashier.Class;
using System.Collections;

namespace Cashier.F7_FreeItem
{
    class FreeItem
    {
        // ค้นหาข้อมูลการรับ
        public static DataTable GetBarcodeDetail_ByBarcode(string barcode)
        {
            DataTable dtBarcode = ConnectionClass.SelectSQL_Main($@" SmartRO_Find_ItemRoByItembarcode '{barcode}' ");
            if (dtBarcode.Rows.Count > 0)
            {
                if ((dtBarcode.Rows[0]["STA24"].ToString() == "0"))
                {
                    ArrayList sqlInAX = new ArrayList { $@" 
                    INSERT INTO SHOP_RO_ITEM  (ITEMBARCODE,GROUPMAIN,GROUPSUB,ZONE,REMARK,
                            WHOIDINS,WHONAMEINS,STA ) 
                    VALUES  ('{dtBarcode.Rows[0]["ITEMBARCODE"]}','IS0538','GR01','','สำหรับของแถมลูกค้า  ส่งเสริมการขาย',
                            '{SystemClass.SystemUserID}','{SystemClass.SystemUserName}','1')" };

                    ConnectionClass.ExecuteSQL_ArrayMain(sqlInAX);
                    dtBarcode = ConnectionClass.SelectSQL_Main($@" SmartRO_Find_ItemRoByItembarcode '{barcode}' ");
                }
            }
            return dtBarcode;
        }

        //FindForPrint
        public static DataTable FindBillForPrint_Docno(string docno)
        {
            return ConnectionClass.SelectSQL_Main($@" 
            SELECT  SHOP_MNRR_HD.MNRRDocNo AS INVOICEID,SHOP_MNRR_HD.MNRRBranch AS POSGROUP,
		            SHOP_MNRR_DT.MNRRQtyUnitID AS UNITID , BRANCH_ID,BRANCH_NAME,
		            SHOP_MNRR_DT.MNRRQtyOrder AS QTY,SHOP_MNRR_DT.MNRRName as SPC_ITEMNAME,SHOP_MNRR_DT.MNRRBarcode AS ITEMBARCODE, 
		            SHOP_MNRR_HD.MNRRUserCode AS WHOINS ,MNRRNAMECH AS SPC_NAME,
		            CASE WHEN CONVERT(VARCHAR,SHOP_MNRR_HD.MNRRDate,23) = CONVERT(VARCHAR,GETDATE(),23)  THEN '1' ELSE '0' END AS STA_PRINT 
            FROM 	SHOP_MNRR_DT WITH (NOLOCK) 
		            INNER JOIN SHOP_MNRR_HD WITH (NOLOCK) ON SHOP_MNRR_DT.MNRRDocNo = SHOP_MNRR_HD.MNRRDocNo 
		            INNER JOIN SHOP_BRANCH WITH (NOLOCK) ON SHOP_MNRR_HD.MNRRBranch = SHOP_BRANCH.BRANCH_ID
            WHERE 	SHOP_MNRR_HD.MNRRDocNo = '{docno}'
		            AND SHOP_MNRR_HD.MNRRStaDoc = '1' 
		            AND SHOP_MNRR_DT.MNRRSta = '1' 
                    AND GROUPMAIN = 'IS0538'
            ORDER BY MNRRSeqNo
            ");
        }
        //Sql Update Bill Apv 24
        public static ArrayList SqlUpdateBill_Apv(string docno, string cstID, string cstName)
        {
            ArrayList sql24 = new ArrayList
                {
                    $@"
                        UPDATE  SHOP_MNRR_HD
                        SET     MNRRStaDoc='1',MNRRStaPrcDoc = '1',MNRRStaApvDoc = '1',
                                CUST_ID = '{cstID}',CUST_NAME = '{cstName}',
                                MNRRDateUp = convert(varchar,getdate(),25),MNRRTimeUp=convert(varchar,getdate(),24),MNRRWhoUp = '{SystemClass.SystemUserID}'
                        WHERE   MNRRDocNo = '{docno}' ",
                    $@"
                    UPDATE	Shop_MNRR_HD 
                    SET     Grand = (SELECT	SUM(MNRRPriceSum) AS GRAND	FROM	Shop_MNRR_DT WITH (NOLOCK) WHERE	MNRRDocNo = '{docno}' ) 
                    WHERE   MNRRDocNo = '{docno}' "
                };
            return sql24;
        }
        //Update Status Bill Void
        public static string UpdateStatus_Void(string docno, string linenum)
        {
            ArrayList sqlUp = new ArrayList
            {   $@"
                    UPDATE  SHOP_MNRR_DT 
                    SET     MNRRSTA = '0',
                            MNRRDateUp = convert(varchar,getdate(),25),MNRRTimeUp=convert(varchar,getdate(),24),MNRRWhoUp = '{SystemClass.SystemUserID}'
                    WHERE   MNRRDocNo = '{docno}' AND MNRRSeqNo = '{linenum}' "
            };
            return ConnectionClass.ExecuteSQL_ArrayMain(sqlUp);
        }
        //SaveData To DB
        public static string SaveData(string docno, double qty, int iRowDT, DataTable dtBarcode)
        {
            ArrayList sql = new ArrayList();
            if (iRowDT == 1)
            {
                sql.Add($@" INSERT INTO Shop_MNRR_HD (
                        MNRRDocNo,MNRRUserCode,MNRRBranch,
                        MNRRBranchTO,MNRRRemark,MNRRWhoIn,MNRRNAMECH,ProgramUse,MNRRStaDoc,GROUPMAIN)  
                    VALUES (
                        '{docno}','{SystemClass.SystemUserID}','{SystemClass.SystemPOSGROUP}',
                        '{SystemClass.SystemPOSGROUP}','ส่งเสริมการขาย','{SystemClass.SystemUserID}','{SystemClass.SystemUserName}','PC_POS','3','IS0538' )");

            }
            double Grand = qty * Double.Parse(dtBarcode.Rows[0]["SPC_PRICEGROUP3"].ToString());
            sql.Add($@"
                    INSERT  INTO  SHOP_MNRR_DT(
                        MNRRDocNo, MNRRSeqNo, MNRRItemID, MNRRItemDim,
                        MNRRBarcode, MNRRName, 
                        MNRRGroup, MNRRGroupSub,
                        MNRRPrice, MNRRQtyOrder, MNRRQtyUnitID, MNRRQtyFactor,
                        MNRRPriceSum, MNRRRemark, MNRRWhoIn, ProgramUse)
                    VALUES(
                        '{docno}', '{iRowDT}', '{dtBarcode.Rows[0]["ITEMID"]}', '{dtBarcode.Rows[0]["INVENTDIMID"] }',
                        '{dtBarcode.Rows[0]["ITEMBARCODE"]}', '{dtBarcode.Rows[0]["SPC_ITEMNAME"].ToString().Replace("{", "").Replace("}", "").Replace("'", "")}',
                        'IS0538','GR01',
                        '{dtBarcode.Rows[0]["SPC_PRICEGROUP3"]}', '{qty}', '{dtBarcode.Rows[0]["UNITID"]}', '{dtBarcode.Rows[0]["QTY"]}',
                        '{Grand}', '{SystemClass.SystemPcName}', '{SystemClass.SystemUserID}', 'PC_POS')
            ");


            return ConnectionClass.ExecuteSQL_ArrayMain(sql);
        }
    }
}
