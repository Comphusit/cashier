﻿using System;
using System.Data;
using System.Windows.Forms;
using Cashier.Class;

namespace Cashier.F7_FreeItem
{
    public partial class FreeItem_Cust : Telerik.WinControls.UI.RadForm
    {
        public string custID, custName, custTel, custAddress;
        //readonly string _custID, _custName, _custTel, _custAddress;

        string typeDesc;
        //Input Find Cust
        private void RadTextBox_CustID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_CustID.Text == "") return;
                DataTable dtCust = F3_BuyItem.BuyItem.GetDetailCust(radTextBox_CustID.Text.Trim());
                if (dtCust.Rows.Count == 0)
                {
                    MsgBoxClass.MassageBoxShowButtonOk_Warning($@"ไม่พบรหัส{typeDesc}ที่ระบุ ลองใหม่อีกครั้ง.", SystemClass.HeaderBuyCust);
                    radTextBox_CustID.SelectAll();
                    radTextBox_CustID.Focus();
                    return;
                }
                else
                {
                    radTextBox_CustID.Text = dtCust.Rows[0]["ACCOUNTNUM"].ToString();
                    radTextBox_CustName.Text = dtCust.Rows[0]["NAME"].ToString();
                    radTextBox_CustTel.Text = dtCust.Rows[0]["PHONE"].ToString();
                    radTextBox_CustRmk.Text = dtCust.Rows[0]["ADDRESS"].ToString();
                    radButton_Choose.Enabled = true;
                    radButton_Choose.Focus();
                    return;
                }
            }
        }
        //Set Focus
        private void RadTextBox_CustName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                radTextBox_CustTel.Focus();
            }
        }
        //Set Focus
        private void RadTextBox_CustTel_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                radTextBox_CustRmk.Focus();
            }
        }
        //Enter Address
        private void RadTextBox_CustRmk_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                CheckInput();
            }
        }

        //NumberOnly
        private void RadTextBox_CustTel_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        //Load
        public FreeItem_Cust()
        {
            InitializeComponent();

            radButton_Choose.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;

        }
        //Clear
        void ClearData()
        {
            radTextBox_CustID.Text = "";
            radTextBox_CustName.Text = "";
            radTextBox_CustTel.Text = "";
            radTextBox_CustRmk.Text = "";
            radButton_Choose.Enabled = false;
            radTextBox_CustID.Focus();
        }
        //load
        private void FreeItem_Cust_Load(object sender, EventArgs e)
        {
            ClearData();

            this.Text = "ข้อมูลลูกค้า";
            this.BackColor = ConfigClass.SetBackColor_F7();
            radLabel_Detail.BackColor = ConfigClass.SetBackColor_F7();
            radLabel_Detail.Text = "ระบุข้อมูลลูกค้า > กด ตกลง";
            radLabel_CstID.Text = "รหัสลูกค้า [Enter]"; radTextBox_CustID.Text = "";
            radLabel_CstsName.Text = "ชื่อลูกค้า"; radTextBox_CustName.Enabled = true;
            radLabel_F.Text = "*รหัส-เบอร์-บัตร ปชช-ชื่อค้นหา";
            typeDesc = "ลูกค้า";

        }
        //ปิด
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        //Choose
        private void RadButton_Choose_Click(object sender, EventArgs e)
        {
            CheckInput();
        }
        //CheckInput
        void CheckInput()
        {
            if (radTextBox_CustID.Text == "")
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning($@"ต้องระบุรหัส{typeDesc}ให้เรียบร้อย เท่านั้น.", SystemClass.HeaderBuyCust);
                radTextBox_CustID.Focus();
                return;
            }

            custID = radTextBox_CustID.Text;
            custName = radTextBox_CustName.Text;
            custTel = radTextBox_CustTel.Text;
            custAddress = radTextBox_CustRmk.Text;

            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

    }
}


