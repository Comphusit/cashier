﻿namespace Cashier
{
    partial class MainMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainMenu));
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.button_PrintDoc = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.BtSmartShop = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.BtOrderCust = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.BuyItem = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.ButFOOD = new System.Windows.Forms.Button();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_F2 = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_F2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.button_PrintDoc, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 0, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(201, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(390, 564);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // button_PrintDoc
            // 
            this.button_PrintDoc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button_PrintDoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_PrintDoc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_PrintDoc.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.button_PrintDoc.Image = ((System.Drawing.Image)(resources.GetObject("button_PrintDoc.Image")));
            this.button_PrintDoc.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button_PrintDoc.Location = new System.Drawing.Point(3, 451);
            this.button_PrintDoc.Name = "button_PrintDoc";
            this.button_PrintDoc.Size = new System.Drawing.Size(384, 110);
            this.button_PrintDoc.TabIndex = 4;
            this.button_PrintDoc.Text = "พิมพ์บิลซ้ำ [F5]";
            this.button_PrintDoc.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button_PrintDoc.UseVisualStyleBackColor = false;
            this.button_PrintDoc.Click += new System.EventHandler(this.Button_PrintDoc_Click);
            this.button_PrintDoc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Button_PrintDoc_KeyDown);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.BtSmartShop);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(384, 106);
            this.panel1.TabIndex = 0;
            // 
            // BtSmartShop
            // 
            this.BtSmartShop.BackColor = System.Drawing.Color.LightBlue;
            this.BtSmartShop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.BtSmartShop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BtSmartShop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtSmartShop.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.BtSmartShop.Image = ((System.Drawing.Image)(resources.GetObject("BtSmartShop.Image")));
            this.BtSmartShop.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtSmartShop.Location = new System.Drawing.Point(0, 0);
            this.BtSmartShop.Name = "BtSmartShop";
            this.BtSmartShop.Size = new System.Drawing.Size(384, 106);
            this.BtSmartShop.TabIndex = 0;
            this.BtSmartShop.Text = "ลูกค้าเครดิต [F1]";
            this.BtSmartShop.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.BtSmartShop.UseVisualStyleBackColor = false;
            this.BtSmartShop.Click += new System.EventHandler(this.BtSmartShop_Click);
            this.BtSmartShop.KeyDown += new System.Windows.Forms.KeyEventHandler(this.BtSmartShop_KeyDown);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.BtOrderCust);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 115);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(384, 106);
            this.panel2.TabIndex = 1;
            // 
            // BtOrderCust
            // 
            this.BtOrderCust.BackColor = System.Drawing.Color.LightPink;
            this.BtOrderCust.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("BtOrderCust.BackgroundImage")));
            this.BtOrderCust.CausesValidation = false;
            this.BtOrderCust.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BtOrderCust.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtOrderCust.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.BtOrderCust.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.BtOrderCust.Image = ((System.Drawing.Image)(resources.GetObject("BtOrderCust.Image")));
            this.BtOrderCust.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtOrderCust.Location = new System.Drawing.Point(0, 0);
            this.BtOrderCust.Name = "BtOrderCust";
            this.BtOrderCust.Size = new System.Drawing.Size(384, 106);
            this.BtOrderCust.TabIndex = 1;
            this.BtOrderCust.Text = "ออร์เดอร์ลูกค้า [F2]";
            this.BtOrderCust.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.BtOrderCust.UseVisualStyleBackColor = false;
            this.BtOrderCust.Click += new System.EventHandler(this.BtOrderCust_Click);
            this.BtOrderCust.KeyDown += new System.Windows.Forms.KeyEventHandler(this.BtOrderCust_KeyDown);
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.BuyItem);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 227);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(384, 106);
            this.panel3.TabIndex = 2;
            // 
            // BuyItem
            // 
            this.BuyItem.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.BuyItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BuyItem.CausesValidation = false;
            this.BuyItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BuyItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BuyItem.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.BuyItem.Image = ((System.Drawing.Image)(resources.GetObject("BuyItem.Image")));
            this.BuyItem.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BuyItem.Location = new System.Drawing.Point(0, 0);
            this.BuyItem.Name = "BuyItem";
            this.BuyItem.Size = new System.Drawing.Size(384, 106);
            this.BuyItem.TabIndex = 2;
            this.BuyItem.Text = "รับซื้อสินค้าลูกค้า [F3]";
            this.BuyItem.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.BuyItem.UseVisualStyleBackColor = false;
            this.BuyItem.Click += new System.EventHandler(this.BuyItem_Click);
            this.BuyItem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.BuyItem_KeyDown);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.ButFOOD);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(3, 339);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(384, 106);
            this.panel4.TabIndex = 3;
            // 
            // ButFOOD
            // 
            this.ButFOOD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ButFOOD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ButFOOD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButFOOD.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F);
            this.ButFOOD.Image = ((System.Drawing.Image)(resources.GetObject("ButFOOD.Image")));
            this.ButFOOD.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.ButFOOD.Location = new System.Drawing.Point(0, 0);
            this.ButFOOD.Name = "ButFOOD";
            this.ButFOOD.Size = new System.Drawing.Size(384, 106);
            this.ButFOOD.TabIndex = 3;
            this.ButFOOD.Text = "รับอาหารกล่อง [F4]";
            this.ButFOOD.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ButFOOD.UseVisualStyleBackColor = false;
            this.ButFOOD.Click += new System.EventHandler(this.ButFOOD_Click);
            this.ButFOOD.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ButFOOD_KeyDown);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Controls.Add(this.radLabel_F2, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel1, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(792, 570);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // radLabel_F2
            // 
            this.radLabel_F2.AutoSize = false;
            this.radLabel_F2.BackColor = System.Drawing.Color.Red;
            this.radLabel_F2.BorderVisible = true;
            this.radLabel_F2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radLabel_F2.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_F2.ForeColor = System.Drawing.Color.Black;
            this.radLabel_F2.Location = new System.Drawing.Point(597, 548);
            this.radLabel_F2.Name = "radLabel_F2";
            this.radLabel_F2.Size = new System.Drawing.Size(192, 19);
            this.radLabel_F2.TabIndex = 52;
            this.radLabel_F2.Text = "กด esc >>  ปิดโปรแกรม";
            this.radLabel_F2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 570);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainMenu";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.MainMenu_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.MainMenu_KeyDown);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_F2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        internal System.Windows.Forms.Button BtSmartShop;
        internal System.Windows.Forms.Button BtOrderCust;
        internal System.Windows.Forms.Button BuyItem;
        internal System.Windows.Forms.Button ButFOOD;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadLabel radLabel_F2;
        internal System.Windows.Forms.Button button_PrintDoc;
    }
}
