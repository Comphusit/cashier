﻿using System;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Cashier.Class;

namespace Cashier
{
    public partial class Main : Telerik.WinControls.UI.RadForm
    {
        private DateTime pTimeClose;
        public Main()
        {
            InitializeComponent();
            radLabel_Version.Text = "V " + SystemClass.SystemVersion;
        }

        private void Main_Load(object sender, EventArgs e)
        {
            SystemClass.SystemPcName = Environment.MachineName;
            if (
                //SystemClass.SystemPcName == "SPCN1465" ||
                SystemClass.SystemPcName == "SPCN1467")
            //SystemClass.SystemPcName == "SPCN1468"
            {
                SystemClass.SystemUserID = "M0604049";
                SystemClass.SystemUserName = "คุณ สุนิตย์สา หนูรักษ์";
                SystemClass.SystemPOSNUMBER = "000";
                SystemClass.SystemRecID = "1";
                SystemClass.SystemPOSGROUP = "MN080";
                SystemClass.SystemLOCATIONID = "เครื่อง 1";
                SystemClass.SystemZONEID = "MyJeeds";
                SystemClass.SystemPRICEGROUPID = "CASH";
                SystemClass.SystemPRICEFIELDNAME = "SPC_PriceGroup3";
                SystemClass.SystemPermissionFreeItem = "1";
            }
            else
            {
                ProcessAndLoginClass.CheckPOS();
                Process[] process = Process.GetProcessesByName("SPC.POS");
                if (process.Count() == 0)
                {
                    Process[] process1 = Process.GetProcessesByName("Retail POS");
                    if (process1.Count() == 0)
                    {
                        MsgBoxClass.MassageBoxShowButtonOk_Error("โปรแกรมไม่สามารถใช้งานได้เพราะโปรแกรมจะต้องใช้งานคู่กับโปรแกรมขายเท่านั้น.", SystemClass.HeaderMassage);
                        Application.Exit();
                        return;
                    }
                }

                DataTable dtEmp = ProcessAndLoginClass.DtGetMachine(SystemClass.SystemPcName);
                if (dtEmp.Rows.Count == 0)
                {
                    MsgBoxClass.MassageBoxShowButtonOk_Error("ไม่มีข้อมูลพนักงานขาย" + Environment.NewLine + "กรุณาเข้าระบบเครื่องขายก่อนใช้งาน", SystemClass.HeaderMassage);
                    Application.Exit();
                    return;
                }
                SystemClass.SystemUserID = dtEmp.Rows[0]["EMPLID"].ToString();
                SystemClass.SystemUserName = dtEmp.Rows[0]["SPC_NAME"].ToString();
                SystemClass.SystemPOSNUMBER = dtEmp.Rows[0]["POSNUMBER"].ToString();
                SystemClass.SystemPOSGROUP = dtEmp.Rows[0]["POSGROUP"].ToString();
                SystemClass.SystemLOCATIONID = dtEmp.Rows[0]["LOCATIONID"].ToString();
                SystemClass.SystemZONEID = dtEmp.Rows[0]["ZONEID"].ToString();
                SystemClass.SystemPRICEGROUPID = dtEmp.Rows[0]["PRICEGROUPID"].ToString();
                SystemClass.SystemPRICEFIELDNAME = dtEmp.Rows[0]["PRICEFIELDNAME"].ToString();
                SystemClass.SystemRecID = dtEmp.Rows[0]["RECID"].ToString();
                SystemClass.SystemPermissionFreeItem = dtEmp.Rows[0]["FreeItem"].ToString();
            }

            this.Text = SystemClass.SystemZONEID + " |  " + SystemClass.SystemUserID + " " + SystemClass.SystemUserName;

            if (SystemClass.SystemPermissionFreeItem == "1") Button_FreeItem.Enabled = true;
            else Button_FreeItem.Enabled = false;

            if (SystemClass.SystemPOSGROUP == "RT")
            {
                BtOrderCust.Enabled = false;
                BuyItem.Enabled = false;
                ButFOOD.Enabled = false;
                Button_Vender.Enabled = false;
                Button_FreeItem.Enabled = false;
            }

            pTimeClose = DateTime.Now.AddMinutes(10);
            Timer_Close.Start();
        }
        //Set F
        void SetF(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    //MsgBoxClass.MassageBoxShowButtonOk_Information("", SystemClass.HeaderSmartShop);
                    OpenPage("F1");
                    break;
                case Keys.F2:
                    //MsgBoxClass.MassageBoxShowButtonOk_Information("", SystemClass.HeaderOrder);
                    OpenPage("F2");
                    break;
                case Keys.F3:
                    //MsgBoxClass.MassageBoxShowButtonOk_Information("", SystemClass.HeaderBuyCust);
                    OpenPage("F3");
                    break;
                case Keys.F4:
                    //MsgBoxClass.MassageBoxShowButtonOk_Information("", SystemClass.HeaderMNPI);
                    OpenPage("F4");
                    break;
                case Keys.F5:
                    //MsgBoxClass.MassageBoxShowButtonOk_Information("", SystemClass.HeaderMNPI);
                    OpenPage("F5");
                    break;
                case Keys.F6:
                    OpenPage("F6");
                    break;
                case Keys.F7:
                    OpenPage("F7");
                    break;
                case Keys.Escape:
                    Application.Exit();
                    break;
                default:
                    break;
            }
        }
        //OpenForm
        void OpenPage(string pCase)
        {
            switch (pCase)
            {
                case "F1":
                    this.Hide();
                    F1_SmartShop.SmartShop_InputCardID _SmartShopInputCardID = new F1_SmartShop.SmartShop_InputCardID();
                    _SmartShopInputCardID.ShowDialog();
                    this.Close();
                    break;

                case "F2":
                    this.Hide();
                    F2_OrderCust.OrderCust_Main _OrderCust = new F2_OrderCust.OrderCust_Main();
                    F2_OrderCust.OrderCust.GetSystemPurchaseFresh();
                    _OrderCust.ShowDialog();
                    this.Close();
                    break;

                case "F3":
                    this.Hide();
                    F3_BuyItem.BuyItem_Main _BuyItem = new F3_BuyItem.BuyItem_Main("0");
                    _BuyItem.ShowDialog();
                    this.Close();
                    break;

                case "F4":
                    if (SystemClass.SystemPOSGROUP == "RT") return;
                    this.Hide();
                    F4_ReciveFood.ReciveFood_Main _ReciveMain = new F4_ReciveFood.ReciveFood_Main();
                    _ReciveMain.ShowDialog();
                    this.Close();
                    break;

                case "F5":
                    this.Hide();
                    GeneralForm.FormPrint _frmPrint = new GeneralForm.FormPrint();
                    _frmPrint.ShowDialog();
                    this.Close();
                    break;

                case "F6":
                    this.Hide();
                    F3_BuyItem.BuyItem_Main _Vender = new F3_BuyItem.BuyItem_Main("1");
                    _Vender.ShowDialog();
                    this.Close();
                    break;

                case "F7":
                    if (SystemClass.SystemPermissionFreeItem == "0")
                    {
                        MsgBoxClass.MassageBoxShowButtonOk_Error($@"ยังไม่เปิดเมนูนี้ให้ใช้งานสำหรับสาขา {Environment.NewLine}ต้องการใช้งานติดต่อ Centershop หรือ ComMinimart", SystemClass.HeaderFreeItem);
                        return;
                    }
                    else
                    {
                        this.Hide();
                        F7_FreeItem.FreeItem_Main _FreeItem = new F7_FreeItem.FreeItem_Main();
                        _FreeItem.ShowDialog();
                        this.Close();
                        break;
                    }

                default:
                    break;
            }
        }

        private void Main_KeyDown(object sender, KeyEventArgs e)
        {
            SetF(e);
        }

        private void BtSmartShop_KeyDown(object sender, KeyEventArgs e)
        {
            SetF(e);
        }

        private void BtOrderCust_KeyDown(object sender, KeyEventArgs e)
        {
            SetF(e);
        }


        private void BuyItem_KeyDown(object sender, KeyEventArgs e)
        {
            SetF(e);
        }

        private void ButFOOD_KeyDown(object sender, KeyEventArgs e)
        {
            SetF(e);
        }

        private void Button_PrintDoc_KeyDown(object sender, KeyEventArgs e)
        {
            SetF(e);
        }

        private void Button_Vender_KeyDown(object sender, KeyEventArgs e)
        {
            SetF(e);
        }

        private void BtSmartShop_Click(object sender, EventArgs e)
        {
            OpenPage("F1");
        }
        private void BtOrderCust_Click(object sender, EventArgs e)
        {
            OpenPage("F2");
        }
        private void BuyItem_Click(object sender, EventArgs e)
        {
            OpenPage("F3");
        }
        private void ButFOOD_Click(object sender, EventArgs e)
        {
            OpenPage("F4");
        }

        private void Button_PrintDoc_Click(object sender, EventArgs e)
        {
            OpenPage("F5");
        }

        private void Button_Vender_Click(object sender, EventArgs e)
        {
            OpenPage("F6");
        }

        private void Button_FreeItem_Click(object sender, EventArgs e)
        {
            OpenPage("F7");
        }
        //OpenDocument
        void OpenDocument(string pCase)
        {
            string sql = @" SELECT	FILENAME	FROM	SHOP_DOCUMENT WITH (NOLOCK)
            WHERE	FORMNAME = '" + pCase + @"' AND FORMTYPE = '00016' 
			AND OPENBY = 'SHOP' ";
            DataTable dt = ConnectionClass.SelectSQL_Main(sql);
            if (dt.Rows.Count == 0)
            {
                Class.MsgBoxClass.MassageBoxShowButtonOk_Error("ไม่พบคู่มือการใช้งานภายในระบบ" + Environment.NewLine + "ติดต่อ ComMinimart Tel.8570",
                    SystemClass.HeaderMassage);
                return;
            }
            else
            {
                string path = dt.Rows[0]["FILENAME"].ToString();
                if (File.Exists(path) == false)
                {
                    Class.MsgBoxClass.MassageBoxShowButtonOk_Error("ไม่พบคู่มือการใช้งานภายในระบบ" + Environment.NewLine + "ติดต่อ ComMinimart Tel.8570",
                        SystemClass.HeaderMassage);
                }
                else
                {
                    try
                    {
                        System.Diagnostics.Process.Start(path);
                    }
                    catch (Exception ex)
                    {
                        Class.MsgBoxClass.MassageBoxShowButtonOk_Error("ไม่สามารถเปิดเอกสารคู่มือได้ ลองใหม่อีกครั้ง" + Environment.NewLine +
                           ex.Message, SystemClass.HeaderMassage);
                    }
                }
            }
        }
        //Document F1
        private void PictureBox_F1_help_Click(object sender, EventArgs e)
        {
            OpenDocument("00351");
        }
        //Document F2
        private void PictureBox_F2_help_Click(object sender, EventArgs e)
        {
            OpenDocument("00352");
        }
        //Document F3
        private void PictureBox_F3_help_Click(object sender, EventArgs e)
        {
            OpenDocument("00353");
        }
        //Document F4
        private void PictureBox_F4_help_Click(object sender, EventArgs e)
        {
            OpenDocument("00354");
        }
        //Document F5
        private void PictureBox_F5_help_Click(object sender, EventArgs e)
        {
            OpenDocument("00355"); 
        }
        //Document F6
        private void PictureBox_F6_help_Click(object sender, EventArgs e)
        {
            OpenDocument("00365");
        }
        private void PictureBox_F7_help_Click(object sender, EventArgs e)
        {
            OpenDocument("00415");
        }

        private void Timer_Close_Tick(object sender, EventArgs e)
        {
            if (DateTime.Now > pTimeClose)
            {
                Timer_Close.Stop();
                //Class.MsgBoxClass.MassageBoxShowButtonOk_Error("โปรแกรมเปิดใช้งานนานเกินกว่าเวลาที่กำหนด " + Environment.NewLine + " ให้ปิดโปรแกรมแล้วเปิดใช้งานใหม่อีกครั้ง ", SystemClass.HeaderMassage);
                Application.Exit();
            }
        }

      
    }
}
