﻿using System;
using System.Data;
using System.Windows.Forms;
using Cashier.Class;

namespace Cashier.F3_BuyItem
{
    public partial class BuyItem_Cust : Telerik.WinControls.UI.RadForm
    {
        public string custID, custName, custTel, custAddress;
        readonly string _custID, _custName, _custTel, _custAddress;

        readonly string _pTypeOpen;//0 รับซื้อของลูกค้า 1 รับฝากของผู้จำหน่าย
        string typeDesc;
        //Input Find Cust
        private void RadTextBox_CustID_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_CustID.Text == "") return;
                DataTable dtCust = new DataTable();
                switch (_pTypeOpen)
                {
                    case "0":
                        dtCust = BuyItem.GetDetailCust(radTextBox_CustID.Text.Trim());
                        break;

                    case "1":
                        dtCust = BuyItem.GetDetailVender(radTextBox_CustID.Text.Trim());
                        break;

                    default:
                        break;
                }


                if (dtCust.Rows.Count == 0)
                {
                    MsgBoxClass.MassageBoxShowButtonOk_Warning($@"ไม่พบรหัส{typeDesc}ที่ระบุ ลองใหม่อีกครั้ง.", SystemClass.HeaderBuyCust);
                    radTextBox_CustID.SelectAll();
                    radTextBox_CustID.Focus();
                    return;
                }
                else
                {
                    radTextBox_CustID.Text = dtCust.Rows[0]["ACCOUNTNUM"].ToString();
                    radTextBox_CustName.Text = dtCust.Rows[0]["NAME"].ToString();
                    radTextBox_CustTel.Text = dtCust.Rows[0]["PHONE"].ToString();
                    radTextBox_CustRmk.Text = dtCust.Rows[0]["ADDRESS"].ToString();
                    if (radTextBox_CustTel.Text == "")
                    {
                        radTextBox_CustTel.Focus();
                    }
                    else
                    {
                        radButton_Choose.Focus();
                    }
                    return;
                }
            }
        }
        //Set Focus
        private void RadTextBox_CustName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                radTextBox_CustTel.Focus();
            }
        }
        //Set Focus
        private void RadTextBox_CustTel_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                radTextBox_CustRmk.Focus();
            }
        }
        //Enter Address
        private void RadTextBox_CustRmk_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                CheckInput();
            }
        }

        //NumberOnly
        private void RadTextBox_CustTel_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar);
        }
        //Load
        public BuyItem_Cust(string pTypeOpen, string pCustID, string pCustName, string pCustTel, string pCustAddress)
        {
            InitializeComponent();

            _custID = pCustID; _custName = pCustName; _custTel = pCustTel; _custAddress = pCustAddress;
            _pTypeOpen = pTypeOpen;

            radButton_Choose.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;

        }
        //Clear
        void ClearData()
        {
            radTextBox_CustID.Text = _custID;
            radTextBox_CustName.Text = _custName;
            radTextBox_CustTel.Text = _custTel;
            radTextBox_CustRmk.Text = _custAddress;

            radTextBox_CustID.Focus();
        }
        //load
        private void BuyItem_Cust_Load(object sender, EventArgs e)
        {

            ClearData();

            switch (_pTypeOpen)
            {
                case "0":
                    this.Text = "ข้อมูลลูกค้า";
                    this.BackColor = ConfigClass.SetBackColor_F3();
                    radLabel_Detail.BackColor = ConfigClass.SetBackColor_F3();
                    radLabel_Detail.Text = "ระบุข้อมูลลูกค้า > กด ตกลง";
                    radLabel_CstID.Text = "รหัสลูกค้า [Enter]"; radTextBox_CustID.Text = "C"; 
                    radLabel_CstsName.Text = "ชื่อลูกค้า [บังคับ]"; radTextBox_CustName.Enabled = true;
                    radLabel_F.Text = "*รหัส-เบอร์-บัตร ปชช-ชื่อค้นหา";
                    typeDesc = "ลูกค้า";
                    break;

                case "1"://ผู้จำหน่าย
                    this.Text = "ข้อมูลผู้จำหน่าย";
                    this.BackColor = ConfigClass.SetBackColor_F6();
                    radLabel_Detail.BackColor = ConfigClass.SetBackColor_F6();
                    radLabel_Detail.Text = "ระบุข้อมูลผู้จำหน่าย > กด ตกลง";
                    radLabel_CstID.Text = "รหัสผู้จำหน่าย [Enter]"; radTextBox_CustID.Text = "V";
                    radLabel_CstsName.Text = "ชื่อผู้จำหน่าย [บังคับ]"; radTextBox_CustName.Enabled = false;
                    radLabel_F.Text = "*รหัส-เบอร์-ชื่อค้นหา";
                    typeDesc = "ผู้จำหน่าย";
                    
                    break;
                default:
                    break;
            }

        }
        //ปิด
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }

        //Choose
        private void RadButton_Choose_Click(object sender, EventArgs e)
        {

            CheckInput();
        }
        //CheckInput
        void CheckInput()
        {

            if (radTextBox_CustName.Text == "")
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning($@"ต้องระบุการชื่อของ{typeDesc}ให้เรียบร้อย เท่านั้น.", SystemClass.HeaderBuyCust);
                radTextBox_CustName.Focus();
                return;
            }

            if (radTextBox_CustTel.Text == "")
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning($@"ต้องระบุการเบอร์โทรของ{typeDesc}ให้เรียบร้อย เท่านั้น.", SystemClass.HeaderBuyCust);
                radTextBox_CustTel.Focus();
                return;
            }

            custID = radTextBox_CustID.Text;
            custName = radTextBox_CustName.Text;
            custTel = radTextBox_CustTel.Text;
            custAddress = radTextBox_CustRmk.Text;

            this.DialogResult = DialogResult.Yes;
            this.Close();
        }

    }
}


