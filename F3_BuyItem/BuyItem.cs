﻿using Cashier.Class;
using System.Data;

namespace Cashier.F3_BuyItem
{
    class BuyItem
    {
        //OK FindBarcode Detail
        public static DataTable GetBarcodeDetail_Barcode(string Barcode)
        {
            return ConnectionClass.SelectSQL_Main(" ItembarcodeClass_GetItembarcode_ALLDeatil '" + Barcode + @"' ");
        }
        //ค้นหาลูกค้า
        public static DataTable GetDetailCust(string custID)
        {
            return ConnectionClass.SelectSQL_Main(" CashDesktop_BuyItem '0','" + custID + @"' ");
        }
        //ค้นหารอบส่งเงิน
        public static DataTable GetXXX_POSDRAWERCYCLETABLE(string pID)
        {
            return ConnectionClass.SelectSQL_Main(" CashDesktop_BuyItem '1','" + pID + @"' ");
        }
        //ค้นหารอบส่งเงินที่ใช้ไปแล้ว
        public static int CheckUseMoneyRound(string pID)
        {
            return ConnectionClass.SelectSQL_Main(" CashDesktop_BuyItem '2','" + pID + @"' ").Rows.Count;
        }
        //พิมบิลซ้ำ
        public static DataTable FindBillForPrint_Docno(string pDocno)
        {
            return ConnectionClass.SelectSQL_Main("  CashDesktop_BuyItem '3','" + pDocno + @"' ");
        }
        //ค้นหาผู้จำหน่าย
        public static DataTable GetDetailVender(string venderID)
        {
            return ConnectionClass.SelectSQL_Main(" CashDesktop_BuyItem '4','" + venderID + @"' ");
        }
    }
}


