﻿namespace Cashier.F3_BuyItem
{
    partial class BuyItem_Cust
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BuyItem_Cust));
            this.radButton_Choose = new Telerik.WinControls.UI.RadButton();
            this.radButton_Cancel = new Telerik.WinControls.UI.RadButton();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_Detail = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.radLabel_F = new Telerik.WinControls.UI.RadLabel();
            this.radTextBox_CustRmk = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_CustTel = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_CustName = new Telerik.WinControls.UI.RadTextBox();
            this.radTextBox_CustID = new Telerik.WinControls.UI.RadTextBox();
            this.radLabel4 = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_CstTel = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_CstsName = new Telerik.WinControls.UI.RadLabel();
            this.radLabel_CstID = new Telerik.WinControls.UI.RadLabel();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Choose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_F)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CustRmk)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CustTel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CustName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CustID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CstTel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CstsName)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CstID)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radButton_Choose
            // 
            this.radButton_Choose.Dock = System.Windows.Forms.DockStyle.Right;
            this.radButton_Choose.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Choose.Location = new System.Drawing.Point(144, 3);
            this.radButton_Choose.Name = "radButton_Choose";
            this.radButton_Choose.Size = new System.Drawing.Size(120, 38);
            this.radButton_Choose.TabIndex = 0;
            this.radButton_Choose.Text = "ตกลง";
            this.radButton_Choose.ThemeName = "Fluent";
            this.radButton_Choose.Click += new System.EventHandler(this.RadButton_Choose_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Choose.GetChildAt(0))).Text = "ตกลง";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Choose.GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(35)))), ((int)(((byte)(25)))), ((int)(((byte)(214)))));
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Choose.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Choose.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Choose.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Choose.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Choose.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(173)))), ((int)(((byte)(244)))), ((int)(((byte)(215)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Choose.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // radButton_Cancel
            // 
            this.radButton_Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.radButton_Cancel.Dock = System.Windows.Forms.DockStyle.Left;
            this.radButton_Cancel.Font = new System.Drawing.Font("Tahoma", 20.25F);
            this.radButton_Cancel.Location = new System.Drawing.Point(270, 3);
            this.radButton_Cancel.Name = "radButton_Cancel";
            this.radButton_Cancel.Size = new System.Drawing.Size(120, 38);
            this.radButton_Cancel.TabIndex = 1;
            this.radButton_Cancel.Text = "ยกเลิก";
            this.radButton_Cancel.ThemeName = "Fluent";
            this.radButton_Cancel.Click += new System.EventHandler(this.RadButton_Cancel_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Text = "ยกเลิก";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton_Cancel.GetChildAt(0))).Font = new System.Drawing.Font("Tahoma", 14.25F);
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor4 = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(203)))), ((int)(((byte)(226)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton_Cancel.GetChildAt(0).GetChildAt(0))).SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.None;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.radLabel_Detail, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(540, 311);
            this.tableLayoutPanel1.TabIndex = 45;
            // 
            // radLabel_Detail
            // 
            this.radLabel_Detail.AutoSize = false;
            this.radLabel_Detail.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.radLabel_Detail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radLabel_Detail.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radLabel_Detail.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Detail.Location = new System.Drawing.Point(3, 289);
            this.radLabel_Detail.Name = "radLabel_Detail";
            this.radLabel_Detail.Size = new System.Drawing.Size(534, 19);
            this.radLabel_Detail.TabIndex = 54;
            this.radLabel_Detail.Text = "<html>เลือก รหัส/บริษัท &gt;&gt; กด เลือก | สีแดง &gt;&gt; ลูกค้าไม่สามารถทำบิลได" +
    "้ [กด เลือก เพื่อแสดงข้อความ]</html>";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.radButton_Cancel, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.radButton_Choose, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 239);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(534, 44);
            this.tableLayoutPanel2.TabIndex = 45;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.radLabel_F);
            this.panel1.Controls.Add(this.radTextBox_CustRmk);
            this.panel1.Controls.Add(this.radTextBox_CustTel);
            this.panel1.Controls.Add(this.radTextBox_CustName);
            this.panel1.Controls.Add(this.radTextBox_CustID);
            this.panel1.Controls.Add(this.radLabel4);
            this.panel1.Controls.Add(this.radLabel_CstTel);
            this.panel1.Controls.Add(this.radLabel_CstsName);
            this.panel1.Controls.Add(this.radLabel_CstID);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Font = new System.Drawing.Font("Tahoma", 12F);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(534, 230);
            this.panel1.TabIndex = 0;
            // 
            // radLabel_F
            // 
            this.radLabel_F.Font = new System.Drawing.Font("Tahoma", 8F);
            this.radLabel_F.ForeColor = System.Drawing.Color.Green;
            this.radLabel_F.Location = new System.Drawing.Point(360, 23);
            this.radLabel_F.Name = "radLabel_F";
            this.radLabel_F.Size = new System.Drawing.Size(147, 16);
            this.radLabel_F.TabIndex = 30;
            this.radLabel_F.Text = "*รหัส-เบอร์-บัตร ปชช-ชื่อค้นหา";
            // 
            // radTextBox_CustRmk
            // 
            this.radTextBox_CustRmk.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_CustRmk.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_CustRmk.Location = new System.Drawing.Point(179, 165);
            this.radTextBox_CustRmk.Multiline = true;
            this.radTextBox_CustRmk.Name = "radTextBox_CustRmk";
            // 
            // 
            // 
            this.radTextBox_CustRmk.RootElement.StretchVertically = true;
            this.radTextBox_CustRmk.Size = new System.Drawing.Size(339, 47);
            this.radTextBox_CustRmk.TabIndex = 3;
            this.radTextBox_CustRmk.Tag = "";
            this.radTextBox_CustRmk.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_CustRmk_KeyDown);
            // 
            // radTextBox_CustTel
            // 
            this.radTextBox_CustTel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_CustTel.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_CustTel.Location = new System.Drawing.Point(179, 111);
            this.radTextBox_CustTel.MaxLength = 10;
            this.radTextBox_CustTel.Name = "radTextBox_CustTel";
            this.radTextBox_CustTel.Size = new System.Drawing.Size(339, 25);
            this.radTextBox_CustTel.TabIndex = 2;
            this.radTextBox_CustTel.Tag = "";
            this.radTextBox_CustTel.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_CustTel_KeyDown);
            this.radTextBox_CustTel.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.RadTextBox_CustTel_KeyPress);
            // 
            // radTextBox_CustName
            // 
            this.radTextBox_CustName.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_CustName.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_CustName.Location = new System.Drawing.Point(179, 61);
            this.radTextBox_CustName.Name = "radTextBox_CustName";
            this.radTextBox_CustName.Size = new System.Drawing.Size(339, 25);
            this.radTextBox_CustName.TabIndex = 1;
            this.radTextBox_CustName.Tag = "";
            this.radTextBox_CustName.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_CustName_KeyDown);
            // 
            // radTextBox_CustID
            // 
            this.radTextBox_CustID.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold);
            this.radTextBox_CustID.ForeColor = System.Drawing.Color.Blue;
            this.radTextBox_CustID.Location = new System.Drawing.Point(179, 15);
            this.radTextBox_CustID.Name = "radTextBox_CustID";
            this.radTextBox_CustID.Size = new System.Drawing.Size(175, 25);
            this.radTextBox_CustID.TabIndex = 0;
            this.radTextBox_CustID.Tag = "";
            this.radTextBox_CustID.KeyDown += new System.Windows.Forms.KeyEventHandler(this.RadTextBox_CustID_KeyDown);
            // 
            // radLabel4
            // 
            this.radLabel4.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel4.Location = new System.Drawing.Point(6, 165);
            this.radLabel4.Name = "radLabel4";
            this.radLabel4.Size = new System.Drawing.Size(41, 23);
            this.radLabel4.TabIndex = 29;
            this.radLabel4.Text = "ที่อยู่";
            // 
            // radLabel_CstTel
            // 
            this.radLabel_CstTel.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_CstTel.Location = new System.Drawing.Point(4, 111);
            this.radLabel_CstTel.Name = "radLabel_CstTel";
            this.radLabel_CstTel.Size = new System.Drawing.Size(135, 23);
            this.radLabel_CstTel.TabIndex = 28;
            this.radLabel_CstTel.Text = "เบอร์โทร [บังคับ]";
            // 
            // radLabel_CstsName
            // 
            this.radLabel_CstsName.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_CstsName.Location = new System.Drawing.Point(4, 63);
            this.radLabel_CstsName.Name = "radLabel_CstsName";
            this.radLabel_CstsName.Size = new System.Drawing.Size(135, 23);
            this.radLabel_CstsName.TabIndex = 27;
            this.radLabel_CstsName.Text = "ชื่อลูกค้า [บังคับ]";
            // 
            // radLabel_CstID
            // 
            this.radLabel_CstID.Font = new System.Drawing.Font("Tahoma", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(222)));
            this.radLabel_CstID.Location = new System.Drawing.Point(4, 15);
            this.radLabel_CstID.Name = "radLabel_CstID";
            this.radLabel_CstID.Size = new System.Drawing.Size(145, 23);
            this.radLabel_CstID.TabIndex = 26;
            this.radLabel_CstID.Text = "รหัสลูกค้า [Enter]";
            // 
            // BuyItem_Cust
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.CancelButton = this.radButton_Cancel;
            this.ClientSize = new System.Drawing.Size(540, 311);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BuyItem_Cust";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ข้อมูลลูกค้า";
            this.Load += new System.EventHandler(this.BuyItem_Cust_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Choose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton_Cancel)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Detail)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_F)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CustRmk)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CustTel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CustName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radTextBox_CustID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CstTel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CstsName)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_CstID)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        protected Telerik.WinControls.UI.RadButton radButton_Choose;
        protected Telerik.WinControls.UI.RadButton radButton_Cancel;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadLabel radLabel_Detail;
        private System.Windows.Forms.Panel panel1;
        private Telerik.WinControls.UI.RadLabel radLabel_CstID;
        private Telerik.WinControls.UI.RadLabel radLabel_CstsName;
        private Telerik.WinControls.UI.RadLabel radLabel_CstTel;
        private Telerik.WinControls.UI.RadLabel radLabel4;
        private Telerik.WinControls.UI.RadTextBox radTextBox_CustName;
        private Telerik.WinControls.UI.RadTextBox radTextBox_CustID;
        private Telerik.WinControls.UI.RadTextBox radTextBox_CustTel;
        private Telerik.WinControls.UI.RadTextBox radTextBox_CustRmk;
        private Telerik.WinControls.UI.RadLabel radLabel_F;
    }
}
