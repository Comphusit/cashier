﻿using System;
using System.Windows.Forms;
using Cashier.Class;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Collections;

namespace Cashier.F3_BuyItem
{
    public partial class BuyItem_Main : Telerik.WinControls.UI.RadForm
    {
        private readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();
        readonly PrintController printController = new StandardPrintController();

        readonly DataTable dtGrid = new DataTable();

        string custID, custName, custTel, custAddress;
        string billMaxMNBC = "", xxxPos = "";

        readonly string _pTypeOpen;//0 รับซื้อของลูกค้า 1 รับฝากของผู้จำหน่าย
        string typeDesc;
        //  double grand;
        public BuyItem_Main(string pTypeOpen)
        {
            InitializeComponent();
            _pTypeOpen = pTypeOpen;
        }
        //Clear
        void ClearData()
        {
            if (dtGrid.Rows.Count > 0) dtGrid.Rows.Clear();
            custID = ""; custName = ""; custTel = ""; custAddress = "";
            billMaxMNBC = ""; xxxPos = "";
            radLabel_txt.Visible = true; radLabel_Grand.Visible = true;//grand = 0;
            radLabel_CustName.Text = "";
            radLabel_Docno.Text = "";
            radLabel_ReciveID.Text = "";
            radLabel_ReciveName.Text = "";
            radTextBox_Barcode.Text = "";
            radLabel_X.Visible = false;
            radLabel_Qty.Visible = false; radLabel_Qty.Text = "1.00";
            radLabel_Grand.Text = "0.00";
            radTextBox_Barcode.Text = "";
            radTextBox_Barcode.Focus();
        }
        //load
        private void BuyItem_Main_Load(object sender, EventArgs e)
        {
            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;
            barcode.BarHeight = 45;
            barcode.LeftMargin = 30;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;

            ClearData();

            switch (_pTypeOpen)
            {
                case "0":
                    radLabel_Detail.Text = "Ctrl+F3 > ระบุลูกค้า | F3 > void สินค้า | F2 > ยกเลิกบิล | PdDn > Multiply | Home : รวม";
                    radLabel_Detail.BackColor = ConfigClass.SetBackColor_F3();
                    this.BackColor = ConfigClass.SetBackColor_F3();
                    this.Text = "รับซื้อสินค้า [MNBC]";
                    radLabel_Head.Text = "รับซื้อสินค้า";
                    typeDesc = "ลูกค้า";
                    break;
                case "1":
                    radLabel_Detail.Text = "Ctrl+F3 > ระบุผู้จำหน่าย | F3 > void สินค้า | F2 > ยกเลิกบิล | PdDn > Multiply | Home : รวม";
                    radLabel_Detail.BackColor = ConfigClass.SetBackColor_F6();
                    this.BackColor = ConfigClass.SetBackColor_F6();
                    radLabel_Head.Text = "รับฝากสินค้า";
                    this.Text = "รับฝากสินค้าจากผู้จำหน่าย [MNBC]";
                    SystemClass.HeaderBuyCust = "รับฝากสินค้าผู้จำหน่าย";
                    typeDesc = "ผู้จำหน่าย";
                    break;
                default:
                    break;
            }


            radLabel_ReciveID.Text = SystemClass.SystemPOSGROUP;
            radLabel_ReciveName.Text = SystemClass.SystemZONEID;

            DatagridClass.SetDefaultRadGridView(radGridView_Show);

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 150));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 350));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("QtyOrder", "จำนวน", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("Price", "ราคา", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("Grand", "ยอดเงินสุทธิ", 120));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 100));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMID", "รหัสสินค้า"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("INVENTDIMID", "มิติสินค้า"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("DIMENSION", "จัดซื้อ"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("DESCRIPTION", "DESCRIPTION"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddVisible("QTY", "อัตราส่วน"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("SPC_SalesPriceType", "SPC_SalesPriceType"));

            radGridView_Show.MasterTemplate.EnableFiltering = false;
            radGridView_Show.TableElement.RowHeight = 60;

            dtGrid.Columns.Add("ITEMBARCODE");
            dtGrid.Columns.Add("SPC_ITEMNAME");
            dtGrid.Columns.Add("QtyOrder");
            dtGrid.Columns.Add("Price");
            dtGrid.Columns.Add("Grand");
            dtGrid.Columns.Add("UNITID");
            dtGrid.Columns.Add("ITEMID");
            dtGrid.Columns.Add("INVENTDIMID");
            dtGrid.Columns.Add("DIMENSION");
            dtGrid.Columns.Add("DESCRIPTION");
            dtGrid.Columns.Add("QTY");
            dtGrid.Columns.Add("SPC_SalesPriceType");

            radTextBox_Barcode.Focus();
        }
        //Format Cell
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            e.CellElement.Font = SystemClass.SetFont12;
            try
            {
                if (e.CellElement is GridRowHeaderCellElement && e.Row is GridViewDataRowInfo)
                {
                    e.CellElement.Text = (e.CellElement.RowIndex + 1).ToString();
                    e.CellElement.TextImageRelation = TextImageRelation.ImageBeforeText;
                }
                else
                {
                    e.CellElement.ResetValue(LightVisualElement.TextImageRelationProperty, ValueResetFlags.Local);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        //F2 remove
        private void RadGridView_Show_KeyDown(object sender, KeyEventArgs e)
        {
            if (dtGrid.Rows.Count == 0) return;

            switch (e.KeyCode)
            {
                case Keys.F3: F3(radGridView_Show.CurrentRow.Index); break;
                case Keys.Escape: radTextBox_Barcode.Focus(); break;
                default:
                    break;
            }
        }
        //Enter Barcode
        private void RadTextBox_Barcode_KeyDown(object sender, KeyEventArgs e)
        {
            if ((e.Control) && (e.KeyCode == Keys.F3))
            {
                CustInput();
                return;
            }

            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (radTextBox_Barcode.Text == "")
                    {
                        MsgBoxClass.MassageBoxShowButtonOk_Warning("ระบุบาร์โค้ดสินค้าก่อน Enter.", SystemClass.HeaderBuyCust);
                        radTextBox_Barcode.Focus();
                        return;
                    }
                    EnterTextbox(radTextBox_Barcode.Text);
                    break;

                case Keys.PageDown:
                    if (radTextBox_Barcode.Text == "")
                    {
                        return;
                    }
                    double qty;
                    try
                    {
                        qty = Convert.ToDouble(radTextBox_Barcode.Text);
                    }
                    catch (Exception)
                    {
                        //qty = 0;
                        MsgBoxClass.MassageBoxShowButtonOk_Warning("จำนวนที่ระบุต้องเป็นตัวเลขเท่านั้น.", SystemClass.HeaderBuyCust);
                        radTextBox_Barcode.SelectAll();
                        radTextBox_Barcode.Focus();
                        return;
                    }
                    if (qty > 10000)
                    {
                        MsgBoxClass.MassageBoxShowButtonOk_Warning("จำนวนสินค้าที่ระบุมากผิดปกติ เช็คใหม่อีกครั้ง", SystemClass.HeaderBuyCust);
                        radTextBox_Barcode.SelectAll();
                        radTextBox_Barcode.Focus();
                        return;
                    }

                    radLabel_X.Visible = true;
                    radLabel_Qty.Text = qty.ToString("N2");
                    radLabel_Qty.Visible = true;
                    radTextBox_Barcode.SelectAll();
                    radTextBox_Barcode.Focus();
                    break;

                case Keys.F2:
                    F2();
                    break;

                case Keys.F3:
                    if (dtGrid.Rows.Count == 0) return;
                    F3(dtGrid.Rows.Count - 1);
                    break;

                case Keys.Home:
                    Home();
                    break;

                case Keys.Escape:
                    if (radGridView_Show.Rows.Count == 0) Application.Exit();
                    break;
                default:
                    return;
            }
        }
        //Enter Barcode
        void EnterTextbox(string barcode)
        {
            //ค้นหารายละเอียดสินค้า
            DataTable dtBarcode = BuyItem.GetBarcodeDetail_Barcode(barcode);
            if (dtBarcode.Rows.Count == 0)
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning("ไม่พบข้อมูลสินค้าของบาร์โค้ดที่ระบุ" + Environment.NewLine + "ลองใหม่อีกครั้ง.", SystemClass.HeaderBuyCust);
                radTextBox_Barcode.SelectAll(); radTextBox_Barcode.Focus();
                return;
            }

            int iRows = CheckBarcodeInGrid(dtBarcode.Rows[0]["ITEMBARCODE"].ToString());
            if (iRows == 999)
            {

                BuyItem_Item _item = new BuyItem_Item(_pTypeOpen, dtBarcode.Rows[0]["ITEMBARCODE"].ToString(),
                   dtBarcode.Rows[0]["SPC_ITEMNAME"].ToString(),
                    Convert.ToDouble(radLabel_Qty.Text).ToString("N2"), dtBarcode.Rows[0]["UNITID"].ToString());
                if (_item.ShowDialog(this) == DialogResult.Yes)
                {
                    double pPrice = _item.sPriceUnit;
                    double pGrand = CheckDigit(pPrice * Convert.ToDouble(radLabel_Qty.Text));

                    dtGrid.Rows.Add(dtBarcode.Rows[0]["ITEMBARCODE"].ToString(),
                        dtBarcode.Rows[0]["SPC_ITEMNAME"].ToString(), Convert.ToDouble(radLabel_Qty.Text).ToString("N2"),
                        Convert.ToDouble(pPrice).ToString("N"),
                        Convert.ToDouble(pGrand).ToString("N"),
                        dtBarcode.Rows[0]["UNITID"].ToString(), dtBarcode.Rows[0]["ITEMID"].ToString(),
                        dtBarcode.Rows[0]["INVENTDIMID"].ToString(), dtBarcode.Rows[0]["DIMENSION"].ToString(), dtBarcode.Rows[0]["DESCRIPTION"].ToString(),
                        dtBarcode.Rows[0]["QTY"].ToString(), dtBarcode.Rows[0]["SPC_SalesPriceType"].ToString());

                    radGridView_Show.DataSource = dtGrid;
                    dtGrid.AcceptChanges();
                    radLabel_Grand.Text = (Convert.ToDouble(radLabel_Grand.Text) + pGrand).ToString("N2");
                }
                else
                {
                    radTextBox_Barcode.SelectAll();
                    radTextBox_Barcode.Focus();
                    return;
                }

            }
            else
            {
                double qty_up = Convert.ToDouble(radGridView_Show.Rows[iRows].Cells["QtyOrder"].Value.ToString());
                qty_up += Convert.ToDouble(radLabel_Qty.Text);
                double grand = Convert.ToDouble(radGridView_Show.Rows[iRows].Cells["Price"].Value.ToString()) * qty_up;

                radLabel_Grand.Text = (Convert.ToDouble(radLabel_Grand.Text) - Convert.ToDouble(radGridView_Show.Rows[iRows].Cells["Grand"].Value.ToString()) + grand).ToString("N2");

                radGridView_Show.Rows[iRows].Cells["QtyOrder"].Value = qty_up.ToString("N2");
                radGridView_Show.Rows[iRows].Cells["Grand"].Value = grand.ToString("N2");
                dtGrid.AcceptChanges();

            }

            radLabel_X.Visible = false;
            radLabel_Qty.Text = "1";
            radLabel_Qty.Visible = false;
            radTextBox_Barcode.Text = "";
            radTextBox_Barcode.Focus();
            radGridView_Show.Rows[dtGrid.Rows.Count - 1].IsCurrent = true;

        }
        //CheckBarcode IN Grid
        int CheckBarcodeInGrid(string barcode)
        {
            int iCheck = 999;
            for (int i = 0; i < radGridView_Show.Rows.Count; i++)
            {
                if (radGridView_Show.Rows[i].Cells["ITEMBARCODE"].Value.ToString() == barcode)
                {
                    iCheck = i;
                    break;
                }
            }
            return iCheck;
        }
        //Check ทศนิยม
        double CheckDigit(double grand)
        {
            if (grand.ToString("N2").Contains("."))
            {
                string[] b = grand.ToString("N2").Split('.');
                double bath = Convert.ToDouble(b[0]);
                int strang = Convert.ToInt32(b[1]);
                double st = 0;
                switch (strang)
                {
                    case int n when (n >= 0 && n < 25):
                        st = 0;
                        break;
                    case int n when (n >= 25 && n < 50):
                        st = 0.25;
                        break;
                    case int n when (n >= 50 && n < 75):
                        st = 0.50;
                        break;
                    case int n when (n >= 75):
                        st = 1;
                        break;
                    default:
                        break;
                }
                return bath + st;
            }
            else
            {
                return grand;
            }
        }
        // Sava Data
        void SaveDataSendAX(string pXXX)
        {
            xxxPos = pXXX;
            int iRows = 1;
            billMaxMNBC = Class.ConfigClass.GetMaxINVOICEID("MNBC", "-", "MNBC", "1");

            string rmk = "",typeOrder = "";
            switch (_pTypeOpen)
            {
                case "0"://OrderCust
                    rmk = "OrderCustByCash";
                    typeOrder = "20";
                    break;
                case "1"://Deposit
                    rmk = "DepositByCash";
                    typeOrder = "30";
                    pXXX = "รับฝากสินค้าผู้จำหน่าย [เครดิต]";
                    break;
                default:
                    break;
            }
            ArrayList sql24 = new ArrayList();
            for (int i = 0; i < radGridView_Show.Rows.Count; i++)
            {
                sql24.Add(String.Format(@"
                    INSERT INTO SHOP_MNBC_DT 
                        (MNBCDocNo, MNBCSeqNo, MNBCItemID, MNBCDimid, 
                       MNBCBarcode, MNBCName, MNBCQty, MNBCPrice,MNBCNet,MNBCUnitID, MNBCFactor, 
                        MNBCRemark, MNBCWhoIn ) 
                    VALUES ('" + billMaxMNBC + @"','" + (iRows) + @"',
                       '" + radGridView_Show.Rows[i].Cells["ITEMID"].Value.ToString() + @"',
                       '" + radGridView_Show.Rows[i].Cells["INVENTDIMID"].Value.ToString() + @"',
                       '" + radGridView_Show.Rows[i].Cells["ITEMBARCODE"].Value.ToString() + @"',
                       '" + radGridView_Show.Rows[i].Cells["SPC_ITEMNAME"].Value.ToString().Replace("'", "").Replace("{", "").Replace("}", "") + @"',
                       '" + Convert.ToDouble(radGridView_Show.Rows[i].Cells["QtyOrder"].Value.ToString()) + @"',
                       '" + Convert.ToDouble(radGridView_Show.Rows[i].Cells["Price"].Value.ToString()) + @"',
                       '" + Convert.ToDouble(radGridView_Show.Rows[i].Cells["Grand"].Value.ToString()) + @"',
                       '" + radGridView_Show.Rows[i].Cells["UNITID"].Value.ToString() + @"',
                       '" + Convert.ToDouble(radGridView_Show.Rows[i].Cells["QTY"].Value.ToString()) + @"',
                       '" + rmk + @"',
                       '" + SystemClass.SystemUserID + @"' ) "));
                iRows++;
            }

            sql24.Add(String.Format(@"
                    INSERT INTO SHOP_MNBC_HD
                        (   MNBCDocNo,MNBCUserCode,MNBCBranch, MNBCTypeOrder,MNBCDptCode,
                            MNBCCstID, MNBCCstName, MNBCCstTel, MNBCCstAddress ,
                            MNBCMoneyRound ,MNBCPriceNet,MNBCComName,
                            MNBCRemark, MNBCWhoIn,MNBCUserName ) 
                    VALUES ('" + billMaxMNBC + @"','" + SystemClass.SystemUserID + @"','" + SystemClass.SystemPOSGROUP + @"','" + typeOrder + @"',
                            '" + SystemClass.SystemPOSGROUP + @"',
                            '" + custID + @"','" + custName + @"','" + custTel + @"','" + custAddress + @"',
                            '" + pXXX + @"','" + Convert.ToDouble(radLabel_Grand.Text) + @"','" + SystemClass.SystemPcName + @"',
                            '" + rmk + @"','" + SystemClass.SystemUserID + @"','" + SystemClass.SystemUserName + @"' ) "));

            string result = ConnectionClass.ExecuteSQL_ArrayMain(sql24);
            if (result == "")
            {
                PrintDocBill();
                Application.Exit();
            }
            else
            {
                MsgBoxClass.MassageBoxShowButtonOk_Error("ไม่สามารถบันทึกข้อมูลได้ ลองใหม่อีกครั้ง" + Environment.NewLine + result, SystemClass.HeaderBuyCust);
                radTextBox_Barcode.SelectAll();
                radTextBox_Barcode.Focus();
                return;
            }
        }
        //F2
        void F2()
        {
            if (dtGrid.Rows.Count == 0) ClearData();

            if (MsgBoxClass.MassageBoxShowYesNo_DialogResult("ยืนยันการยกเลิกบิลที่ดำเนินการอยู่ ?.", SystemClass.HeaderBuyCust) == DialogResult.No) return;

            Application.Exit();
        }
        //F3
        void F3(int iRows)
        {
            if (dtGrid.Rows.Count == 0) return;


            String desc = dtGrid.Rows[iRows]["ITEMBARCODE"].ToString() + " " + dtGrid.Rows[iRows]["SPC_ITEMNAME"].ToString() +
                " จำนวน  " + dtGrid.Rows[iRows]["QtyOrder"].ToString() + "  " + dtGrid.Rows[iRows]["UNITID"].ToString();

            if (MessageBox.Show("ยืนยันการยกเลิกรายการ " + Environment.NewLine + desc + " ?.", SystemClass.HeaderBuyCust,
                       MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;

            radLabel_Grand.Text = (Convert.ToDouble(radLabel_Grand.Text) - Convert.ToDouble(radGridView_Show.Rows[iRows].Cells["Grand"].Value.ToString())).ToString("N2");

            radGridView_Show.Rows.RemoveAt(iRows);
            dtGrid.AcceptChanges();
            radTextBox_Barcode.SelectAll();
            radTextBox_Barcode.Focus();

            if (radGridView_Show.Rows.Count > 0)
            {
                radGridView_Show.Rows[dtGrid.Rows.Count - 1].IsCurrent = true;
            }
        }
        //Cust
        string CustInput()
        {
            BuyItem_Cust _orderCust = new BuyItem_Cust(_pTypeOpen, custID, custName, custTel, custAddress);
            if (_orderCust.ShowDialog(this) == DialogResult.Yes)
            {
                custID = _orderCust.custID;
                custName = _orderCust.custName;
                custTel = _orderCust.custTel;
                custAddress = _orderCust.custAddress;

                radLabel_CustName.Text = typeDesc + " " + custName + " " + custTel;
                return "1";
            }
            else
            {
                return "0";
            }
        }
        //Home
        void Home()
        {
            if (dtGrid.Rows.Count == 0) return;

            if ((custName == "") || (custTel == ""))
            {
                if (CustInput() == "0")
                {
                    return;
                }
            }

            string f0 = "F3";
            switch (_pTypeOpen)
            {
                case "0":
                    f0 = "F3";
                    break;
                case "1":
                    f0 = "F6";
                    break;
                default:
                    break;
            }
            GeneralForm.FormHome _home = new GeneralForm.FormHome(f0, "0", radLabel_Head.Text, radLabel_Grand.Text, radGridView_Show.Rows.Count.ToString("N2"));
            if (_home.ShowDialog(this) == DialogResult.Yes)
            {
                radLabel_Docno.Text = _home.pXXX;
                SaveDataSendAX(_home.pXXX);
            }
            else
            {
                radTextBox_Barcode.SelectAll();
                radTextBox_Barcode.Focus();
                return;
            }
        }
        //พิมพ์เอกสาร
        private void PrintDocument_printBill_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            barcode.Data = billMaxMNBC;
            Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());

            int Y = 0;
            if (_pTypeOpen=="0")
            {
                e.Graphics.DrawString("รับซื้อสินค้าของลูกค้า", SystemClass.SetFont12, Brushes.Black, 10, Y);
            }
            else
            {
                e.Graphics.DrawString("รับฝากสินค้าของผู้จำหน่าย", SystemClass.SetFont12, Brushes.Black, 10, Y);
            }
            
            Y += 25;
            e.Graphics.DrawString(SystemClass.SystemPOSGROUP + " " + SystemClass.SystemZONEID, SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 20;
            e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 20;
            e.Graphics.DrawString($@"{typeDesc} : " + custID + " โทร : " + custTel, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString(custName, SystemClass.printFont, Brushes.Black, 0, Y);
            
            if (_pTypeOpen == "0")
            {
                Y += 22;
                e.Graphics.DrawString("รอบส่งเงิน " + xxxPos, SystemClass.printFont, Brushes.Black, 10, Y);
            }
            
            Y += 20;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawImage(barcodeInBitmap, 0, Y);
            Y += 67;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            for (int ii = 0; ii < radGridView_Show.Rows.Count; ii++)
            {
                Y += 20;
                e.Graphics.DrawString((ii + 1).ToString() +
                                 ".(" + (Convert.ToDouble(radGridView_Show.Rows[ii].Cells["QtyOrder"].Value).ToString("#,#0.00")).ToString() + " X " +
                                  radGridView_Show.Rows[ii].Cells["UNITID"].Value.ToString() + ")   " +
                                  radGridView_Show.Rows[ii].Cells["ITEMBARCODE"].Value.ToString(),
                     SystemClass.printFont, Brushes.Black, 1, Y);
                Y += 15;
                e.Graphics.DrawString(radGridView_Show.Rows[ii].Cells["UNITID"].Value.ToString() + " ละ " +
                    radGridView_Show.Rows[ii].Cells["Price"].Value.ToString() + " บาท รวม " +
                    radGridView_Show.Rows[ii].Cells["Grand"].Value.ToString() + " บาท",
                    SystemClass.printFont, Brushes.Black, 0, Y);
                Y += 15;
                e.Graphics.DrawString(radGridView_Show.Rows[ii].Cells["SPC_ITEMNAME"].Value.ToString(),
                    SystemClass.printFont, Brushes.Black, 0, Y);
            }
            Y += 15;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString("จำนวนทั้งหมด " + radGridView_Show.Rows.Count.ToString("N2") + @"  รายการ", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("รวมยอดเงินทั้งหมด " + radLabel_Grand.Text, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("แคชเชียร์ : " + SystemClass.SystemUserID, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString(SystemClass.SystemUserName, SystemClass.printFont, Brushes.Black, 0, Y);
            e.Graphics.PageUnit = GraphicsUnit.Inch;
        }

        //print
        void PrintDocBill()
        {
            System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 799, 32760);
            PrinterSettings settings = new PrinterSettings();
            settings.DefaultPageSettings.PaperSize = ps;
            string defaultPrinterName = settings.PrinterName;

            PrintDocument_printBill.PrintController = printController;
            PrintDocument_printBill.PrinterSettings.PrinterName = defaultPrinterName;

            PrintDocument_printBill.Print();
            PrintDocument_printBill.Print();
            PrintDocument_printBill.Print();
        }
    }
}
