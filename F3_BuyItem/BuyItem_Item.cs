﻿using System;
using System.Windows.Forms;
using Cashier.Class; 

namespace Cashier.F3_BuyItem
{
    public partial class BuyItem_Item : Telerik.WinControls.UI.RadForm
    {
        public double sPriceUnit;
        readonly string _pTypeOpen;//0 รับซื้อของลูกค้า 1 รับฝากของผู้จำหน่าย
        //Number
        private void RadTextBox_Price_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = !char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar) && (e.KeyChar != '.');
        }

        //Load
        public BuyItem_Item(string pTypeOpen,string pBarcode, string pName, string pQty, string pUnit)
        {
            InitializeComponent();

            _pTypeOpen = pTypeOpen;
            radLabel_Barcode.Text = pBarcode;
            radLabel_Name.Text = pName;
            radLabel_Qty.Text = pQty;
            radLabel_Unit.Text = pUnit;

        }
        //Clear
        void ClearData()
        {
            radTextBox_Price.Text = "";
            radTextBox_Price.Focus();
        }
        //load
        private void BuyItem_Item_Load(object sender, EventArgs e)
        {
            switch (_pTypeOpen)
            {
                case "0":
                    this.BackColor = ConfigClass.SetBackColor_F3();
                    radLabel_Detail.BackColor = ConfigClass.SetBackColor_F3();
                    break;
                case "1":
                    this.BackColor = ConfigClass.SetBackColor_F6();
                    radLabel_Detail.BackColor = ConfigClass.SetBackColor_F6();
                    break;
                default:
                    break;
            }
            radLabel_Detail.Text = "ระบุ ราคาต่อหน่วย > กด Enter";
            ClearData();
        }
        //ปิด
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.No;
            this.Close();
        }


        private void RadTextBox_Price_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_Price.Text == "")
                {
                    MsgBoxClass.MassageBoxShowButtonOk_Warning("ต้องระบุราคาต่อหน่วยให้เรียบร้อย เท่านั้น.", SystemClass.HeaderBuyCust);
                    radTextBox_Price.Focus();
                    return;
                }

                sPriceUnit = Convert.ToDouble(radTextBox_Price.Text);

                this.DialogResult = DialogResult.Yes;
                this.Close();
            }
        }
    }
}


