﻿namespace Cashier
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_Version = new Telerik.WinControls.UI.RadLabel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.BtSmartShop = new System.Windows.Forms.Button();
            this.PictureBox_F1_help = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.BtOrderCust = new System.Windows.Forms.Button();
            this.PictureBox_F2_help = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.ButFOOD = new System.Windows.Forms.Button();
            this.PictureBox_F4_help = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.PictureBox_F3_help = new System.Windows.Forms.PictureBox();
            this.BuyItem = new System.Windows.Forms.Button();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.Button_Vender = new System.Windows.Forms.Button();
            this.PictureBox_F6_help = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.PictureBox_F5_help = new System.Windows.Forms.PictureBox();
            this.button_PrintDoc = new System.Windows.Forms.Button();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.PictureBox_F7_help = new System.Windows.Forms.PictureBox();
            this.Button_FreeItem = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.radLabel_F2 = new Telerik.WinControls.UI.RadLabel();
            this.Timer_Close = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Version)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_F1_help)).BeginInit();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_F2_help)).BeginInit();
            this.tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_F4_help)).BeginInit();
            this.tableLayoutPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_F3_help)).BeginInit();
            this.tableLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_F6_help)).BeginInit();
            this.tableLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_F5_help)).BeginInit();
            this.tableLayoutPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_F7_help)).BeginInit();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_F2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel2.Controls.Add(this.radLabel_Version, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel1, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 2, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(792, 570);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // radLabel_Version
            // 
            this.radLabel_Version.AutoSize = false;
            this.radLabel_Version.BackColor = System.Drawing.Color.Transparent;
            this.radLabel_Version.Dock = System.Windows.Forms.DockStyle.Top;
            this.radLabel_Version.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radLabel_Version.ForeColor = System.Drawing.Color.Black;
            this.radLabel_Version.Location = new System.Drawing.Point(3, 3);
            this.radLabel_Version.Name = "radLabel_Version";
            this.radLabel_Version.Size = new System.Drawing.Size(152, 19);
            this.radLabel_Version.TabIndex = 54;
            this.radLabel_Version.Text = "V";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel5, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel7, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel8, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel9, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel6, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel10, 0, 5);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(161, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 13F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 9F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(469, 564);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel4.Controls.Add(this.BtSmartShop, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.PictureBox_F1_help, 1, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(463, 67);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // BtSmartShop
            // 
            this.BtSmartShop.BackColor = System.Drawing.Color.LightBlue;
            this.BtSmartShop.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.BtSmartShop.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BtSmartShop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtSmartShop.Font = new System.Drawing.Font("Tahoma", 19.5F);
            this.BtSmartShop.Image = ((System.Drawing.Image)(resources.GetObject("BtSmartShop.Image")));
            this.BtSmartShop.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtSmartShop.Location = new System.Drawing.Point(3, 3);
            this.BtSmartShop.Name = "BtSmartShop";
            this.BtSmartShop.Size = new System.Drawing.Size(364, 61);
            this.BtSmartShop.TabIndex = 0;
            this.BtSmartShop.Text = "ลูกค้าเครดิต [F1]";
            this.BtSmartShop.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtSmartShop.UseVisualStyleBackColor = false;
            this.BtSmartShop.Click += new System.EventHandler(this.BtSmartShop_Click);
            this.BtSmartShop.KeyDown += new System.Windows.Forms.KeyEventHandler(this.BtSmartShop_KeyDown);
            // 
            // PictureBox_F1_help
            // 
            this.PictureBox_F1_help.BackColor = System.Drawing.Color.Transparent;
            this.PictureBox_F1_help.BackgroundImage = global::Cashier.Properties.Resources.help_1;
            this.PictureBox_F1_help.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PictureBox_F1_help.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PictureBox_F1_help.ErrorImage = null;
            this.PictureBox_F1_help.InitialImage = null;
            this.PictureBox_F1_help.Location = new System.Drawing.Point(373, 3);
            this.PictureBox_F1_help.Name = "PictureBox_F1_help";
            this.PictureBox_F1_help.Size = new System.Drawing.Size(87, 61);
            this.PictureBox_F1_help.TabIndex = 53;
            this.PictureBox_F1_help.TabStop = false;
            this.PictureBox_F1_help.Click += new System.EventHandler(this.PictureBox_F1_help_Click);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel5.Controls.Add(this.BtOrderCust, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.PictureBox_F2_help, 1, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 76);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(463, 67);
            this.tableLayoutPanel5.TabIndex = 1;
            // 
            // BtOrderCust
            // 
            this.BtOrderCust.BackColor = System.Drawing.Color.LightPink;
            this.BtOrderCust.CausesValidation = false;
            this.BtOrderCust.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BtOrderCust.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtOrderCust.Font = new System.Drawing.Font("Tahoma", 19.5F);
            this.BtOrderCust.ForeColor = System.Drawing.Color.Black;
            this.BtOrderCust.Image = ((System.Drawing.Image)(resources.GetObject("BtOrderCust.Image")));
            this.BtOrderCust.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BtOrderCust.Location = new System.Drawing.Point(3, 3);
            this.BtOrderCust.Name = "BtOrderCust";
            this.BtOrderCust.Size = new System.Drawing.Size(364, 61);
            this.BtOrderCust.TabIndex = 0;
            this.BtOrderCust.Text = "ออร์เดอร์ลูกค้า [F2]";
            this.BtOrderCust.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BtOrderCust.UseVisualStyleBackColor = false;
            this.BtOrderCust.Click += new System.EventHandler(this.BtOrderCust_Click);
            this.BtOrderCust.KeyDown += new System.Windows.Forms.KeyEventHandler(this.BtOrderCust_KeyDown);
            // 
            // PictureBox_F2_help
            // 
            this.PictureBox_F2_help.BackColor = System.Drawing.Color.Transparent;
            this.PictureBox_F2_help.BackgroundImage = global::Cashier.Properties.Resources.help_1;
            this.PictureBox_F2_help.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PictureBox_F2_help.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PictureBox_F2_help.ErrorImage = null;
            this.PictureBox_F2_help.Location = new System.Drawing.Point(373, 3);
            this.PictureBox_F2_help.Name = "PictureBox_F2_help";
            this.PictureBox_F2_help.Size = new System.Drawing.Size(87, 61);
            this.PictureBox_F2_help.TabIndex = 54;
            this.PictureBox_F2_help.TabStop = false;
            this.PictureBox_F2_help.Click += new System.EventHandler(this.PictureBox_F2_help_Click);
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel7.Controls.Add(this.ButFOOD, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.PictureBox_F4_help, 1, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 222);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 67F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(463, 67);
            this.tableLayoutPanel7.TabIndex = 3;
            // 
            // ButFOOD
            // 
            this.ButFOOD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.ButFOOD.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ButFOOD.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButFOOD.Font = new System.Drawing.Font("Tahoma", 19.5F);
            this.ButFOOD.Image = ((System.Drawing.Image)(resources.GetObject("ButFOOD.Image")));
            this.ButFOOD.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ButFOOD.Location = new System.Drawing.Point(3, 3);
            this.ButFOOD.Name = "ButFOOD";
            this.ButFOOD.Size = new System.Drawing.Size(364, 61);
            this.ButFOOD.TabIndex = 0;
            this.ButFOOD.Text = "รับอาหารกล่อง [F4]";
            this.ButFOOD.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.ButFOOD.UseVisualStyleBackColor = false;
            this.ButFOOD.Click += new System.EventHandler(this.ButFOOD_Click);
            this.ButFOOD.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ButFOOD_KeyDown);
            // 
            // PictureBox_F4_help
            // 
            this.PictureBox_F4_help.BackColor = System.Drawing.Color.Transparent;
            this.PictureBox_F4_help.BackgroundImage = global::Cashier.Properties.Resources.help_1;
            this.PictureBox_F4_help.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PictureBox_F4_help.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PictureBox_F4_help.ErrorImage = null;
            this.PictureBox_F4_help.Location = new System.Drawing.Point(373, 3);
            this.PictureBox_F4_help.Name = "PictureBox_F4_help";
            this.PictureBox_F4_help.Size = new System.Drawing.Size(87, 61);
            this.PictureBox_F4_help.TabIndex = 54;
            this.PictureBox_F4_help.TabStop = false;
            this.PictureBox_F4_help.Click += new System.EventHandler(this.PictureBox_F4_help_Click);
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel8.Controls.Add(this.PictureBox_F3_help, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.BuyItem, 0, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 149);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 67F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(463, 67);
            this.tableLayoutPanel8.TabIndex = 2;
            // 
            // PictureBox_F3_help
            // 
            this.PictureBox_F3_help.BackColor = System.Drawing.Color.Transparent;
            this.PictureBox_F3_help.BackgroundImage = global::Cashier.Properties.Resources.help_1;
            this.PictureBox_F3_help.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PictureBox_F3_help.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PictureBox_F3_help.ErrorImage = null;
            this.PictureBox_F3_help.Location = new System.Drawing.Point(373, 3);
            this.PictureBox_F3_help.Name = "PictureBox_F3_help";
            this.PictureBox_F3_help.Size = new System.Drawing.Size(87, 61);
            this.PictureBox_F3_help.TabIndex = 54;
            this.PictureBox_F3_help.TabStop = false;
            this.PictureBox_F3_help.Click += new System.EventHandler(this.PictureBox_F3_help_Click);
            // 
            // BuyItem
            // 
            this.BuyItem.BackColor = System.Drawing.Color.BlanchedAlmond;
            this.BuyItem.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BuyItem.CausesValidation = false;
            this.BuyItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.BuyItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BuyItem.Font = new System.Drawing.Font("Tahoma", 19.5F);
            this.BuyItem.Image = ((System.Drawing.Image)(resources.GetObject("BuyItem.Image")));
            this.BuyItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.BuyItem.Location = new System.Drawing.Point(3, 3);
            this.BuyItem.Name = "BuyItem";
            this.BuyItem.Size = new System.Drawing.Size(364, 61);
            this.BuyItem.TabIndex = 0;
            this.BuyItem.Text = "รับซื้อสินค้าลูกค้า [F3]";
            this.BuyItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.BuyItem.UseVisualStyleBackColor = false;
            this.BuyItem.Click += new System.EventHandler(this.BuyItem_Click);
            this.BuyItem.KeyDown += new System.Windows.Forms.KeyEventHandler(this.BuyItem_KeyDown);
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel9.Controls.Add(this.Button_Vender, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.PictureBox_F6_help, 1, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 295);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 67F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(463, 67);
            this.tableLayoutPanel9.TabIndex = 4;
            // 
            // Button_Vender
            // 
            this.Button_Vender.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(247)))), ((int)(((byte)(208)))), ((int)(((byte)(122)))));
            this.Button_Vender.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Button_Vender.CausesValidation = false;
            this.Button_Vender.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Button_Vender.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_Vender.Font = new System.Drawing.Font("Tahoma", 19.5F);
            this.Button_Vender.Image = ((System.Drawing.Image)(resources.GetObject("Button_Vender.Image")));
            this.Button_Vender.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Button_Vender.Location = new System.Drawing.Point(3, 3);
            this.Button_Vender.Name = "Button_Vender";
            this.Button_Vender.Size = new System.Drawing.Size(364, 61);
            this.Button_Vender.TabIndex = 0;
            this.Button_Vender.Text = "    รับฝากสินค้า ผู้จำหน่าย [F6]";
            this.Button_Vender.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Button_Vender.UseVisualStyleBackColor = false;
            this.Button_Vender.Click += new System.EventHandler(this.Button_Vender_Click);
            this.Button_Vender.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Button_Vender_KeyDown);
            // 
            // PictureBox_F6_help
            // 
            this.PictureBox_F6_help.BackColor = System.Drawing.Color.Transparent;
            this.PictureBox_F6_help.BackgroundImage = global::Cashier.Properties.Resources.help_1;
            this.PictureBox_F6_help.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PictureBox_F6_help.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PictureBox_F6_help.ErrorImage = null;
            this.PictureBox_F6_help.Location = new System.Drawing.Point(373, 3);
            this.PictureBox_F6_help.Name = "PictureBox_F6_help";
            this.PictureBox_F6_help.Size = new System.Drawing.Size(87, 61);
            this.PictureBox_F6_help.TabIndex = 55;
            this.PictureBox_F6_help.TabStop = false;
            this.PictureBox_F6_help.Click += new System.EventHandler(this.PictureBox_F6_help_Click);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel6.Controls.Add(this.PictureBox_F5_help, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.button_PrintDoc, 0, 0);
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 441);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(463, 67);
            this.tableLayoutPanel6.TabIndex = 6;
            // 
            // PictureBox_F5_help
            // 
            this.PictureBox_F5_help.BackColor = System.Drawing.Color.Transparent;
            this.PictureBox_F5_help.BackgroundImage = global::Cashier.Properties.Resources.help_1;
            this.PictureBox_F5_help.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PictureBox_F5_help.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PictureBox_F5_help.ErrorImage = null;
            this.PictureBox_F5_help.Location = new System.Drawing.Point(373, 3);
            this.PictureBox_F5_help.Name = "PictureBox_F5_help";
            this.PictureBox_F5_help.Size = new System.Drawing.Size(87, 61);
            this.PictureBox_F5_help.TabIndex = 54;
            this.PictureBox_F5_help.TabStop = false;
            this.PictureBox_F5_help.Click += new System.EventHandler(this.PictureBox_F5_help_Click);
            // 
            // button_PrintDoc
            // 
            this.button_PrintDoc.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.button_PrintDoc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_PrintDoc.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_PrintDoc.Font = new System.Drawing.Font("Tahoma", 19.5F);
            this.button_PrintDoc.Image = ((System.Drawing.Image)(resources.GetObject("button_PrintDoc.Image")));
            this.button_PrintDoc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button_PrintDoc.Location = new System.Drawing.Point(3, 3);
            this.button_PrintDoc.Name = "button_PrintDoc";
            this.button_PrintDoc.Size = new System.Drawing.Size(364, 61);
            this.button_PrintDoc.TabIndex = 0;
            this.button_PrintDoc.Text = "พิมพ์บิลซ้ำ [F5]";
            this.button_PrintDoc.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button_PrintDoc.UseVisualStyleBackColor = false;
            this.button_PrintDoc.Click += new System.EventHandler(this.Button_PrintDoc_Click);
            this.button_PrintDoc.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Button_PrintDoc_KeyDown);
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 2;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 80F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel10.Controls.Add(this.PictureBox_F7_help, 1, 0);
            this.tableLayoutPanel10.Controls.Add(this.Button_FreeItem, 0, 0);
            this.tableLayoutPanel10.Location = new System.Drawing.Point(3, 368);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(463, 67);
            this.tableLayoutPanel10.TabIndex = 5;
            // 
            // PictureBox_F7_help
            // 
            this.PictureBox_F7_help.BackColor = System.Drawing.Color.Transparent;
            this.PictureBox_F7_help.BackgroundImage = global::Cashier.Properties.Resources.help_1;
            this.PictureBox_F7_help.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.PictureBox_F7_help.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PictureBox_F7_help.ErrorImage = null;
            this.PictureBox_F7_help.Location = new System.Drawing.Point(373, 3);
            this.PictureBox_F7_help.Name = "PictureBox_F7_help";
            this.PictureBox_F7_help.Size = new System.Drawing.Size(87, 61);
            this.PictureBox_F7_help.TabIndex = 54;
            this.PictureBox_F7_help.TabStop = false;
            this.PictureBox_F7_help.Click += new System.EventHandler(this.PictureBox_F7_help_Click);
            // 
            // Button_FreeItem
            // 
            this.Button_FreeItem.BackColor = System.Drawing.Color.Gainsboro;
            this.Button_FreeItem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Button_FreeItem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Button_FreeItem.Font = new System.Drawing.Font("Tahoma", 19.5F);
            this.Button_FreeItem.Image = ((System.Drawing.Image)(resources.GetObject("Button_FreeItem.Image")));
            this.Button_FreeItem.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Button_FreeItem.Location = new System.Drawing.Point(3, 3);
            this.Button_FreeItem.Name = "Button_FreeItem";
            this.Button_FreeItem.Size = new System.Drawing.Size(364, 61);
            this.Button_FreeItem.TabIndex = 0;
            this.Button_FreeItem.Text = "   แจกของแถมลูกค้า [F7]";
            this.Button_FreeItem.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Button_FreeItem.UseVisualStyleBackColor = false;
            this.Button_FreeItem.Click += new System.EventHandler(this.Button_FreeItem_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.radLabel_F2, 0, 2);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(636, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(153, 564);
            this.tableLayoutPanel3.TabIndex = 53;
            // 
            // radLabel_F2
            // 
            this.radLabel_F2.AutoSize = false;
            this.radLabel_F2.BackColor = System.Drawing.Color.Red;
            this.radLabel_F2.BorderVisible = true;
            this.radLabel_F2.Font = new System.Drawing.Font("Tahoma", 9.75F);
            this.radLabel_F2.ForeColor = System.Drawing.Color.Black;
            this.radLabel_F2.Location = new System.Drawing.Point(3, 537);
            this.radLabel_F2.Name = "radLabel_F2";
            this.radLabel_F2.Size = new System.Drawing.Size(147, 19);
            this.radLabel_F2.TabIndex = 0;
            this.radLabel_F2.Text = "กด esc >>  ปิดโปรแกรม";
            this.radLabel_F2.TextAlignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Timer_Close
            // 
            this.Timer_Close.Tick += new System.EventHandler(this.Timer_Close_Tick);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(792, 570);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Font = new System.Drawing.Font("Tahoma", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Main";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Main";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.Main_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Main_KeyDown);
            this.tableLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_Version)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_F1_help)).EndInit();
            this.tableLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_F2_help)).EndInit();
            this.tableLayoutPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_F4_help)).EndInit();
            this.tableLayoutPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_F3_help)).EndInit();
            this.tableLayoutPanel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_F6_help)).EndInit();
            this.tableLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_F5_help)).EndInit();
            this.tableLayoutPanel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox_F7_help)).EndInit();
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radLabel_F2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Telerik.WinControls.UI.RadLabel radLabel_F2;
        internal System.Windows.Forms.Button BuyItem;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.PictureBox PictureBox_F1_help;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.PictureBox PictureBox_F2_help;
        internal System.Windows.Forms.Button button_PrintDoc;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.PictureBox PictureBox_F5_help;
        private System.Windows.Forms.PictureBox PictureBox_F4_help;
        private System.Windows.Forms.PictureBox PictureBox_F3_help;
        internal System.Windows.Forms.Button BtOrderCust;
        internal System.Windows.Forms.Button ButFOOD;
        internal System.Windows.Forms.Button BtSmartShop;
        private Telerik.WinControls.UI.RadLabel radLabel_Version;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.PictureBox PictureBox_F6_help;
        internal System.Windows.Forms.Button Button_Vender;
        private System.Windows.Forms.Timer Timer_Close;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.PictureBox PictureBox_F7_help;
        internal System.Windows.Forms.Button Button_FreeItem;
    }
}
