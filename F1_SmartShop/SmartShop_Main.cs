﻿using System;
using System.Windows.Forms;
using Cashier.Class;
using Telerik.WinControls;
using Telerik.WinControls.UI;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;

namespace Cashier.F1_SmartShop
{
    public partial class SmartShop_Main : Telerik.WinControls.UI.RadForm
    {
        private readonly BarcodeLib.Barcode.Linear barcode = new BarcodeLib.Barcode.Linear();
        readonly PrintController printController = new StandardPrintController();
        private string pCopyDesc;

        string pCaseYa = "0";//กรณีช่องทางการขาย 35 และ 37

        int iCountItem = 0;
        int iRowDT = 0;
        readonly DataTable dtGrid = new DataTable();
        readonly int maxRows = 20;
        public SmartShop_Main()
        {
            InitializeComponent();
        }
        //Clear
        void ClearData()
        {
            if (dtGrid.Rows.Count > 0) dtGrid.Rows.Clear();

            iRowDT = 0;
            iCountItem = 0;
            pCaseYa = "0";
            pCopyDesc = "";
            radLabel_CustID.Text = "";
            radLabel_CustName.Text = "";
            radLabel_Docno.Text = "";
            radLabel_ReciveID.Text = "";
            radLabel_ReciveName.Text = "";
            radTextBox_Barcode.Text = "";
            radLabel_X.Visible = false;
            radLabel_Qty.Visible = false; radLabel_Qty.Text = "1";
            radLabel_Grand.Text = "0.00";
            radTextBox_Barcode.Text = "";
            radTextBox_Barcode.Focus();
        }
        //load
        private void SmartShop_Main_Load(object sender, EventArgs e)
        {
            barcode.Type = BarcodeLib.Barcode.BarcodeType.CODE128;
            barcode.UOM = BarcodeLib.Barcode.UnitOfMeasure.PIXEL;
            barcode.BarWidth = 1;
            barcode.BarHeight = 45;
            barcode.LeftMargin = 30;
            barcode.RightMargin = 0;
            barcode.TopMargin = 0;
            barcode.BottomMargin = 0;

            ClearData();

            radLabel_Detail.Text = "F3 > void สินค้า | F2 > ยกเลิกบิล | PdDn > Multiply | Home : รวม";

            if ((SystemClass.SystemPOSGROUP == "MN098") || (SystemClass.SystemPOSGROUP == "RT"))
            {
                pCaseYa = "1";
                if (SystemClass.SmartShop_CstID == "C104647") SystemClass.SmartShop_SyChannel = "36";
                if (SystemClass.SmartShop_CstID == "C143199") SystemClass.SmartShop_SyChannel = "36";
            }

            DatagridClass.SetDefaultRadGridView(radGridView_Show);

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("LINENUM", "ลำดับ"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("ITEMID", "ITEMID"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("INVENTDIMID", "INVENTDIMID"));

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("ITEMBARCODE", "บาร์โค้ด", 140));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("SPC_ITEMNAME", "ชื่อสินค้า", 400));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManualSetRight("QTY", "ปริมาณ", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("UNITID", "หน่วย", 80));

            if (pCaseYa == "1")
            {
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("SALEPRICE", "ราคา/หน่วย", 100));
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddManualSetRight("LINEAMOUNT", "ยอดรวม", 150));
            }
            else
            {
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddVisible("SALEPRICE", "ราคา/หน่วย"));
                radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddVisible("LINEAMOUNT", "ยอดรวม"));
            }

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("SPC_SalesPriceType", "ประเภทการขาย"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("STA_VOID", "STA_VOID"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("SPC_ITEMSALESTAX", "ภาษีสินค้า"));

            radGridView_Show.MasterTemplate.EnableFiltering = false;
            radGridView_Show.TableElement.RowHeight = 60;

            dtGrid.Columns.Add("LINENUM");
            dtGrid.Columns.Add("ITEMID");
            dtGrid.Columns.Add("INVENTDIMID");
            dtGrid.Columns.Add("ITEMBARCODE");
            dtGrid.Columns.Add("SPC_ITEMNAME");
            dtGrid.Columns.Add("QTY");
            dtGrid.Columns.Add("UNITID");
            dtGrid.Columns.Add("SALEPRICE");
            dtGrid.Columns.Add("LINEAMOUNT");
            dtGrid.Columns.Add("SPC_SalesPriceType");
            dtGrid.Columns.Add("STA_VOID");
            dtGrid.Columns.Add("SPC_ITEMSALESTAX");

            radLabel_CustID.Text = SystemClass.SmartShop_CstID;
            radLabel_CustName.Text = SystemClass.SmartShop_CstName;
            radLabel_ReciveID.Text = SystemClass.SmartShop_CardID;
            radLabel_ReciveName.Text = SystemClass.SmartShop_CardName;
            radTextBox_Barcode.Focus();
        }
        //Format Cell
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            e.CellElement.Font = SystemClass.SetFont12;
            try
            {
                if (e.CellElement is GridRowHeaderCellElement && e.Row is GridViewDataRowInfo)
                {
                    e.CellElement.Text = (e.CellElement.RowIndex + 1).ToString();
                    e.CellElement.TextImageRelation = TextImageRelation.ImageBeforeText;
                }
                else
                {
                    e.CellElement.ResetValue(LightVisualElement.TextImageRelationProperty, ValueResetFlags.Local);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        //F2 remove
        private void RadGridView_Show_KeyDown(object sender, KeyEventArgs e)
        {
            if (dtGrid.Rows.Count == 0) return;

            switch (e.KeyCode)
            {
                case Keys.F3: F3(radGridView_Show.CurrentRow.Index); break;
                case Keys.Escape: radTextBox_Barcode.Focus(); break;
                default:
                    break;
            }
        }
        //Enter Barcode
        private void RadTextBox_Barcode_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Enter:
                    if (radTextBox_Barcode.Text == "")
                    {
                        MsgBoxClass.MassageBoxShowButtonOk_Warning("ระบุบาร์โค้ดสินค้าก่อน Enter.", SystemClass.HeaderSmartShop);
                        radTextBox_Barcode.Focus();
                        return;
                    }

                    if (iRowDT == maxRows)
                    {
                        MsgBoxClass.MassageBoxShowButtonOk_Warning("กำหนดทำรายการ 20 รายการ / บิลเท่านั้น" + Environment.NewLine +
                            "ให้ปิดบิลนี้ให้เรียบร้อยแล้วทำทำบิลใหม่เพิ่ม.", SystemClass.HeaderSmartShop);
                        radTextBox_Barcode.Focus();
                        return;
                    }

                    EnterTextbox(radTextBox_Barcode.Text);
                    break;

                case Keys.PageDown:
                    if (radTextBox_Barcode.Text == "")
                    {
                        return;
                    }
                    int qty;
                    try
                    {
                        qty = Convert.ToInt32((radTextBox_Barcode.Text));
                    }
                    catch (Exception)
                    {
                        qty = 0;
                    }
                    //check qty
                    if (qty <= 0)
                    {
                        MsgBoxClass.MassageBoxShowButtonOk_Warning("จำนวนที่ระบุต้องมากกว่า 0 เท่านั้น.", SystemClass.HeaderSmartShop);
                        //radLabel_X.Visible = false;
                        radTextBox_Barcode.SelectAll();
                        radTextBox_Barcode.Focus();
                        return;
                    }
                    if (qty > 10000)
                    {
                        MsgBoxClass.MassageBoxShowButtonOk_Warning("จำนวนสินค้าที่ระบุมากผิดปกติ เช็คใหม่อีกครั้ง", SystemClass.HeaderBuyCust);
                        radTextBox_Barcode.SelectAll();
                        radTextBox_Barcode.Focus();
                        return;
                    }
                    radLabel_X.Visible = true;
                    radLabel_Qty.Text = qty.ToString("N2");
                    radLabel_Qty.Visible = true;
                    radTextBox_Barcode.SelectAll();
                    radTextBox_Barcode.Focus();
                    break;

                case Keys.F2:
                    F2();
                    break;

                case Keys.F3:
                    if (dtGrid.Rows.Count == 0) return;
                    F3(dtGrid.Rows.Count - 1);
                    break;

                case Keys.Home:
                    Home();
                    break;

                case Keys.Escape:
                    if (radGridView_Show.Rows.Count == 0) Application.Exit();
                    break;
                default:
                    return;
            }
        }
        //Check ChFor MN098-RT
        String CheckCH_YA(string Barcode)
        {
            int iYa = SmartShop.GetSmartShopD044_Barcode(Barcode);//1 คอนซูเมอร์[37]  0 ยา[35]
            if (dtGrid.Rows.Count == 0)
            {
                DataTable dtCh = new DataTable();
                switch (iYa)
                {
                    case 0:
                        dtCh = SmartShop.GetChannel_CustID(SystemClass.SmartShop_CstID, "35");
                        break;
                    case 1:
                        dtCh = SmartShop.GetChannel_CustID(SystemClass.SmartShop_CstID, "37");
                        break;
                    default:
                        break;
                }
                if (dtCh.Rows.Count > 0)
                {
                    SystemClass.SmartShop_SyChannel = dtCh.Rows[0]["ORIGINID"].ToString();
                    SystemClass.SmartShop_SyPOSPRICE = dtCh.Rows[0]["SPC_REFFIELDNAME"].ToString();
                    SystemClass.SmartShop_Term = dtCh.Rows[0]["PAYMTERMID"].ToString();
                }
                else
                {
                    MsgBoxClass.MassageBoxShowButtonOk_Warning("รหัสลูกค้าที่ระบุไม่มีช่องทางการขาย" + Environment.NewLine +
                        "35-เครดิต (ร้านยา,คลินิค)-บิลยา" + Environment.NewLine +
                        "37-เครดิต (ร้านยา,คลีนิค)-บิลคอนซูเมอร์" + Environment.NewLine +
                        "ไม่สามารถรับสินค้าที่สาขานี้ได้" + Environment.NewLine +
                        "ติดต่อแผนกบัญชีเพื่อสอบถามข้อมูลเพิ่มเติม."
                        , SystemClass.HeaderSmartShop);
                    return "1";
                }
            }
            else
            {
                switch (iYa)
                {
                    case 0:
                        if (SystemClass.SmartShop_SyChannel != "35")
                        {
                            MsgBoxClass.MassageBoxShowButtonOk_Warning("ช่องทางการขายของสินค้าไม่เหมือนกัน จะต้องแยกบิลในการขาย" + Environment.NewLine +
                                                    "บิลนี้ซื้อได้เฉพาะ บิลยา " + Environment.NewLine +
                                                    "35-เครดิต (ร้านยา,คลินิค)-บิลยา" + Environment.NewLine +
                                                    "ถ้าต้องการซื้อสินค้านี้ ให้ทำบิลใหม่."
                                                    , SystemClass.HeaderSmartShop);
                            return "1";
                        }
                        break;
                    case 1:
                        if (SystemClass.SmartShop_SyChannel != "37")
                        {
                            MsgBoxClass.MassageBoxShowButtonOk_Warning("ช่องทางการขายของสินค้าไม่เหมือนกัน จะต้องแยกบิลในการขาย" + Environment.NewLine +
                                                                "บิลนี้ซื้อได้เฉพาะ บิลคอนซูเมอร์ " + Environment.NewLine +
                                                                "37-เครดิต (ร้านยา,คลีนิค)-บิลคอนซูเมอร์" + Environment.NewLine +
                                                                "ถ้าต้องการซื้อสินค้านี้ ให้ทำบิลใหม่."
                                                                , SystemClass.HeaderSmartShop);
                            return "1";
                        }
                        break;
                    default:
                        break;
                }
            }
            return "0";
        }
        //Enter Barcode
        void EnterTextbox(string barcode)
        {
            double priceNet = 0;
            double weight;
            string barcodeType = "0";//0 ธรรมดา 1 ตาชั่ง+น้ำหนัก

            //ค้นหารายละเอียดสินค้า
            DataTable dtBarcode = SmartShop.GetBarcodeDetail_Barcode(barcode);
            if (dtBarcode.Rows.Count == 0)
            {
                if (barcode.Length == 13) { dtBarcode = SmartShop.GetBarcodeDetail_Barcode(barcode.Substring(0, 7)); } else { return; }

                if (dtBarcode.Rows.Count == 0)
                {
                    MsgBoxClass.MassageBoxShowButtonOk_Warning("ไม่พบข้อมูลสินค้าของบาร์โค้ดที่ระบุ" + Environment.NewLine + "ลองใหม่อีกครั้ง.", SystemClass.HeaderSmartShop);
                    radTextBox_Barcode.SelectAll(); radTextBox_Barcode.Focus();
                    return;
                }
                else { barcodeType = "1"; }
            }

            //check สถานะสินค้า
            if (dtBarcode.Rows[0]["SPC_ITEMACTIVE"].ToString() == "0")
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning("สินค้าบาร์โค้ดนี้ อยู่ในสถานะไม่เคลื่อนไหว" + Environment.NewLine + "ไม่สามารถทำรายการได้", SystemClass.HeaderSmartShop);
                radTextBox_Barcode.SelectAll(); radTextBox_Barcode.Focus();
                return;
            }

            //Check สถานะบิลในส่วนร้านยา MN098-RT
            if ((pCaseYa == "1") && (SystemClass.SmartShop_CstID != "C104647"))
            {
                //Check สถานะบิลในส่วนร้านยา MN098-RT
                if ((pCaseYa == "1") && (SystemClass.SmartShop_CstID == "C143199"))
                {
                }
                else
                {
                    string iYa = CheckCH_YA(barcode);
                    if (iYa == "1") return;
                }
            }


            //else
            //{
            //    // if (SystemClass.SmartShop_CstID == "C104647") SystemClass.SmartShop_SyChannel = "4";
            //}


            //check กรณีสินค้าตาชั่งและน้ำหนัก
            if (barcodeType == "1")
            {
                DataTable checkPriceNet_Weight = SmartShop.FindPriceNetWeight_ByBarcode(barcode,
                        dtBarcode.Rows[0]["SPC_SalesPriceType"].ToString(),
                        Convert.ToDouble(Convert.ToDouble(dtBarcode.Rows[0][SystemClass.SmartShop_SyPOSPRICE].ToString())), 0);

                priceNet = Convert.ToDouble(checkPriceNet_Weight.Rows[0]["priceNet"].ToString());
                weight = Convert.ToDouble(checkPriceNet_Weight.Rows[0]["weight"].ToString());
            }
            else
            {
                try
                { weight = Convert.ToDouble(radLabel_Qty.Text); }
                catch (Exception)
                { weight = 1; }

                switch (dtBarcode.Rows[0]["SPC_SalesPriceType"].ToString())
                {
                    case "1": //ราคาบังคับใช้

                        priceNet = weight * (Convert.ToDouble(dtBarcode.Rows[0][SystemClass.SmartShop_SyPOSPRICE].ToString()));
                        break;

                    case "2"://ราคาแก้ไขได้
                        GeneralForm.InputData frm = new GeneralForm.InputData("F1", "0",
                             dtBarcode.Rows[0]["ITEMBARCODE"].ToString() + " - " + dtBarcode.Rows[0]["SPC_ITEMNAME"].ToString(),
                             "ราคาขาย", "บาท")
                        {
                            pInputData = Convert.ToDouble(dtBarcode.Rows[0][SystemClass.SmartShop_SyPOSPRICE].ToString()).ToString("N2")
                        };

                        if (frm.ShowDialog(this) == DialogResult.Yes)
                        {
                            priceNet = weight * Convert.ToDouble(frm.pInputData);
                        }
                        else
                        {
                            MessageBox.Show("สินค้าเป็นประเภทราคาแบบแก้ไขได้ ต้องระบุราคาเท่านั้น.", SystemClass.HeaderSmartShop, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        break;
                    case "3"://ราคาเครื่องชั้่ง
                        MsgBoxClass.MassageBoxShowButtonOk_Warning("สินค้าเป็นประเภทเครื่องชั่ง ให้ระบุบาร์โค้ด 13 หลักเท่านั้น.", SystemClass.HeaderSmartShop);
                        return;
                    case "4":
                        MsgBoxClass.MassageBoxShowButtonOk_Warning("สินค้าเป็นประเภทเครื่องชั่ง[น้ำหนัก] ให้ระบุบาร์โค้ด 13 หลักเท่านั้น", SystemClass.HeaderSmartShop);
                        return;
                    case "5":
                        MsgBoxClass.MassageBoxShowButtonOk_Warning("สินค้าเป็นประเภทเครื่องชั่ง[เครื่องชั่งแพ็ค] ให้ระบุบาร์โค้ด 13 หลักเท่านั้น", SystemClass.HeaderSmartShop);
                        return;
                    default:
                        break;
                }
            }

            //Check
            if (pCaseYa == "1")
            {
                if (priceNet == 0)
                {
                    MsgBoxClass.MassageBoxShowButtonOk_Error("สำหรับช่องทางนี้ ไม่สามารถระบุราคาเป็น 0 ได้.", SystemClass.HeaderSmartShop);
                    return;
                }
            }

            //Check วงเงินคงเหลือ
            //if ((Convert.ToDouble(radLabel_Grand.Text) + priceNet) > SystemClass.SmartShop_CstMax)
            if ((Convert.ToDouble(radLabel_Grand.Text) + priceNet) > SystemClass.SmartShop_CstIDMax)
            {
                //MsgBoxClass.MassageBoxShowButtonOk_Error("สินค้าทั้งหมดเกินยอดเงินเหลือในการรับสินค้าแล้ว" + Environment.NewLine +
                //    "ยอดเงินที่รับสินค้าได้ = " + SystemClass.SmartShop_CstMax.ToString() + " บาท", SystemClass.HeaderSmartShop);
                //return;
                double creditRecive = SystemClass.SmartShop_CstIDMax - (Convert.ToDouble(radLabel_Grand.Text));

                MsgBoxClass.MassageBoxShowButtonOk_Error("สินค้าทั้งหมดเกินยอดเงินเหลือในการรับสินค้าแล้ว" + Environment.NewLine +
                   "ยอดเงินที่เหลิอที่จะรับสินค้าได้ = " + creditRecive.ToString("#,#0.00") + " บาท" + Environment.NewLine + 
                   "ราคาสินค้าที่ระบุ คือ " + priceNet.ToString("#,#0.00") + " บาท", SystemClass.HeaderSmartShop);
                return;
            }

            //Save
            if (dtGrid.Rows.Count == 0)
            {
                if (radLabel_Docno.Text == "")
                {
                    SaveHD(dtBarcode, priceNet, weight);
                }
                else
                {
                    SaveDT(dtBarcode, priceNet, weight);
                }
            }
            else
            {
                SaveDT(dtBarcode, priceNet, weight);
            }
        }
        // Sava HD
        void SaveHD(DataTable dtBarcode, double priceNet, double weight)
        {

            string billMaxNO = Class.ConfigClass.GetMaxINVOICEID("MNHS", "-", "MNHS", "1");
            string result = SmartShop.SaveData(billMaxNO, 1, dtBarcode, priceNet, weight);
            if (result == "")
            {
                dtGrid.Rows.Add("1",
                    dtBarcode.Rows[0]["ITEMID"].ToString(),
                    dtBarcode.Rows[0]["INVENTDIMID"].ToString(),
                    dtBarcode.Rows[0]["ITEMBARCODE"].ToString(),
                    dtBarcode.Rows[0]["SPC_ITEMNAME"].ToString(),
                    weight.ToString("N2"),
                    dtBarcode.Rows[0]["UNITID"].ToString(),
                    Convert.ToDouble(dtBarcode.Rows[0][SystemClass.SmartShop_SyPOSPRICE].ToString()).ToString("N2"),
                    priceNet.ToString("N2"),
                    dtBarcode.Rows[0]["SPC_SalesPriceType"].ToString(),
                    "1",
                    dtBarcode.Rows[0]["SPC_ITEMSALESTAX"].ToString()
                    );
                radGridView_Show.DataSource = dtGrid;
                dtGrid.AcceptChanges();

                radLabel_Grand.Text = Convert.ToDouble((Convert.ToDouble(radLabel_Grand.Text) + priceNet)).ToString("N2");

                iCountItem = 1;
                iRowDT = 1;
                radLabel_X.Visible = false;
                radLabel_Qty.Text = "1";
                radLabel_Qty.Visible = false;
                radLabel_Docno.Text = billMaxNO;
                radTextBox_Barcode.Text = "";
                radTextBox_Barcode.Focus();
                radGridView_Show.Rows[dtGrid.Rows.Count - 1].IsCurrent = true;
            }
            else
            {
                MsgBoxClass.MassageBoxShowButtonOk_Error("ไม่สามารถบันทึกข้อมูลได้ ลองใหม่อีกครั้ง" + Environment.NewLine + result, SystemClass.HeaderSmartShop);
                radTextBox_Barcode.SelectAll();
                radTextBox_Barcode.Focus();
                return;
            }
        }
        //Save DT
        void SaveDT(DataTable dtBarcode, double priceNet, double weight)
        {
            iRowDT++;
            string result = SmartShop.SaveData(radLabel_Docno.Text, iRowDT, dtBarcode, priceNet, weight);
            if (result == "")
            {
                iCountItem++;
                dtGrid.Rows.Add(iRowDT,
                    dtBarcode.Rows[0]["ITEMID"].ToString(),
                    dtBarcode.Rows[0]["INVENTDIMID"].ToString(),
                    dtBarcode.Rows[0]["ITEMBARCODE"].ToString(),
                    dtBarcode.Rows[0]["SPC_ITEMNAME"].ToString(),
                    weight.ToString("N2"),
                    dtBarcode.Rows[0]["UNITID"].ToString(),
                    Convert.ToDouble(dtBarcode.Rows[0][SystemClass.SmartShop_SyPOSPRICE].ToString()).ToString("N2"),
                    priceNet.ToString("N2"),
                    dtBarcode.Rows[0]["SPC_SalesPriceType"].ToString(),
                    "1",
                    dtBarcode.Rows[0]["SPC_ITEMSALESTAX"].ToString()
                    );
                radGridView_Show.DataSource = dtGrid;
                dtGrid.AcceptChanges();

                radLabel_Grand.Text = Convert.ToDouble((Convert.ToDouble(radLabel_Grand.Text) + priceNet)).ToString("N2");

                radLabel_X.Visible = false;
                radLabel_Qty.Text = "1";
                radLabel_Qty.Visible = false;

                radTextBox_Barcode.Text = "";
                radTextBox_Barcode.Focus();
                radGridView_Show.Rows[dtGrid.Rows.Count - 1].IsCurrent = true;
            }
            else
            {
                iRowDT -= 1;
                MsgBoxClass.MassageBoxShowButtonOk_Error("ไม่สามารถบันทึกข้อมูลได้ ลองใหม่อีกครั้ง" + Environment.NewLine + result, SystemClass.HeaderSmartShop);
                radTextBox_Barcode.SelectAll();
                radTextBox_Barcode.Focus();
                return;
            }
        }
        //F2
        void F2()
        {
            if (dtGrid.Rows.Count == 0) ClearData();

            if (MsgBoxClass.MassageBoxShowYesNo_DialogResult("ยืนยันการยกเลิกบิลเลขที่ " + radLabel_Docno.Text + " ?.", SystemClass.HeaderSmartShop) == DialogResult.No) return;

            Application.Exit();
        }
        //F3
        void F3(int iRows)
        {
            if (dtGrid.Rows.Count == 0) return;

            if (radGridView_Show.Rows[iRows].Cells["LINENUM"].Value.ToString() == "") { return; }
            if (radGridView_Show.Rows[iRows].Cells["STA_VOID"].Value.ToString() == "3") return;

            String desc = dtGrid.Rows[iRows]["ITEMBARCODE"].ToString() + " " + dtGrid.Rows[iRows]["SPC_ITEMNAME"].ToString() +
                " จำนวน  " + dtGrid.Rows[iRows]["QTY"].ToString() + "  " + dtGrid.Rows[iRows]["UNITID"].ToString();

            if (MessageBox.Show("ยืนยันการยกเลิกรายการ " + Environment.NewLine + desc + " ?.", SystemClass.HeaderSmartShop,
                       MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No) return;

            String result = SmartShop.UpdateStatus_Void(radLabel_Docno.Text, dtGrid.Rows[iRows]["LINENUM"].ToString()); //ConnectionClass.ExecuteSQL_ArrayMain(sqlUp);
            if (result == "")
            {
                iCountItem--;
                radGridView_Show.Rows[iRows].Cells["STA_VOID"].Value = "3";
                radGridView_Show.Rows[iRows].Cells["SPC_ITEMNAME"].Value = "Void-" + radGridView_Show.Rows[iRows].Cells["SPC_ITEMNAME"].Value;
                radGridView_Show.Rows[iRows].Cells["QTY"].Value = "-" + radGridView_Show.Rows[iRows].Cells["QTY"].Value;
                radGridView_Show.Rows[iRows].Cells["LINEAMOUNT"].Value = "-" + radGridView_Show.Rows[iRows].Cells["LINEAMOUNT"].Value;

                radLabel_Grand.Text = Convert.ToDouble(Convert.ToDouble(radLabel_Grand.Text) +
                    Convert.ToDouble(radGridView_Show.Rows[iRows].Cells["LINEAMOUNT"].Value.ToString())).ToString("N2");
                dtGrid.AcceptChanges();
                radTextBox_Barcode.SelectAll();
                radTextBox_Barcode.Focus();
            }
            else
            {
                MsgBoxClass.MassageBoxShowButtonOk_Warning("ไม่สามารถยกเลิกรายการสินค้า" + Environment.NewLine + desc + " ได้ " + Environment.NewLine +
                    result, SystemClass.HeaderSmartShop);
                radTextBox_Barcode.SelectAll();
                radTextBox_Barcode.Focus();
                return;
            }
        }
        //Home
        void Home()
        {
            if (dtGrid.Rows.Count == 0) return;

            GeneralForm.FormHome _home = new GeneralForm.FormHome("F1", "0", radLabel_Docno.Text, radLabel_Grand.Text, iCountItem.ToString());
            if (_home.ShowDialog(this) == DialogResult.Yes)
            {
                DataTable dtSendAX = SmartShop.FindBillDetail_Docno(radLabel_Docno.Text);
                if (dtSendAX.Rows.Count == 0)
                {
                    MsgBoxClass.MassageBoxShowButtonOk_Error("ไม่พบเลขที่บิล " + radLabel_Docno.Text + " ลองใหม่อีกครั้ง.", SystemClass.HeaderSmartShop);
                    return;
                }

                string result = ConnectionClass.ExecuteMain_AX_24_SameTime(SmartShop.SqlUpdateBill_Apv(radLabel_Docno.Text), SmartShop.SqlSendAX(dtSendAX));
                if (result == "")
                {
                    PrintDocBill();
                    Application.Exit();
                }
                else
                {
                    MsgBoxClass.MassageBoxShowButtonOk_Error("ไม่สามารถส่งข้อมูลได้ ลองใหม่อีกครั้ง" + Environment.NewLine + result, SystemClass.HeaderSmartShop);
                    radTextBox_Barcode.SelectAll();
                    radTextBox_Barcode.Focus();
                    return;
                }
            }
            else
            {
                radTextBox_Barcode.SelectAll();
                radTextBox_Barcode.Focus();
                return;
            }
        }

        //พิมพ์เอกสาร
        private void PrintDocument_printBill_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            barcode.Data = radLabel_Docno.Text;
            Bitmap barcodeInBitmap = new Bitmap(barcode.drawBarcode());

            int Y = 0;
            e.Graphics.DrawString("ลูกค้าเครดิตมินิมาร์ท", SystemClass.SetFont12, Brushes.Black, 10, Y);
            Y += 25;
            e.Graphics.DrawString(SystemClass.SystemPOSGROUP + " " + SystemClass.SystemZONEID, SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 20;
            e.Graphics.DrawString("วันที่พิมพ์ " + DateTime.Now.ToString("dd-MM-yyyy HH:mm:ss"), SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 20;
            e.Graphics.DrawString("รหัสลูกค้า " + SystemClass.SmartShop_CstID, SystemClass.printFont, Brushes.Black, 10, Y);
            Y += 20;
            e.Graphics.DrawString(SystemClass.SmartShop_CstName, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawImage(barcodeInBitmap, 5, Y);
            Y += 70;
            e.Graphics.DrawString("ผู้รับสินค้า " + SystemClass.SmartShop_CardID, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString(SystemClass.SmartShop_CardName, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString("------------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);

            for (int ii = 0; ii < radGridView_Show.Rows.Count; ii++)
            {
                if (radGridView_Show.Rows[ii].Cells["STA_VOID"].Value.ToString() == "1")
                {
                    Y += 20;
                    e.Graphics.DrawString((ii + 1).ToString() + ".(" + (Convert.ToDouble(radGridView_Show.Rows[ii].Cells["QTY"].Value).ToString("#,#0.00")).ToString() + " X " +
                                      radGridView_Show.Rows[ii].Cells["UNITID"].Value.ToString() + ")   " +
                                      radGridView_Show.Rows[ii].Cells["ITEMBARCODE"].Value.ToString(),
                         SystemClass.printFont, Brushes.Black, 1, Y);
                    if (pCaseYa == "1")
                    {
                        Y += 15;
                        e.Graphics.DrawString(radGridView_Show.Rows[ii].Cells["UNITID"].Value.ToString() + " ละ " +
                            radGridView_Show.Rows[ii].Cells["SALEPRICE"].Value.ToString() + " บาท รวม " +
                            radGridView_Show.Rows[ii].Cells["LINEAMOUNT"].Value.ToString() + " บาท",
                            SystemClass.printFont, Brushes.Black, 0, Y);
                    }
                    Y += 15;
                    e.Graphics.DrawString(radGridView_Show.Rows[ii].Cells["SPC_ITEMNAME"].Value.ToString(),
                        SystemClass.printFont, Brushes.Black, 0, Y);
                }

            }

            Y += 15;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString("จำนวนทั้งหมด " + iCountItem.ToString("N2") + @"  รายการ", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString("แคชเชียร์ : " + SystemClass.SystemUserID, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString(SystemClass.SystemUserName, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 15;
            e.Graphics.DrawString("---------------------------------------------------------", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 20;
            e.Graphics.DrawString(pCopyDesc, SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 25;
            e.Graphics.DrawString("ผู้รับ[ลูกค้า]__________________________________", SystemClass.printFont, Brushes.Black, 0, Y);
            Y += 25;
            e.Graphics.DrawString("ผู้ส่ง[สาขา]__________________________________", SystemClass.printFont, Brushes.Black, 0, Y);

            e.Graphics.PageUnit = GraphicsUnit.Inch;
        }
        //print
        void PrintDocBill()
        {
            System.Drawing.Printing.PaperSize ps = new System.Drawing.Printing.PaperSize("User Defined Paper Size", 58, 3276);
            PrinterSettings settings = new PrinterSettings();
            settings.DefaultPageSettings.PaperSize = ps;
            string defaultPrinterName = settings.PrinterName;

            PrintDocument_printBill.PrintController = printController;
            PrintDocument_printBill.PrinterSettings.PrinterName = defaultPrinterName;

            pCopyDesc = ">>>>>> สำหรับลูกค้า ";
            PrintDocument_printBill.Print();
            pCopyDesc = ">>>>>> สำหรับมินิมาร์ท ";
            PrintDocument_printBill.Print();
            pCopyDesc = ">>>>>> สำหรับบัญชี ";
            PrintDocument_printBill.Print();
        }
    }
}
