﻿using System;
using System.Data;
using System.Windows.Forms;
using Cashier.Class;
using Telerik.WinControls;
using Telerik.WinControls.UI;

namespace Cashier.F1_SmartShop
{
    public partial class SmartShop_InputCardID : Telerik.WinControls.UI.RadForm
    {
        //Load
        public SmartShop_InputCardID()
        {
            InitializeComponent();

            radButton_Choose.ButtonElement.ShowBorder = true;
            radButton_Cancel.ButtonElement.ShowBorder = true;
        }
        //Clear
        void ClearData()
        {
            radLabel_ReciveName.Text = "";
            radTextBox_IdCard.Focus();
        }
        //load
        private void SmartShop_InputCardID_Load(object sender, EventArgs e)
        {

            DatagridClass.SetDefaultRadGridView(radGridView_Show);

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("CUST_ID", "รหัส", 80));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_AddManual("NAME", "ชื่อบริษัท", 440));

            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddVisible("CreditMax", "CreditMax"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddNumberColumn_AddVisible("CustTranOpen", "CustTranOpen"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("SPC_PrintTaxInvoice", "SPC_PrintTaxInvoice"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("PaymTermId", "PaymTermId"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("Blocked", "Blocked"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("CustActive", "CustActive"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("CUST_STATUS", "CUST_STATUS"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("CustSta", "CustSta"));
            radGridView_Show.MasterTemplate.Columns.Add(DatagridClass.AddTextBoxColumn_Visible("SPC_EmplARResponsible", "SPC_EmplARResponsible"));

            ExpressionFormattingObject obj1 = new ExpressionFormattingObject("MyCondition1", "CreditMax = '0' ", false)
            { CellBackColor = ConfigClass.SetColor_Red() };
            radGridView_Show.Columns["CUST_ID"].ConditionalFormattingObjectList.Add(obj1);

            ExpressionFormattingObject obj2 = new ExpressionFormattingObject("MyCondition1", "Blocked <> '0' ", false)  //AX-Blocked    0 ปกติ   1 ใบแจ้งหนี้ 2 ทั้งหมด
            { CellBackColor = ConfigClass.SetColor_Red() };
            radGridView_Show.Columns["CUST_ID"].ConditionalFormattingObjectList.Add(obj2);

            ExpressionFormattingObject obj3 = new ExpressionFormattingObject("MyCondition1", "CustActive = '0' ", false)//CustActive    1 สถานะปกติ 0 ถูกระบุหยุดรับ ใน 24   
            { CellBackColor = ConfigClass.SetColor_Red() };
            radGridView_Show.Columns["CUST_ID"].ConditionalFormattingObjectList.Add(obj3);

            ExpressionFormattingObject obj4 = new ExpressionFormattingObject("MyCondition1", "CUST_STATUS = '0' ", false) //CUST_STATUS   1 คือ บัตร ปชช. คนนี้ยังสามารถรับได้ 0 รับไม่ได้
            { CellBackColor = ConfigClass.SetColor_Red() };
            radGridView_Show.Columns["CUST_ID"].ConditionalFormattingObjectList.Add(obj4);

            ExpressionFormattingObject obj5 = new ExpressionFormattingObject("MyCondition1", "CustSta = '0' ", false)   //CustSta       1 มีสัญญา   0 ไม่มีสัญญา จะรับได้แค่ 5000 บาทเท่านั้น
            { CellBackColor = ConfigClass.SetColor_Red() };
            radGridView_Show.Columns["CUST_ID"].ConditionalFormattingObjectList.Add(obj5);

            radGridView_Show.MasterTemplate.EnableFiltering = false;
            radGridView_Show.TableElement.RowHeight = 60;
            radLabel_Detail.Text = "เลือก รหัส/บริษัท >> กด เลือก | สีแดง >> ลูกค้าไม่สามารถทำบิลได้ [กด เลือก เพื่อแสดงข้อความ]";

            ClearData();


        }
        //ปิด
        private void RadButton_Cancel_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        //Format Cell
        private void RadGridView_Show_ViewCellFormatting(object sender, CellFormattingEventArgs e)
        {
            e.CellElement.Font = SystemClass.SetFont12;
            try
            {
                if (e.CellElement is GridRowHeaderCellElement && e.Row is GridViewDataRowInfo)
                {
                    e.CellElement.Text = (e.CellElement.RowIndex + 1).ToString();
                    e.CellElement.TextImageRelation = TextImageRelation.ImageBeforeText;
                }
                else
                {
                    e.CellElement.ResetValue(LightVisualElement.TextImageRelationProperty, ValueResetFlags.Local);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        //Choose
        private void RadButton_Choose_Click(object sender, EventArgs e)
        {
            CheckCustID();
        }
        //Enter IDCard
        private void RadTextBox_IdCard_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (radTextBox_IdCard.Text == "") return;
                CheckIDCard();
            }
        }
        //Check ID Card
        void CheckIDCard()
        {
            this.Cursor = Cursors.WaitCursor;
            DataTable dtIDCard = SmartShop.GetCustRecive_IDCard(radTextBox_IdCard.Text);

            if (dtIDCard.Rows.Count == 0)
            {
                MsgBoxClass.MassageBoxShowButtonOk_Error("รหัสบัตรประชาชนหรือพาสสปอร์ตที่ระบุ" + Environment.NewLine +
                    "ไม่ถูกต้อง หรือ ไม่มีสทธิ์รับสินค้าที่มินิมาร์ท" + Environment.NewLine + "เช็คข้อมูลที่ระบุใหม่อีกครั้ง", SystemClass.HeaderSmartShop);
                radTextBox_IdCard.SelectAll();
                radTextBox_IdCard.Focus();
                this.Cursor = Cursors.Default;
                return;
            }

            radGridView_Show.DataSource = dtIDCard;
            dtIDCard.AcceptChanges();

            for (int i = 0; i < dtIDCard.Rows.Count; i++)
            {
                Double transOpen = SmartShop.GetCustTranOpen_CustID(dtIDCard.Rows[i]["CUST_ID"].ToString());
                Double max = Convert.ToDouble(dtIDCard.Rows[i]["CreditMax"].ToString()) - transOpen;
                radGridView_Show.Rows[i].Cells["CustTranOpen"].Value = transOpen;
                radGridView_Show.Rows[i].Cells["CreditMax"].Value = max;
            }

            radLabel_ReciveName.Text = dtIDCard.Rows[0]["CUST_PNAME"].ToString();
            radTextBox_IdCard.Enabled = false;

            this.Cursor = Cursors.Default;
        }
        //Check CustID
        void CheckCustID()
        {
            if (radGridView_Show.Rows.Count == 0) return;
            if (radGridView_Show.CurrentRow.Cells["CUST_ID"].Value.ToString() == "") return;

            if (radGridView_Show.CurrentRow.Cells["CustActive"].Value.ToString() == "0")
            {
                MsgBoxClass.MassageBoxShowButtonOk_Error("ลูกค้าถูกหยุดรับสินค้าที่มินิมาร์ทไว้" + Environment.NewLine +
                    radGridView_Show.CurrentRow.Cells["CUST_ID"].Value.ToString() + " - " + radGridView_Show.CurrentRow.Cells["NAME"].Value.ToString() + Environment.NewLine +
                   "รบกวนติดต่อฝ่ายบัญชีเพื่อสอบถามข้อมูลเพิ่มเติม", SystemClass.HeaderSmartShop);
                return;
            }

            if (radGridView_Show.CurrentRow.Cells["Blocked"].Value.ToString() != "0")
            {
                MsgBoxClass.MassageBoxShowButtonOk_Error("ลูกค้าถูกบัญชีหยุดการทำบิลไว้ทุกกรณี" + Environment.NewLine +
                    radGridView_Show.CurrentRow.Cells["CUST_ID"].Value.ToString() + " - " + radGridView_Show.CurrentRow.Cells["NAME"].Value.ToString() + Environment.NewLine +
                   "รบกวนติดต่อฝ่ายบัญชีเพื่อสอบถามข้อมูลเพิ่มเติม", SystemClass.HeaderSmartShop);
                return;
            }

            if (radGridView_Show.CurrentRow.Cells["CUST_STATUS"].Value.ToString() == "0")
            {
                MsgBoxClass.MassageBoxShowButtonOk_Error("รหัสบัตรประชาชนหรือพาสสปอร์ตที่ระบุ" + Environment.NewLine +
                    radTextBox_IdCard.Text + " - " + radLabel_ReciveName.Text + Environment.NewLine +
                   "ถูกหยุดการรับสินค้าที่มินิมาร์ท" + Environment.NewLine + "รบกวนติดต่อฝ่ายบัญชีเพื่อสอบถามข้อมูลเพิ่มเติม", SystemClass.HeaderSmartShop);
                return;
            }

            if (Convert.ToDouble(radGridView_Show.CurrentRow.Cells["CreditMax"].Value.ToString()) == 0)
            {
                MsgBoxClass.MassageBoxShowButtonOk_Error("ลูกค้ามียอดเงินค้างชำระเกินวงเงิน ไม่อนุญาติให้รับสินค้าเพิ่มได้" + Environment.NewLine +
                    radGridView_Show.CurrentRow.Cells["CUST_ID"].Value.ToString() + " - " + radGridView_Show.CurrentRow.Cells["NAME"].Value.ToString() + Environment.NewLine +
                   "รบกวนติดต่อฝ่ายบัญชีเพื่อสอบถามข้อมูลเพิ่มเติม", SystemClass.HeaderSmartShop);
                return;
            }

            if (radGridView_Show.CurrentRow.Cells["CustSta"].Value.ToString() == "0")
            {
                if (SmartShop.GetBill_CustID(radGridView_Show.CurrentRow.Cells["CUST_ID"].Value.ToString()) > 0)
                {
                    MsgBoxClass.MassageBoxShowButtonOk_Error("ลูกค้ายังไม่มีสัญญาสามารถรับสินค้าที่มินิมาร์ทได้แค่ 1 ครั้ง เท่านั้น" + Environment.NewLine +
                    radGridView_Show.CurrentRow.Cells["CUST_ID"].Value.ToString() + " - " + radGridView_Show.CurrentRow.Cells["NAME"].Value.ToString() + Environment.NewLine +
                    "รบกวนติดต่อฝ่ายบัญชีเพื่อสอบถามข้อมูลเพิ่มเติม", SystemClass.HeaderSmartShop);
                    return;
                }
            }

            //SystemClass.SmartShop_CstMax

            SystemClass.SmartShop_CstID = radGridView_Show.CurrentRow.Cells["CUST_ID"].Value.ToString();//รหัสลูกค้า
            SystemClass.SmartShop_CstName = radGridView_Show.CurrentRow.Cells["NAME"].Value.ToString();//ชื่อลูกค้า
            SystemClass.SmartShop_CardID = radTextBox_IdCard.Text;//รหัสบัตร ปชช
            SystemClass.SmartShop_CardName = radLabel_ReciveName.Text;//ชื่อผู้รับสินค้า
            SystemClass.SmartShop_CreditMax = 0;//วงเงินคงเหลือที่ลูกค้าสามารถรับสินค้าได้ในแต่ละวัน
            SystemClass.SmartShop_CreditLimit = Convert.ToDouble(radGridView_Show.CurrentRow.Cells["CreditMax"].Value.ToString());//วงเงินคงเหลือข3800800940871องลูกค้า
            //SystemClass.SmartShop_CstMax = SmartShop.GetMaxLimit_BchID(SystemClass.SystemPOSGROUP);//วงเงินที่ลูกค้าสามารถรับสินค้าได้ในแต่ละวัน สาขาใหญ่+ร้านยา 15000 สำหรับทุกไซต์คือ 5000
            SystemClass.SmartShop_CstIDMax = SmartShop.GetMaxLimit_CustID(SystemClass.SmartShop_CstID);//วงเงินที่ลูกค้าสามารถรับสินค้าได้ในแต่ละวัน
            SystemClass.SmartShop_SPC_PrintTaxInvoice = radGridView_Show.CurrentRow.Cells["SPC_PrintTaxInvoice"].Value.ToString();// ลูกค้ามีภาษี หรือ ไม่มี
            SystemClass.SmartShop_Term = radGridView_Show.CurrentRow.Cells["PaymTermId"].Value.ToString();//จำนวนวันเครดิต
            SystemClass.SmartShop_EmplARResponsible = radGridView_Show.CurrentRow.Cells["SPC_EmplARResponsible"].Value.ToString();//พนักงานที่รับผิดชอบลูกค้า

            ////กรณีของร้านยาน้องคอป ใช้ SmartShop_CstIDMax แทนแล้ว
            //if (SystemClass.SmartShop_CstID == "C104647") SystemClass.SmartShop_CstMax = 200000;
            //if (SystemClass.SmartShop_CstID == "C143199") SystemClass.SmartShop_CstMax = 200000;

            //วงเงินคงเหลือของลูกค้าต่อ วัน
            //SystemClass.SmartShop_CstMax -= SmartShop.GetGrandToday_CustID(SystemClass.SmartShop_CstID);
            SystemClass.SmartShop_CstIDMax -= SmartShop.GetGrandToday_CustID(SystemClass.SmartShop_CstID);
            //วงเงินของลูกค้าวันนั้นหมดแล้ว
            //if (SystemClass.SmartShop_CstMax <= 0)
            if (SystemClass.SmartShop_CstIDMax <= 0)
            {
                MsgBoxClass.MassageBoxShowButtonOk_Error("ลูกค้าได้รับสินค้าวันนี้เรียบร้อยครบวงเงินแล้ว" + Environment.NewLine +
                   radGridView_Show.CurrentRow.Cells["CUST_ID"].Value.ToString() + " - " + radGridView_Show.CurrentRow.Cells["NAME"].Value.ToString() + Environment.NewLine +
                  "ไม่สามารถรับเพิ่มได้อีก", SystemClass.HeaderSmartShop);
                return;
            }
            //กรณีที่ลูกค้ามีวงเงินเครดิตน้อยกว่าวงเงินประจำวัน
            //if (SystemClass.SmartShop_CreditLimit < SystemClass.SmartShop_CstMax) SystemClass.SmartShop_CstMax = SystemClass.SmartShop_CreditLimit;
            if (SystemClass.SmartShop_CreditLimit < SystemClass.SmartShop_CstIDMax) SystemClass.SmartShop_CstIDMax = SystemClass.SmartShop_CreditLimit;

            //set ช่องทางการขาย
            SystemClass.SmartShop_SyChannel = "4";
            SystemClass.SmartShop_SyPOSPRICE = SystemClass.SystemPRICEFIELDNAME;


            this.Hide();
            F1_SmartShop.SmartShop_Main _SmartShopMain = new F1_SmartShop.SmartShop_Main();
            _SmartShopMain.ShowDialog();
            this.Close();
        }

    }
}


