﻿using System;
using System.Data;
using Cashier.Class;
using System.Collections;

namespace Cashier.F1_SmartShop
{
    class SmartShop
    {
        //0 วงเงินเครดิตของแต่ละสาขา
        public static Double GetMaxLimit_BchID(string BranchID)
        {
            if (BranchID == "RT") BranchID = "MN000";
            DataTable dt = ConnectionClass.SelectSQL_Main(" CashDesktop_SmartShop_Select @pCase = '0',@StrCustID_CardID = '',@StrBranch = '" + BranchID + @"',@StrDocno_Barcode ='' ");
            if (dt.Rows.Count == 0)
            {
                return 0;
            }
            else
            {
                return Convert.ToDouble(dt.Rows[0]["BRANCH_SMARTSHOP_LIMIT"].ToString());
            }
        }
        //9 วงเงินเครดิตของแต่ละลูกค้า
        public static Double GetMaxLimit_CustID(string custID)
        {
            DataTable dt = ConnectionClass.SelectSQL_Main($@" CashDesktop_SmartShop_Select @pCase = '9',@StrCustID_CardID = '{custID}',@StrBranch = '',@StrDocno_Barcode ='' ");
            if (dt.Rows.Count == 0)
            {
                return 0;
            }
            else
            {
                return Convert.ToDouble(dt.Rows[0]["MAXLIMIT"].ToString());
            }
        }
        //1 ค้นหารหัสของลูกค้าทั้งหมดที่ บัตรปชช. ที่ระบุให้มารับได้
        public static DataTable GetCustRecive_IDCard(string IdCard)
        {
            return ConnectionClass.SelectSQL_Main(" CashDesktop_SmartShop_Select @pCase = '1',@StrCustID_CardID = '" + IdCard + @"',@StrBranch = '',@StrDocno_Barcode ='' ");
        }
        //2 ค้นหา ยอดของลูกค้าที่ค้างชำระ
        public static Double GetCustTranOpen_CustID(string CustID)
        {
            DataTable dt = ConnectionClass.SelectSQL_Main(" CashDesktop_SmartShop_Select @pCase = '2',@StrCustID_CardID = '" + CustID + @"',@StrBranch = '',@StrDocno_Barcode ='' ");
            if (dt.Rows.Count == 0)
            {
                return 0;
            }
            else
            {
                return Convert.ToDouble(dt.Rows[0]["AmountCr"].ToString());
            }
        }
        //ค้นหาช่องทางการขาย สำหรับลูกค้า MN098-RT
        public static DataTable GetChannel_CustID(string CustID, string Ch)
        {
            string sql = @"
                SELECT	ORIGINID,PRICEGROUP,NAME,SPC_REFFIELDNAME,PAYMTERMID	
                FROM	SPC_CustSalesOriginLink WITH (NOLOCK) 
		                INNER JOIN PriceDiscGroup  WITH (NOLOCK) ON SPC_CustSalesOriginLink.PRICEGROUP = GROUPID 
                WHERE	ACCOUNTNUM = '" + CustID + @"'
		                AND SPC_CustSalesOriginLink.DATAAREAID = N'SPC' AND PriceDiscGroup.DATAAREAID = N'SPC' 
		                AND ORIGINID IN ('" + Ch + @"') 
            ";
            return ConnectionClass.SelectSQL_SendServer(sql, SystemClass.pServerAX);
        }
        //3 ในกรณีที่ลูกค้าไม่ได้ทำสัญญา จะให้ซื้อได้แค่ 1 ครั้งเท่านั้น
        public static int GetBill_CustID(string CustID)
        {
            return ConnectionClass.SelectSQL_Main(" CashDesktop_SmartShop_Select @pCase = '3',@StrCustID_CardID = '" + CustID + @"',@StrBranch = '',@StrDocno_Barcode ='' ").Rows.Count;
        }
        //4 เช็คยอดซื้อของลูกค้า ณ วันปัจจุบัน
        public static Double GetGrandToday_CustID(string CustID)
        {
            DataTable dt = ConnectionClass.SelectSQL_Main(" CashDesktop_SmartShop_Select @pCase = '4',@StrCustID_CardID = '" + CustID + @"',@StrBranch = '',@StrDocno_Barcode ='' ");
            if (dt.Rows.Count == 0)
            {
                return 0;
            }
            else
            {
                return Convert.ToDouble(dt.Rows[0]["LINEAMOUNT"].ToString());
            }
        }
        //OK FindBarcode Detail
        public static DataTable GetBarcodeDetail_Barcode(string Barcode)
        {
            return ConnectionClass.SelectSQL_Main(" ItembarcodeClass_GetItembarcode_ALLDeatil '" + Barcode + @"' ");
        }
        //5 ค้นหาสินค้าในกลุ่มของ ช่อง 35-37
        public static int GetSmartShopD044_Barcode(string Barcode)
        {
            return ConnectionClass.SelectSQL_Main(" CashDesktop_SmartShop_Select @pCase = '5',@StrCustID_CardID = '',@StrBranch = '',@StrDocno_Barcode ='" + Barcode + @"' ").Rows.Count;
        }
        //OK ค้นหา ราคา + น้ำหนัก จาก สินค้า
        public static DataTable FindPriceNetWeight_ByBarcode(string _pBarcode, string _pType, double _pPrice, double _pweight) //_pType ประเภทราคา _pPrice ราคาขายตอหน่วย //_pweight น้ำนหัก
        {
            DataTable DtPW = new DataTable();
            DtPW.Columns.Add("priceNet"); DtPW.Columns.Add("weight");
            double weight = 0; double priceNet = 0;

            switch (_pType)
            {
                case "1":
                    priceNet = _pweight * _pPrice;
                    weight = _pweight;
                    break;
                case "2":
                    priceNet = _pweight * _pPrice;
                    weight = _pweight;
                    break;
                case "3":
                    priceNet = Convert.ToDouble(_pBarcode.Substring(7, 3) + '.' + _pBarcode.Substring(10, 3));
                    weight = priceNet / _pPrice;
                    break;
                case "4":
                    weight = Convert.ToDouble(_pBarcode.Substring(7, 3) + '.' + _pBarcode.Substring(10, 3));
                    priceNet = weight / _pPrice;
                    break;
                case "5":
                    priceNet = Convert.ToDouble(_pBarcode.Substring(7, 3) + '.' + _pBarcode.Substring(10, 3)); ;
                    weight = 1;
                    break;
            }
            DtPW.Rows.Add(priceNet, weight);
            return DtPW;
        }
        //6 ค้นหาบิลเพื่อส่งเข้า AX
        public static DataTable FindBillDetail_Docno(string Docno)
        {
            return ConnectionClass.SelectSQL_Main(" CashDesktop_SmartShop_Select @pCase = '6',@StrCustID_CardID = '',@StrBranch = '',@StrDocno_Barcode ='" + Docno + @"' ");
        }
        //Send AX
        public static ArrayList SqlSendAX(DataTable dt24)
        {
            string MNHS = "";
            int SeqNo = 0;

            ArrayList sqlAX = new ArrayList();
            for (int ii = 0; ii < dt24.Rows.Count; ii++)
            {
                if (MNHS != dt24.Rows[ii]["NEWINVOICE"].ToString())
                {
                    SeqNo = 1;
                    MNHS = dt24.Rows[ii]["NEWINVOICE"].ToString();

                    string LINEPRICECALC = dt24.Rows[ii]["LINEPRICECALC"].ToString();
                    if (SystemClass.SmartShop_CstID == "C104647") LINEPRICECALC = "1";
                    if (SystemClass.SmartShop_CstID == "C143199") LINEPRICECALC = "1";

                    switch (dt24.Rows[ii]["SALESORIGINID"].ToString())
                    {
                        case "35":
                            LINEPRICECALC = "1";
                            break;
                        case "36":
                            LINEPRICECALC = "1";
                            break;
                        case "37":
                            LINEPRICECALC = "1";
                            break;
                        default:
                            break;
                    }

                    sqlAX.Add(@"
                            INSERT INTO SPC_POSWSTABLE20( 
                                    INVOICEACCOUNT,INVODATE,
                                    PRINTTAXINVOICE,SALESORIGINID,
                                    CUSTACCOUNT,SALESID,
                                    INVOICEDATE,INVOICEID,
                                    TAXINVOICE,DLVMODE,
                                    DLVTERM,DLVREASON,
                                    PAYMENT,PRICEGROUPID,LINEDISC,
                                    MULTILINEDISC,ENDDISC,DISCPERCENT,
                                    PURCHORDERFORMNUM,SALESTAKER,
                                    SALESSPN,DELIVERYNAME,
                                    DELIVERYADDRESS,EMPLCHECKER,
                                    EMPLPICK,SHIPPINGDATEREQUESTED,UPDATE2SALESSTATUS,SALESTYPE,
                                    CASHIERID,REMARKS,
                                    INVENTSITEID, INVENTLOCATIONID, 
                                    DATAAREAID, RECVERSION,
                                    RECID,NAME,
                                    SALESPOOLID,DEPARTMENT,
                                    LINEPRICECALC,EmplARResponsible)
                            VALUES (
                                    '" + dt24.Rows[ii]["INVOICEACCOUNT"].ToString() + @"','" + dt24.Rows[ii]["INVODATE"].ToString() + @"',
                                    '" + dt24.Rows[ii]["PRINTTAXINVOICE"].ToString() + @"','" + dt24.Rows[ii]["SALESORIGINID"].ToString() + @"',
                                    '" + dt24.Rows[ii]["CUSTACCOUNT"].ToString() + @"','" + dt24.Rows[ii]["SALESID"].ToString() + @"',
                                    '" + dt24.Rows[ii]["INVOICEDATEAX"].ToString() + @"','" + dt24.Rows[ii]["NEWINVOICE"].ToString() + @"',
                                    '" + dt24.Rows[ii]["TAXX"].ToString() + @"','" + dt24.Rows[ii]["DLVMODE"].ToString() + @"',
                                    '" + dt24.Rows[ii]["DLVTERM"].ToString() + @"','" + dt24.Rows[ii]["DLVREASON"].ToString() + @"',
                                    '" + dt24.Rows[ii]["PAYMENT"].ToString() + @"','" + dt24.Rows[ii]["PRICEGROUPID"].ToString() + @"','0',
                                    '" + dt24.Rows[ii]["MULTILINEDISC"].ToString() + @"','" + dt24.Rows[ii]["ENDDISC"].ToString() + @"','0',
                                    '" + dt24.Rows[ii]["PURCHORDERFORMNUM"].ToString() + @"','" + dt24.Rows[ii]["SALESTAKER"].ToString() + @"',
                                    '" + dt24.Rows[ii]["SALESSPN"].ToString() + @"','" + dt24.Rows[ii]["DELIVERYNAME"].ToString() + @"',
                                    '" + dt24.Rows[ii]["DELIVERYADDRESS"].ToString() + @"','" + dt24.Rows[ii]["EMPLCHECKER"].ToString() + @"',
                                    '" + dt24.Rows[ii]["EMPLPICK"].ToString() + @"','" + dt24.Rows[ii]["INVOICEDATEAX"].ToString() + @"','1','3',
                                    '" + dt24.Rows[ii]["CASHIERID"].ToString() + @"','" + dt24.Rows[ii]["REMARKS"].ToString() + @"',
                                    '" + dt24.Rows[ii]["INVENTSITEID"].ToString() + @"','" + dt24.Rows[ii]["INVENTLOCATIONID"].ToString() + @"',
                                    '" + dt24.Rows[ii]["DATAAREAID"].ToString() + @"','" + dt24.Rows[ii]["RECVERSION"].ToString() + @"',
                                    '" + dt24.Rows[ii]["RECIDHD"].ToString() + @"','" + dt24.Rows[ii]["NAMEHD"].ToString() + @"',
                                    '" + dt24.Rows[ii]["SALESPOOLID"].ToString() + @"','" + dt24.Rows[ii]["DEPARTMENT"].ToString() + @"',
                                    '" + LINEPRICECALC + @"','" + dt24.Rows[ii]["REMARKS"].ToString() + @"'
                            )
                    ");

                    //        'UPDATE2SALESSTATUS =  3 ออกใบแจ้งหนี้   2 รายการเบิก 1 เปิดใบสั่งขายค้างไว้
                    //        'SALESTYPE = 3 ใบสั่งขาย 1 ใบเสนอราคา
                    //        'LINEPRICECALC = 0 เอาราคาที่ส่งไป  = 1 เอาราคาจาก AX
                    string INVENTTRANSID = dt24.Rows[ii]["INVOICEID"].ToString() + @"_" + dt24.Rows[ii]["LineNUMID"].ToString();
                    sqlAX.Add(@"
                            INSERT INTO SPC_POSWSLINE20 (
                                INVOICEID,INVOICEDATE,LINENUM,
                                QTY,SALESPRICE,
                                LINEAMOUNT,ITEMID,
                                NAME,SALESUNIT,
                                ITEMBARCODE,INVENTSERIALID,
                                LINEDISC,LINEPERCENT,
                                SALESTYPE,SUPPITEMGROUPID,
                                ITEMSALESTAX,INVENTDIMID,
                                MULTILNDISC,MULTILNPERCENT,INVENTTRANSID,
                                SUPPITEMTABLERECID, DATAAREAID,
                            RECVERSION,RECID,COSTPRICE
                            ) VALUES (
                                '" + dt24.Rows[ii]["NEWINVOICE"].ToString() + @"','" + dt24.Rows[ii]["INVOICEDATEAX"].ToString() + @"','" + SeqNo + @"',
                                '" + Convert.ToDouble(dt24.Rows[ii]["QTY_SEND"].ToString()) + @"','" + Convert.ToDouble(dt24.Rows[ii]["SALESPRICE"].ToString()) + @"',
                                '" + Convert.ToDouble(dt24.Rows[ii]["LINEAMOUNT_SEND"].ToString()) + @"','" + dt24.Rows[ii]["ITEMID"].ToString() + @"',
                                '" + dt24.Rows[ii]["NAMEDT"].ToString() + @"','" + dt24.Rows[ii]["SALESUNIT"].ToString() + @"',
                                '" + dt24.Rows[ii]["ITEMBARCODE"].ToString() + @"','" + dt24.Rows[ii]["INVENTSERIALID"].ToString() + @"',
                                '" + Convert.ToDouble(dt24.Rows[ii]["LINEDISC"].ToString()) + @"','" + Convert.ToDouble(dt24.Rows[ii]["LINEPERCENT"].ToString()) + @"',
                                '" + dt24.Rows[ii]["SALESTYPE"].ToString() + @"','" + dt24.Rows[ii]["SUPPITEMGROUPID"].ToString() + @"',
                                '" + dt24.Rows[ii]["ITEMSALESTAX"].ToString() + @"','" + dt24.Rows[ii]["INVENTDIMID"].ToString() + @"',
                                '" + Convert.ToDouble(dt24.Rows[ii]["MULTILNDISC"].ToString()) + @"','" + Convert.ToDouble(dt24.Rows[ii]["MULTILNPERCENT"].ToString()) + @"','" + INVENTTRANSID + @"',
                                '" + dt24.Rows[ii]["SUPPITEMTABLERECID"].ToString() + @"','SPC',
                                '" + dt24.Rows[ii]["RECVERSION"].ToString() + @"','1','0'
                            )
                    ");
                }
                else
                {
                    SeqNo++;
                    string INVENTTRANSID = dt24.Rows[ii]["INVOICEID"].ToString() + @"_" + dt24.Rows[ii]["LineNUMID"].ToString();
                    sqlAX.Add(@"
                            INSERT INTO SPC_POSWSLINE20 (
                                INVOICEID,INVOICEDATE,LINENUM,
                                QTY,SALESPRICE,
                                LINEAMOUNT,ITEMID,
                                NAME,SALESUNIT,
                                ITEMBARCODE,INVENTSERIALID,
                                LINEDISC,LINEPERCENT,
                                SALESTYPE,SUPPITEMGROUPID,
                                ITEMSALESTAX,INVENTDIMID,
                                MULTILNDISC,MULTILNPERCENT,INVENTTRANSID,
                                SUPPITEMTABLERECID, DATAAREAID,
                            RECVERSION,RECID,COSTPRICE
                            ) VALUES (
                                '" + dt24.Rows[ii]["NEWINVOICE"].ToString() + @"','" + dt24.Rows[ii]["INVOICEDATEAX"].ToString() + @"','" + SeqNo + @"',
                                '" + Convert.ToDouble(dt24.Rows[ii]["QTY_SEND"].ToString()) + @"','" + Convert.ToDouble(dt24.Rows[ii]["SALESPRICE"].ToString()) + @"',
                                '" + Convert.ToDouble(dt24.Rows[ii]["LINEAMOUNT_SEND"].ToString()) + @"','" + dt24.Rows[ii]["ITEMID"].ToString() + @"',
                                '" + dt24.Rows[ii]["NAMEDT"].ToString() + @"','" + dt24.Rows[ii]["SALESUNIT"].ToString() + @"',
                                '" + dt24.Rows[ii]["ITEMBARCODE"].ToString() + @"','" + dt24.Rows[ii]["INVENTSERIALID"].ToString() + @"',
                                '" + Convert.ToDouble(dt24.Rows[ii]["LINEDISC"].ToString()) + @"','" + Convert.ToDouble(dt24.Rows[ii]["LINEPERCENT"].ToString()) + @"',
                                '" + dt24.Rows[ii]["SALESTYPE"].ToString() + @"','" + dt24.Rows[ii]["SUPPITEMGROUPID"].ToString() + @"',
                                '" + dt24.Rows[ii]["ITEMSALESTAX"].ToString() + @"','" + dt24.Rows[ii]["INVENTDIMID"].ToString() + @"',
                                '" + Convert.ToDouble(dt24.Rows[ii]["MULTILNDISC"].ToString()) + @"','" + Convert.ToDouble(dt24.Rows[ii]["MULTILNPERCENT"].ToString()) + @"','" + INVENTTRANSID + @"',
                                '" + dt24.Rows[ii]["SUPPITEMTABLERECID"].ToString() + @"','SPC',
                                '" + dt24.Rows[ii]["RECVERSION"].ToString() + @"','1','0'
                            )
                    ");

                }
            }

            return sqlAX;
        }
        //7 FindForPrint
        public static DataTable FindBillForPrint_Docno(string Docno)
        {
            return ConnectionClass.SelectSQL_Main(" CashDesktop_SmartShop_Select @pCase = '7',@StrCustID_CardID = '',@StrBranch = '',@StrDocno_Barcode ='" + Docno + @"' ");
        }
        //SaveData
        public static string SaveData(string docno, int iRowDT, DataTable dtBarcode, double priceNet, double weight)
        {
            ArrayList sql = new ArrayList();
            if (iRowDT == 1)
            {
                string INVENTLOCATIONID = SystemClass.SystemPOSGROUP;
                if (SystemClass.SystemPOSGROUP == "RT") INVENTLOCATIONID = "RETAILAREA";

                sql.Add(@"
                    INSERT INTO SHOP_MNHS_HD 
                    (   INVOICEACCOUNT,PRINTTAXINVOICE,CUSTACCOUNT,
                        SALESID,INVOICEID,TAXINVOICE,
                        PAYMENT,SALESTAKER,SALESSPN,DEVICEID,
                        EMPLCHECKER,EMPLPICK, 
                        POSNUMBER,POSGROUP,CASHIERID,EMPLARRESPONSIBLE,
                        INVENTLOCATIONID,NAME, 
                        PERSONNELID,PERSONNELNAME,
                        DOCUTYPE,WHOIN,WHONAME,SALESORIGINID,REMARKS,BRANCH_NAME,TRANSID_RECID 
                    )VALUES(
                        '" + SystemClass.SmartShop_CstID + @"','" + SystemClass.SmartShop_SPC_PrintTaxInvoice + @"','" + SystemClass.SmartShop_CstID + @"',
                        '" + SystemClass.SystemUserID + @"','" + docno + @"','" + SystemClass.SmartShop_SPC_PrintTaxInvoice + @"',
                        '" + SystemClass.SmartShop_Term + @"','" + SystemClass.SystemUserID + @"','" + SystemClass.SystemUserID + @"','" + SystemClass.SystemPcName + @"',
                        '" + SystemClass.SystemUserID + @"','" + SystemClass.SystemUserID + @"',
                        '" + SystemClass.SystemPOSNUMBER + @"','" + SystemClass.SystemPOSGROUP + @"','" + SystemClass.SystemUserID + @"','" + SystemClass.SmartShop_CstID + @"',
                        '" + INVENTLOCATIONID + @"','" + SystemClass.SmartShop_CstName + @"',
                        '" + SystemClass.SmartShop_CardID + @"','" + SystemClass.SmartShop_CardName + @"',
                        '3','" + SystemClass.SystemUserID + @"','" + SystemClass.SystemUserName + @"','" + SystemClass.SmartShop_SyChannel + @"','" + SystemClass.SmartShop_EmplARResponsible + @"',
                        '" + SystemClass.SystemZONEID + @"','" + SystemClass.SystemRecID + @"' 
                    ) ");
            }

            string INVENTTRANSID = docno + "_" + iRowDT.ToString();
            sql.Add(@"
                    INSERT INTO SHOP_MNHS_DT 
                    (
                        INVOICEID,LINENUM,QTY,SALESPRICE,LINEAMOUNT,
                        ITEMID,NAME,
                        SALESUNIT,ITEMBARCODE,
                        SALESTYPE,ITEMSALESTAX,
                        INVENTDIMID,INVENTTRANSID,DOCUTYPE, WHOIN
                    )VALUES(
                        '" + docno + @"','" + iRowDT + @"','" + weight + @"','" + dtBarcode.Rows[0][SystemClass.SmartShop_SyPOSPRICE].ToString() + @"','" + priceNet + @"',
                        '" + dtBarcode.Rows[0]["ITEMID"].ToString() + @"','" + dtBarcode.Rows[0]["SPC_ITEMNAME"].ToString().Replace("'", "").Replace("{", "").Replace("}", "") + @"',
                        '" + dtBarcode.Rows[0]["UNITID"].ToString() + @"','" + dtBarcode.Rows[0]["ITEMBARCODE"].ToString() + @"',
                        '" + dtBarcode.Rows[0]["SPC_SalesPriceType"].ToString() + @"','" + dtBarcode.Rows[0]["SPC_ITEMSALESTAX"].ToString() + @"',
                        '" + dtBarcode.Rows[0]["INVENTDIMID"].ToString() + @"','" + INVENTTRANSID + @"','1','" + SystemClass.SystemUserID + @"'
                    )");

            return ConnectionClass.ExecuteSQL_ArrayMain(sql);
        }
        //SaveData
        public static string UpdateStatus_Void(string docno, string lineNum)
        {
            ArrayList sqlUp = new ArrayList
            {
            String.Format(@"
                UPDATE  SHOP_MNHS_DT
                SET     DOCUTYPE = '3' ,
                        DATEUP = GETDATE(),TIMEUP=CONVERT(VARCHAR,GETDATE(),24),WHOUP = '" + SystemClass.SystemUserID + @"' 
                WHERE   INVOICEID = '" + docno + @"' AND LINENUM = '" + lineNum + @"' ")
            };

            return ConnectionClass.ExecuteSQL_ArrayMain(sqlUp);
        }
        //SaveData
        public static ArrayList SqlUpdateBill_Apv(string docno)
        {
            ArrayList sql24 = new ArrayList
                {
                    @"
                    UPDATE  SHOP_MNHS_HD
                    SET     DOCUTYPE = '1',UpAX = '1'
                    WHERE   INVOICEID = '" + docno + @"'
                "
                };
            return sql24;
        }
    }
}
