﻿using System;
using System.Data;
using System.Windows.Forms;
using Cashier.Class;

namespace Cashier
{
    public partial class MainMenu : Telerik.WinControls.UI.RadForm
    {
        public MainMenu()
        {
            InitializeComponent();

          //  this.ShowInTaskbar = true;
        }
        //Form Load
        private void MainMenu_Load(object sender, EventArgs e)
        {
            SystemClass.SystemPcName = Environment.MachineName;
            if (SystemClass.SystemPcName == "SPCN1465" ||
                SystemClass.SystemPcName == "SPCN1467" ||
                SystemClass.SystemPcName == "SPCN1468")
            {
                SystemClass.SystemUserID = "M0604049";
                SystemClass.SystemUserName = "คุณ สุนิตย์สา หนูรักษ์";
                SystemClass.SystemPOSNUMBER = "A01";
                SystemClass.SystemPOSGROUP = "MN050";
                SystemClass.SystemLOCATIONID = "เครื่อง 1";
                SystemClass.SystemZONEID = "เทศบาล";
                SystemClass.SystemPRICEGROUPID = "CASH";
                SystemClass.SystemPRICEFIELDNAME = "SPC_PriceGroup3";
            }
            else
            {
                ProcessAndLoginClass.CheckPOS();
                DataTable dtEmp = ProcessAndLoginClass.DtGetMachine(SystemClass.SystemPcName);
                if (dtEmp.Rows.Count == 0)
                {
                    MsgBoxClass.MassageBoxShowButtonOk_Error("ไม่มีข้อมูลพนักงานขาย" + Environment.NewLine + "กรุณาเข้าระบบเครื่องขายก่อนใช้งาน", SystemClass.HeaderMassage);
                    Application.Exit();
                    return;
                }
                SystemClass.SystemUserID = dtEmp.Rows[0]["EMPLID"].ToString();
                SystemClass.SystemUserName = dtEmp.Rows[0]["SPC_NAME"].ToString();
                SystemClass.SystemPOSNUMBER = dtEmp.Rows[0]["POSNUMBER"].ToString();
                SystemClass.SystemPOSGROUP = dtEmp.Rows[0]["POSGROUP"].ToString();
                SystemClass.SystemLOCATIONID = dtEmp.Rows[0]["LOCATIONID"].ToString();
                SystemClass.SystemZONEID = dtEmp.Rows[0]["ZONEID"].ToString();
                SystemClass.SystemPRICEGROUPID = dtEmp.Rows[0]["PRICEGROUPID"].ToString();
                SystemClass.SystemPRICEFIELDNAME = dtEmp.Rows[0]["PRICEFIELDNAME"].ToString();
            }

            this.Text = SystemClass.SystemZONEID + " |  " + SystemClass.SystemUserID + " " + SystemClass.SystemUserName;

            if (SystemClass.SystemPOSGROUP == "RT")
            {
                BtOrderCust.Enabled = false;
                BuyItem.Enabled = false;
                ButFOOD.Enabled = false;
            }

        }
        //Set F
        private void MainMenu_KeyDown(object sender, KeyEventArgs e)
        {
            SetF(e);
        }
        // //Set F
        private void BtSmartShop_KeyDown(object sender, KeyEventArgs e)
        {
            SetF(e);
        }
        //Set F
        private void BtOrderCust_KeyDown(object sender, KeyEventArgs e)
        {
            SetF(e);
        }
        //Set F
        private void BuyItem_KeyDown(object sender, KeyEventArgs e)
        {
            SetF(e);
        }
        //Set F
        private void ButFOOD_KeyDown(object sender, KeyEventArgs e)
        {
            SetF(e);
        }
        private void Button_PrintDoc_KeyDown(object sender, KeyEventArgs e)
        {
            SetF(e);
        }
        //Set F
        void SetF(KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.F1:
                    //MsgBoxClass.MassageBoxShowButtonOk_Information("", SystemClass.HeaderSmartShop);
                    OpenPage("F1");
                    break;
                case Keys.F2:
                    //MsgBoxClass.MassageBoxShowButtonOk_Information("", SystemClass.HeaderOrder);
                    OpenPage("F2");
                    break;
                case Keys.F3:
                    //MsgBoxClass.MassageBoxShowButtonOk_Information("", SystemClass.HeaderBuyCust);
                    OpenPage("F3");
                    break;
                case Keys.F4:
                    //MsgBoxClass.MassageBoxShowButtonOk_Information("", SystemClass.HeaderMNPI);
                    OpenPage("F4");
                    break;
                case Keys.F5:
                    //MsgBoxClass.MassageBoxShowButtonOk_Information("", SystemClass.HeaderMNPI);
                    OpenPage("F5");
                    break;
                case Keys.Escape:
                    Application.Exit();
                    break;
                default:
                    break;
            }
        }
        //OpenForm
        void OpenPage(string pCase)
        {
            switch (pCase)
            {
                case "F1":
                    this.Hide();
                    F1_SmartShop.SmartShop_InputCardID _SmartShopInputCardID = new F1_SmartShop.SmartShop_InputCardID();
                    _SmartShopInputCardID.ShowDialog();
                    this.Close();
                    break;

                case "F2":
                    this.Hide();
                    F2_OrderCust.OrderCust_Main _OrderCust = new F2_OrderCust.OrderCust_Main();
                    F2_OrderCust.OrderCust.GetSystemPurchaseFresh();
                    _OrderCust.ShowDialog();
                    this.Close();
                    break;

                case "F3":
                    this.Hide();
                    F3_BuyItem.BuyItem_Main _BuyItem = new F3_BuyItem.BuyItem_Main("0");
                    _BuyItem.ShowDialog();
                    this.Close();
                    break;

                case "F4":
                    if (SystemClass.SystemPOSGROUP == "RT") return;
                    this.Hide();
                    F4_ReciveFood.ReciveFood_Main _ReciveMain = new F4_ReciveFood.ReciveFood_Main();
                    _ReciveMain.ShowDialog();
                    this.Close();
                    break;

                case "F5":
                    this.Hide();
                    GeneralForm.FormPrint _frmPrint = new GeneralForm.FormPrint();
                    _frmPrint.ShowDialog();
                    this.Close();
                    break;

                case "F6":
                    this.Hide();
                    F3_BuyItem.BuyItem_Main _vender = new F3_BuyItem.BuyItem_Main("1");
                    _vender.ShowDialog();
                    this.Close();
                    break;

                default:
                    break;
            }
        }

        private void BtSmartShop_Click(object sender, EventArgs e)
        {
            OpenPage("F1");
        }

        private void BtOrderCust_Click(object sender, EventArgs e)
        {
            OpenPage("F2");
        }
        private void BuyItem_Click(object sender, EventArgs e)
        {
            OpenPage("F3");
        }

        private void ButFOOD_Click(object sender, EventArgs e)
        {
            OpenPage("F4");
        }
        //print
        private void Button_PrintDoc_Click(object sender, EventArgs e)
        {
            OpenPage("F5");
        }


    }
}
